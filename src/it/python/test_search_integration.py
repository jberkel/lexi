from unittest import TestCase, TestSuite
from tempfile import NamedTemporaryFile
from shutil import copyfile
from urllib.request import urlopen
import lzma
import os

from lexi.models import database_connection
from lexi.models.fts import FTSHeadword


class DictionarySearcher(object):
    bucket = 'moti-app'
    region = 'eu-central-1'
    tmp_dir = 'tmp'
    version = '20170320'

    def __init__(self, language_code):
        super().__init__()
        self.language_code = language_code
        self.key = 'db/%s/wiktionary-%s-%s.sqlite.lzma' % \
                   (self.version, self.version, language_code)

    def __str__(self):
        return 'language: %s' % self.language_code

    def search(self, keyword):
        with database_connection(self._download_and_decompress()):
            return [headword.headword for headword in FTSHeadword.headword_olha_query(keyword)]

    def _download_and_decompress(self):
        db_file = self._db_path()
        if os.path.exists(db_file):
            return db_file

        print('downloading %s => %s' % (self.key, db_file))
        tmpfile = NamedTemporaryFile(delete=False)
        with tmpfile as output:
            self._decompress_into(output)

        if not os.path.exists(os.path.dirname(db_file)):
            os.makedirs(os.path.dirname(db_file))
        try:
            os.rename(tmpfile.name, db_file)
        except OSError:
            copyfile(tmpfile.name, db_file)
            os.unlink(tmpfile.name)

        return db_file

    def _decompress_into(self, output):
        url = 'https://s3.%s.amazonaws.com/%s/%s' % (self.region, self.bucket, self.key)
        body = urlopen(url)
        if body.getcode() != 200:
            raise Exception('Invalid HTTP response: %d' % body.getcode())
        decompressor = lzma.LZMADecompressor(format=lzma.FORMAT_ALONE)
        for chunk in iter(lambda: body.read(32768), b''):
            output.write(decompressor.decompress(chunk))

    def _db_path(self):
        return '%s/test-db-%s.sqlite' % (self.tmp_dir, self.language_code)


class DictionarySearchIntegrationTest(TestCase):
    searcher = None
    query_term = None
    expected_results = []

    def runTest(self):
        self.assertIsNotNone(self.searcher)
        results = self.searcher.search(self.query_term)[0:3]
        for expected_result in self.expected_results:
            self.assertIn(expected_result, results,
                          msg='searching for \'%s\' in %s, got: %s, expected: %s' %
                              (self.query_term, self.searcher, results, self.expected_results))


def suite() -> TestSuite:
    from search_integration_data import all_tests

    test_suite = TestSuite()
    for test in all_tests:
        for query_term, results in test.items():
            for language_code, expected_results in results.items():
                test = DictionarySearchIntegrationTest()
                test.searcher = DictionarySearcher(language_code)
                test.query_term = query_term
                test.expected_results = expected_results
                test_suite.addTest(test)
    return test_suite


def load_tests(loader, standard_tests, pattern):
    return suite()


if __name__ == '__main__':
    from unittest import TextTestRunner

    TextTestRunner().run(suite())
