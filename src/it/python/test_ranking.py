import os
from unittest import TestCase
from lexi.dictionary import DictionaryDatabase
import lexi.fts_rank
from lexi.fts_rank import RowMatch
from lexi.models.fts import FTSHeadword
from lexi.rank import Ranker
from lexi.serialization import Page
import apsw


def deduplicate(row_match: RowMatch) -> RowMatch:
    if row_match.phrases() >= {0, 1}:
        return RowMatch(fixed_score=row_match.fixed_score,
                        phrase_matches=[m for m in row_match.phrase_matches if m.number != 1])
    else:
        return row_match


def olha_rank(fts: apsw.FTS5ExtensionApi, *args) -> float:
    phrase_weightings: list[float] = [float(x) for x in args[3:]]
    rank_parameters = lexi.fts_rank.RankParameters(phrase_weightings)
    # score_column = int(args[1])
    # base_score = float(fts.column_text(score_column))
    base_score = 1.0
    row_match = deduplicate(lexi.fts_rank.gather_match_information(fts, base_score))
    score = lexi.fts_rank.score(row_match, rank_parameters)

    headword = fts.column_text(0)
    print(f"{headword=} {score=}")
    print(f"{row_match=}")
    return score


class Ranking(TestCase):
    rankParams = [
        ('scoreColumn', 4),
        ('phraseWeightings', 15.0, 1.0)
    ]

    def test_faire(self):
        faire = os.path.dirname(__file__) + '/../resources/faire.ndjson'

        with DictionaryDatabase(':memory:') as db:
            db.register_ranking_function("py_olhaRank", olha_rank)

            with open(faire, 'r') as f:
                db.make_dictionary(
                    '20250101',
                    'fr',
                    'French',
                    (Page.from_json(line) for line in f),
                    Ranker()
                )
                db.reindex()

                query = FTSHeadword.headword_olha_query('faire',
                                                        rank_function='py_olhaRank',
                                                        limit=5,
                                                        rank_param=self.rankParams)

                matches = [headword.headword for headword in query]
                self.assertEqual([
                    'faire',
                    'laissez-faire',
                    'laisser-faire',
                    'faire bien',
                    'faire bombance'
                ], matches)
