# noinspection SpellCheckingInspection
basic_lookups = {
    'cat': {
        'de': ['Katze'],
        'es': ['gato'],
        'nl': ['kater'],
        'it': ['micio'],
        'ko': ['고양이'],
        'pt': ['gato']
    },
    'dog': {
        # 'de': ['Hund'],  # TODO to dog, dog-eared, to dog
        'es': ['perro'],
        'fr': ['chien'],
        'it': ['cane'],
        'ko': ['개', '견'],
        'nl': ['hond'],
        'pt': ['cão', 'cachorro']
    },
    'man': {
        'de': ['Mensch', 'Mann'],
        'es': ['hombre'],
        'fr': ['homme'],
        'it': ['uomo'],
        'nl': ['man', 'mens'],
        'pt': ['homem'],
    },
    'red': {
        'de': ['rot'],
        'es': ['rojo'],
        'fr': ['rouge'],
        'it': ['rosso'],
        # 'ko': ['붉다'],    # TODO: matches red deer, red wine, red blood cell
        'nl': ['rood'],
        'pt': ['vermelho']
    },
    'woman': {
        'de': ['Frau'],
        # 'es': ['mujer'],  # TODO
        'fr': ['femme'],
        'it': ['donna'],
        'ko': ['여성'],
        'nl': ['vrouw'],
        'pt': ['mulher']
    },
    'water': {
        'de': ['Wasser'],
        'es': ['agua'],
        'fr': ['eau'],
        'it': ['acqua'],
        'ko': ['물'],
        'nl': ['water'],
        'pt': ['água'],
    },
    'bottle': {
        # 'de': ['Flasche'],   # TODO matches bottle up
        # 'es': ['botella'],   # TODO matches bottle cap, bottle up, baby bottle
        # 'fr': ['bouteille'], # TODO bottle up, bottle out
        # 'it': ['bottiglia'], # TODO bottle up, bottle-feed, bottle cap, feeding bottler
        'nl': ['fles'],
        'pt': ['garrafa'],
    }
}

# noinspection SpellCheckingInspection
reverse_lookups = {
    'acq': {
        'it': ['acqua']
    },
    'femm': {
        'fr': ['femme']
    },
    'mens': {
        'de': ['Mensch']
    }
}

all_tests = [basic_lookups, reverse_lookups]
