import os
from unittest import TestCase
from lexi.dictionary import DictionaryDatabase
from lexi.models.fts import FTSHeadword
from lexi.rank import Ranker
from lexi.serialization import Page


class IndexAndSearch(TestCase):
    def test_faire(self):
        faire = os.path.dirname(__file__) + '/../resources/faire.ndjson'

        with DictionaryDatabase(':memory:') as db:
            with open(faire, 'r') as f:
                db.make_dictionary(
                    '20250101',
                    'fr',
                    'French',
                    (Page.from_json(line) for line in f),
                    Ranker()
                )
                db.reindex()

                query = FTSHeadword.headword_olha_query('faire', limit=5)
                matches = [headword.headword for headword in query]

                self.assertEqual([
                    'faire',
                    'faire attention',
                    'faire chier',
                    'faire son affaire',
                    "faire l'amour",
                ], matches)
