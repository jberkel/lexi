from peewee import IntegrityError

from base_test import BaseTest
from lexi.models import BaseModel, Category, HeadwordCategoryLink, \
    EntryCategoryLink, \
    SenseCategoryLink, Headword, Example, Entry, Etymology, Metadata, \
    Pronunciation, \
    Relation, Sense, Translation
from lexi.models.jwktl import PartOfSpeech, PronunciationType, RelationType


class TestBaseModel(BaseTest):
    pass


class TestHeadword(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(Headword(), BaseModel)

    def test_table_name(self):
        self.assertEqual('headwords', Headword()._meta.table_name)

    def test_create(self):
        headword = Headword.create(headword_text='test', pos=PartOfSpeech.NOUN,
                                   score=0)
        self.assertGreater(headword.id, 0)

    def test_create_requires_part_of_speech(self):
        with self.assertRaises(IntegrityError):
            Headword.create(headword='test')

    def test_headwords_with_translations_when_no_translations_present(self):
        self.generator.create_headword('foo', translations=[])
        self.assertEqual([
            ('foo', [])
        ], [(
            headword.headword_text,
            [t.headword_text for t in headword.translations]
        ) for headword in Headword.headwords_with_translations('foo')])

    def test_headwords_with_translations_with_rank(self):
        self.generator.create_headword('foo', translations=[], score=.42)
        self.assertEqual([
            ('foo', .42)
        ], [(
            headword.headword_text,
            headword.score
        ) for headword in Headword.headwords_with_translations('foo')])

    def test_headwords_with_translations_when_translations_present(self):
        self.generator.create_headword('foo', translations=['bar', 'baz'])
        self.assertEqual([
            ('foo', ['bar', 'baz'])
        ], [(
            headword.headword_text,
            [t.translation_text for t in headword.translations]
        ) for headword in Headword.headwords_with_translations('foo')])


class TestEntry(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(Entry(), BaseModel)

    def test_table_name(self):
        self.assertEqual('entries', Entry()._meta.table_name)

    def test_create(self):
        entry = Entry.create(headword_text='test', pos=PartOfSpeech.NOUN)
        self.assertGreater(entry.id, 0)

    def test_create_requires_headword_text(self):
        with self.assertRaises(IntegrityError) as cm:
            Entry.create(pos=PartOfSpeech.NOUN)
        self.assertEqual('NOT NULL constraint '
                         'failed: entries.headword_text', str(cm.exception))

    def test_create_requires_pos(self):
        with self.assertRaises(IntegrityError) as cm:
            Entry.create(headword_text='test')
        self.assertEqual('NOT NULL constraint failed: '
                         'entries.pos', str(cm.exception))

    def test_add_and_query_data(self):
        entry = Entry.create(headword_text='test', pos=PartOfSpeech.NOUN)
        entry.data = {'gender': 'm', 'transliteration': ['1', '2']}
        entry.save()

        self.assertEqual(entry, Entry.get(Entry.data['gender'] == 'm'))


class TestEtymology(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(Etymology(), BaseModel)

    def test_table_name(self):
        self.assertEqual('etymologies', Etymology()._meta.table_name)

    def test_create(self):
        etymology = Etymology.create(etymology_text='test')
        self.assertGreater(etymology.id, 0)


class TestSense(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(Sense(), BaseModel)

    def test_table_name(self):
        self.assertEqual('senses', Sense()._meta.table_name)

    def test_create(self):
        entry = Entry.create(headword_text='test', pos=PartOfSpeech.NOUN)
        sense = Sense.create(sense_text='test', entry=entry)
        self.assertGreater(sense.id, 0)

    def test_create_requires_entry(self):
        with self.assertRaises(IntegrityError) as cm:
            Sense.create(sense_text='text')
        self.assertEqual('NOT NULL constraint failed: senses.entry_id',
                         str(cm.exception))

    def test_pack_number(self):
        self.assertEqual(Sense.pack_number((1, 0, 0, 0, 0)), 1)
        self.assertEqual(Sense.pack_number((1, 1, 0, 0, 0)), 257)
        self.assertEqual(Sense.pack_number((2, 0, 0, 0, 0)), 2)
        self.assertEqual(Sense.pack_number((2, 1, 0, 0, 0)), 258)
        self.assertEqual(Sense.pack_number((2, 1, 1, 0, 0)), 65794)

    def test_unpack_number(self):
        self.assertEqual(Sense.unpack_number(1), (1, 0, 0, 0, 0))
        self.assertEqual(Sense.unpack_number(257), (1, 1, 0, 0, 0))
        self.assertEqual(Sense.unpack_number(2), (2, 0, 0, 0, 0))
        self.assertEqual(Sense.unpack_number(258), (2, 1, 0, 0, 0))
        self.assertEqual(Sense.unpack_number(65794), (2, 1, 1, 0, 0))


class TestRelation(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(Relation(), BaseModel)

    def test_table_name(self):
        self.assertEqual('relations', Relation()._meta.table_name)

    def test_create(self):
        entry = Entry.create(headword_text='test', pos=PartOfSpeech.NOUN)
        Relation.create(entry=entry, type=RelationType.ANTONYM,
                        target_headword_text='foo')

    def test_create_requires_entry_id(self):
        with self.assertRaises(IntegrityError) as cm:
            Relation.create(type=RelationType.ANTONYM,
                            target_headword_text='foo')
        self.assertEqual('NOT NULL constraint failed: '
                         'relations.entry_id', str(cm.exception))

    def test_create_requires_type(self):
        entry = Entry.create(headword_text='test', pos=PartOfSpeech.NOUN)
        with self.assertRaises(IntegrityError) as cm:
            Relation.create(entry=entry, target_headword_text='foo')
        self.assertEqual('NOT NULL constraint failed: '
                         'relations.type', str(cm.exception))

    def test_create_requires_target_headword_text(self):
        entry = Entry.create(headword_text='test', pos=PartOfSpeech.NOUN)
        with self.assertRaises(IntegrityError) as cm:
            Relation.create(entry=entry, type=RelationType.ANTONYM)
        self.assertEqual('NOT NULL constraint failed: '
                         'relations.target_headword_text',
                         str(cm.exception))


class TestPronunciation(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(Pronunciation(), BaseModel)

    def test_table_name(self):
        self.assertEqual('pronunciations', Pronunciation()._meta.table_name)

    def test_create(self):
        headword = Headword.create(headword_text='test', pos=PartOfSpeech.NOUN,
                                   score=0)
        Pronunciation.create(pronunciation_text='test', headword=headword,
                             type=PronunciationType.IPA)

    def test_create_needs_entry_id(self):
        with self.assertRaises(IntegrityError) as cm:
            Pronunciation.create(pronunciation_text='test',
                                 type=PronunciationType.IPA)
        self.assertEqual('NOT NULL constraint failed: '
                         'pronunciations.headword_id', str(cm.exception))

    def test_create_does_not_need_pronunciation_text(self):
        Entry.create(headword_text='test', pos=PartOfSpeech.NOUN)

    def test_copy(self):
        headword = Headword.create(headword_text='test', pos=PartOfSpeech.NOUN,
                                   score=0)
        p = Pronunciation.create(pronunciation_text='test', headword=headword,
                                 type=PronunciationType.IPA)
        p_copy = p.copy(pronunciation_text='test2')
        self.assertEqual(2, Pronunciation.select().count())

        self.assertNotEqual(p.id, p_copy.id)
        self.assertEqual(p.headword, p_copy.headword)
        self.assertEqual(p.type, p_copy.type)
        self.assertEqual('test2', p_copy.pronunciation_text)


class TestExample(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(Example(), BaseModel)

    def test_table_name(self):
        self.assertEqual('examples', Example()._meta.table_name)

    def test_create(self):
        entry = Entry.create(headword_text='test', pos=PartOfSpeech.NOUN)
        sense = Sense.create(sense_text='test', entry=entry)
        example = Example.create(example_text='test', translation='translation',
                                 sense=sense)
        self.assertGreater(example.id, 0)
        self.assertEqual('test', example.example_text)
        self.assertEqual('translation', example.translation)

    def test_create_requires_sense(self):
        with self.assertRaises(IntegrityError) as cm:
            Example.create(example_text='test')
        self.assertEqual('NOT NULL constraint failed: '
                         'examples.sense_id', str(cm.exception))


class TestTranslation(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(Translation(), BaseModel)

    def test_table_name(self):
        self.assertEqual('translations', Translation()._meta.table_name)

    def test_create(self):
        translation = Translation.create(headword_text='test',
                                         translation_text='translation',
                                         pos=PartOfSpeech.NOUN)
        self.assertGreater(translation.id, 0)


class TestMetadata(BaseTest):
    def test_table_name(self):
        self.assertEqual('metadata', Metadata()._meta.table_name)

    def test_as_dictionary(self):
        Metadata.create(key='key1', value='value1')
        Metadata.create(key='key2', value='value2')
        data = Metadata.as_dictionary()
        self.assertEqual({'key1': 'value1', 'key2': 'value2'}, data)


class TestCategory(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(Category(), BaseModel)

    def test_table_name(self):
        self.assertEqual('categories', Category()._meta.table_name)


class TestHeadwordCategoryLinks(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(HeadwordCategoryLink(), BaseModel)

    def test_table_name(self):
        self.assertEqual('headword_category_links',
                         HeadwordCategoryLink()._meta.table_name)


class TestEntryCategoryLinks(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(EntryCategoryLink(), BaseModel)

    def test_table_name(self):
        self.assertEqual('entry_category_links',
                         EntryCategoryLink()._meta.table_name)


class TestSenseCategoryLinks(BaseTest):
    def test_is_base_model(self):
        self.assertIsInstance(SenseCategoryLink(), BaseModel)

    def test_table_name(self):
        self.assertEqual('sense_category_links',
                         SenseCategoryLink()._meta.table_name)


if __name__ == '__main__':
    from unittest import main

    main()
