from unittest import TestCase

from data_generator import DataGenerator
from lexi.models.jwktl import PartOfSpeech, RelationType


# https://en.wiktionary.org/wiki/qui_custodiet_ipsos_custodes
class TestDataGenerator(TestCase):
    def setUp(self):
        super().setUp()
        self.subject = DataGenerator()
        self.subject.create_tables()

    def tearDown(self):
        super().tearDown()
        self.subject.close()

    def test_create_entry_with_headword(self):
        entry = self.subject.create_entry('test-entry')
        self.assertEqual(entry.headword_text, 'test-entry')

    def test_create_entry_with_default_noun_pos(self):
        entry = self.subject.create_entry()
        self.assertEqual(entry.pos, PartOfSpeech.NOUN)

    def test_create_entry_with_specified_pos(self):
        entry = self.subject.create_entry(pos=PartOfSpeech.ADJECTIVE)
        self.assertEqual(entry.pos, PartOfSpeech.ADJECTIVE)

    def test_create_entry_with_etymology(self):
        entry = self.subject.create_entry()
        self.assertIsNotNone(entry.etymology)
        self.assertEqual('etymology for word', entry.etymology.etymology_text)

    def test_create_entry_has_sense(self):
        entry = self.subject.create_entry()
        self.assertEqual(['sense for word'], [s.sense_text for s in entry.senses])

    def test_create_entry_has_no_associated_headword_id(self):
        entry = self.subject.create_entry()
        self.assertIsNone(entry.headword_id)

    def test_create_headword_with_headword_text(self):
        headword = self.subject.create_headword('test-headword')
        self.assertEqual(headword.headword_text, 'test-headword')

    def test_create_headword_with_sense_text(self):
        headword = self.subject.create_headword(sense_text='sense text')
        self.assertEqual(headword.sense_text, 'sense text')

    def test_create_headword_with_default_pos(self):
        headword = self.subject.create_headword()
        self.assertEqual(PartOfSpeech.NOUN, headword.pos)

    def test_create_headword_with_specified_pos(self):
        headword = self.subject.create_headword(pos=PartOfSpeech.ADJECTIVE)
        self.assertEqual(PartOfSpeech.ADJECTIVE, headword.pos)

    def test_create_headword_creates_headword_with_entry(self):
        headword = self.subject.create_headword()
        self.assertEqual(len(headword.entries), 1)
        self.assertEqual(headword.entries[0].headword_text, headword.headword_text)
        self.assertEqual(headword.entries[0].pos, headword.pos)

    def test_create_headword_with_translations(self):
        headword = self.subject.create_headword(headword_text='casa', translations=['house'])
        self.assertEqual([t.translation_text for t in headword.translations], ['house'])

    def test_create_relation_with_type(self):
        relation = self.subject.create_relation()
        self.assertEqual(relation.type, RelationType.SYNONYM)

    def test_create_relation_with_source_entry(self):
        relation = self.subject.create_relation()
        self.assertIsNotNone(relation.entry)

    def test_create_relation_with_target_headword(self):
        relation = self.subject.create_relation()
        self.assertIsNotNone(relation.target_headword)


if __name__ == '__main__':
    from unittest import main

    main()
