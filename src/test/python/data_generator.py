#!/usr/bin/env python
from lexi.models import Models
from lexi.models import Category, EntryCategoryLink, HeadwordCategoryLink, Headword, Entry, \
    Sense, Example, Etymology, Metadata, Pronunciation, Relation, Translation
from lexi.models.fts import Models as FTSModels
from lexi.models.jwktl import PartOfSpeech, RelationType, PronunciationType
from lexi.models import database
from mixer.backend.peewee import mixer


class DataGenerator:
    DEFAULT_POS = PartOfSpeech.PARTICLE

    def __init__(self, db=':memory:'):
        self._database = database
        self._database.init(db)
        self._mixer = mixer
        mixer.register(Entry, pos=DataGenerator.DEFAULT_POS)
        mixer.register(Headword, pos=DataGenerator.DEFAULT_POS)

    def close(self):
        self._database.close()

    def __enter__(self):
        self._database.connect()
        return self

    def __exit__(self, exception_type, exception_val, trace):
        self.close()

    def create_tables(self):
        for models in [Models, FTSModels]:
            self._database.drop_tables(models, safe=True)
            self._database.create_tables(models)

    def create_headword(self, headword_text=None, pos=PartOfSpeech.NOUN,
                        translations=None, data=None, **kwargs):
        if headword_text is None:
            headword_text = self._mixer.RANDOM

        headword = self.blend(Headword,
                              headword_text=headword_text,
                              pos=pos,
                              **kwargs)
        self.create_entry(entry_text=headword.headword_text,
                          pos=pos,
                          headword=headword,
                          data=data)
        if translations is None:
            translations = ['translation for %s' % headword.headword_text]
        for translation_text in translations:
            self.blend(Translation,
                       headword_text=headword.headword_text,
                       headword=headword,
                       translation_text=translation_text,
                       sense='sense')
        return headword

    def create_entry(self, entry_text='word', pos=PartOfSpeech.NOUN,
                     headword=None, data=None, **kwargs):
        etymology = self.blend(Etymology, etymology_text='etymology for %s' % entry_text)
        entry = self.blend(Entry,
                           etymology=etymology,
                           pos=pos,
                           headword_text=entry_text,
                           headword=headword,
                           data=data, **kwargs)
        self.create_sense(entry)
        return entry

    def create_sense(self, entry=None, **kwargs):
        if entry is None:
            entry = self.create_entry()
        sense = self.blend(Sense, entry=entry,
                           sense_text='sense for %s' % entry.headword_text, **kwargs)
        return sense

    def create_pronunciation(self, headword=None, **kwargs):
        if headword is None:
            headword = self.create_headword()

        return self.blend(Pronunciation,
                          type=PronunciationType.IPA,
                          pronunciation_text='pronunciation for %s' % headword.headword_text,
                          headword=headword, **kwargs)

    def create_example(self, sense=None, **kwargs):
        if sense is None:
            sense = self.create_sense()
        return self.blend(Example,
                          sense=sense,
                          example_text='example for %s' % sense.sense_text, **kwargs)

    def create_relation(self, headword1=None, headword2=None, relation_type=RelationType.SYNONYM):
        if headword1 is None:
            headword1 = self.create_headword()
        if headword2 is None:
            headword2 = self.create_headword()
        assert (len(headword1.entries) > 0)
        return self.blend(Relation, type=relation_type, entry=headword1.entries[0],
                          target_headword=headword2)

    def create_metadata(self, key, value):
        return self.blend(Metadata, key=key, value=value)

    def create_headword_category_link(self, headword, name='Category'):
        category, _ = Category.get_or_create(category_text=name)
        return HeadwordCategoryLink(category=category, headword=headword).save(force_insert=True)

    def create_entry_category_link(self, headword_or_entry, name='Category'):
        category, _ = Category.get_or_create(category_text=name)
        if isinstance(headword_or_entry, Headword):
            for entry in headword_or_entry.entries:
                EntryCategoryLink(category=category, entry=entry).save(force_insert=True)
        elif isinstance(headword_or_entry, Entry):
            EntryCategoryLink(category=category, entry=headword_or_entry).save(force_insert=True)
        else:
            raise Exception('Invalid type: %s' % headword_or_entry)

    def blend(self, scheme, **values):
        return self._mixer.blend(scheme, **values)
