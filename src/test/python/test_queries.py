from base_test import BaseTest
from lexi.models import Headword
from lexi.models.queries import prefetch_headwords


class TestQueries(BaseTest):
    def test_prefetch_headwords_prefetches_entries(self):
        self.generator.create_headword('casa')
        entries = [entry.headword_text
                   for headword in prefetch_headwords(Headword.query('casa'))
                   for entry in headword.entries]
        self.assertEqual(['casa'], entries)

    def test_prefetch_headwords_prefetches_senses(self):
        self.generator.create_headword('casa')
        senses = [sense.sense_text
                  for headword in prefetch_headwords(Headword.query('casa'))
                  for entry in headword.entries
                  for sense in entry.senses]
        self.assertEqual(['sense for casa'], senses)

    def test_prefetch_headwords_prefetches_translations(self):
        self.generator.create_headword('casa', translations=['house'])
        translations = [translation.translation_text
                        for headword in prefetch_headwords(Headword.query('casa'))
                        for translation in headword.translations]
        self.assertEqual(['house'], translations)

    def test_prefetch_headwords_prefetches_relationships_with_targets(self):
        casa = self.generator.create_headword('casa')
        foo = self.generator.create_headword('foo')
        self.generator.create_relation(casa, foo)

        linked_headwords = [relation.target_headword.headword_text for headword in
                            prefetch_headwords(Headword.query('casa'))
                            for entry in headword.entries
                            for relation in entry.relations
                            ]
        self.assertEqual(['foo'], linked_headwords)


if __name__ == '__main__':
    from unittest import main

    main()
