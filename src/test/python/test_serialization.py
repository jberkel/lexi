import unittest

from lexi.serialization import Entry, Relation
from polyfactories import EntryFactory, SenseFactory, HeadwordLineFactory


class TestSerialization(unittest.TestCase):

    def test_is_just_forms(self):
        entry = EntryFactory.build(categories=['Spanish non-lemma forms',
                                               'Spanish adjective forms'])
        self.assertTrue(entry.is_just_forms())

    def test_is_just_forms_mixed(self):
        entry = EntryFactory.build(
            categories=['Spanish forms', 'Spanish nouns'])
        self.assertFalse(entry.is_just_forms())

    def test_is_lemma(self):
        entry = EntryFactory.build(categories=['Spanish lemmas'])
        self.assertTrue(entry.is_lemma())

    def test_is_lemma_negative(self):
        entry = EntryFactory.build(categories=['Spanish adjective forms'])
        self.assertFalse(entry.is_lemma())

    def test_is_non_lemma(self):
        entry = EntryFactory.build(categories=['Spanish non-lemma forms'])
        self.assertTrue(entry.is_non_lemma())

    def test_is_translation_hub(self):
        entry1 = EntryFactory.build(categories=['Translation hubs'])
        entry2 = EntryFactory.build(senses=[
            SenseFactory.build(categories=['English translation hubs'])
        ])
        entry3 = EntryFactory.build(categories=['Foo hubs'])

        self.assertTrue(entry1.is_translation_hub())
        self.assertTrue(entry2.is_translation_hub())
        self.assertFalse(entry3.is_translation_hub())

    def test_sorted_by_forms_sort_key(self):
        entries = [
            EntryFactory.build(pageTitle="bar",
                               categories=['Spanish non-lemma forms']),
            EntryFactory.build(pageTitle="quux",
                               categories=['Spanish non-lemma forms']),
            EntryFactory.build(pageTitle="foo", categories=['Spanish nouns'])
        ]
        self.assertEqual(["foo", "bar", "quux"],
                         [x.pageTitle for x in
                          sorted(entries, key=Entry.forms_sort_key)])

    def test_senses_with_entry_relations(self):
        entry = EntryFactory.build(
            senses=[
                SenseFactory.build(relations=[
                    Relation(target="foo", type=0)
                ])
            ],
            relations=[
                Relation(target="bar", type=0)
            ]
        )
        senses = entry.senses_with_entry_relations()
        self.assertIsNot(senses, entry.senses)

        self.assertEqual(1, len(senses))
        # original object is not modified
        self.assertEqual(1, len(entry.senses[0].relations))

        self.assertEqual([
            Relation(target="foo", type=0),
            Relation(target="bar", type=0)
        ], senses[0].relations)

    def test_senses_with_entry_relations_multiple_senses_unchanged(self):
        entry = EntryFactory.build(
            senses=[
                SenseFactory.build(),
                SenseFactory.build()
            ],
            relations=[
                Relation(target="bar", type=0)
            ]
        )
        senses = entry.senses_with_entry_relations()
        self.assertIs(senses, entry.senses)

    def test_data(self):
        entry = EntryFactory.build(
            headwordLines=[
                HeadwordLineFactory.build(headword='test', alternativeSpellings=[])
            ],
            senses=[
                SenseFactory.build(formOf="xxx"),
                SenseFactory.build(formOf="xyz")
            ]
        )

        self.assertEqual(
            {
                'formOf': ['xxx', 'xyz'],
                'genders': [2],
                'headword': 'test'
            }, entry.data())


if __name__ == '__main__':
    from unittest import main

    main()
