from unittest import TestCase
from base_test import BaseTest
from lexi.models import Translation
from lexi.models.fts import BaseFTSModel, FTSHeadword, FTSExpressionBuilder
import json


class TestBaseFTSModel(BaseTest):
    def test_index_is_not_implemented(self):
        with self.assertRaises(NotImplementedError):
            BaseFTSModel.index()

    def test_quote_rank_params(self):
        self.assertEqual("'foo', 0, 1.0, 'bar', 1",
                         BaseFTSModel.quote_rank_params([('foo', 0, 1.0), ('bar', 1)]))


class TestFTSHeadword(BaseTest):
    def test_table_name(self):
        self.assertEqual('fts_headwords', FTSHeadword()._meta.table_name)

    def test_prefix_option_is_set(self):
        create_sql = ';'.join(FTSHeadword.sqlall())
        self.assertIn("prefix='1 2 3 4'", create_sql, 'prefix option not set in %s' % create_sql)

    def test_tokenize_option_is_set(self):
        create_sql = ';'.join(FTSHeadword.sqlall())
        self.assertIn('tokenize="unicode61"', create_sql,
                      'tokenize option not set in %s' % create_sql)

    def test_with_options(self):
        with FTSHeadword.with_options(tokenize='bar'):
            self.assertEqual({'prefix': "'1 2 3 4'", 'tokenize': 'bar'},
                             FTSHeadword._meta.options)
        self.assertEqual({'prefix': "'1 2 3 4'", 'tokenize': 'unicode61'},
                         FTSHeadword._meta.options)

    def test_reindex(self):
        self.reindex()
        self.assertEqual(3, FTSHeadword.select().count())

    def test_pos_is_part_of_fts_model(self):
        self.reindex()
        model = FTSHeadword.select().first()
        self.assertIsNotNone(model.pos)

    def test_headword_is_part_of_fts_model(self):
        self.reindex()
        model = FTSHeadword.select().first()
        self.assertIsNotNone(model)
        self.assertIsNotNone(model.headword)

    def test_headword_query_by_translation(self):
        self.reindex()

        self.assertEqual(['mesa'], [x.headword_text for x in FTSHeadword.headword_query('table')])
        self.assertEqual(['cása'], [x.headword_text for x in FTSHeadword.headword_query('house')])

    def test_headword_query_by_transliteration(self):
        self.reindex()

        self.assertEqual(['manège'],
                         [x.headword_text for x in FTSHeadword.headword_query('manesch')])

    def test_headword_query_by_headword(self):
        self.reindex()

        self.assertEqual(['mesa'], [x.headword_text for x in FTSHeadword.headword_query('mesa')])
        self.assertEqual(['cása'], [x.headword_text for x in FTSHeadword.headword_query('casa')])

    def test_headword_query_with_highlight(self):
        self.reindex()
        self.assertEqual(['*cása*'],
                         [r.highlight
                          for r in FTSHeadword.headword_query('casa', open='*', close='*')])

    def test_headword_query_with_highlight_translation(self):
        self.reindex()
        self.assertEqual(['["home", "*house*"]'],
                         [r.highlight_translation
                          for r in FTSHeadword.headword_query('house', open='*', close='*')])

    def run_query(self, query_func):
        for (word, score, translations) in [('x', 0, ['a', 'b']), ('y', 1, ['a', 'b'])]:
            self.generator.create_headword(word,
                                           translations=translations,
                                           score=score)

        FTSHeadword.reindex()

        return list(query_func('a'))

    def test_headword_query(self):
        results = self.run_query(lambda q: FTSHeadword.headword_query(q).tuples())
        self.assertEqual([
            ('y', 0, 1.0, 'y', '["<a>", "b"]'),
            ('x', 0, 0.0, 'x', '["<a>", "b"]')
        ], results)

    def test_headword_subquery(self):
        results = self.run_query(lambda q: FTSHeadword.headword_subquery(q))
        self.assertEqual([
            ('y', 0, 1.0, 'y', '["<a>", "b"]'),
            ('x', 0, 0.0, 'x', '["<a>", "b"]')
        ], results)

    def ignored_test_headword_olha_query(self):
        results = self.run_query(lambda q: FTSHeadword.headword_olha_query(q).tuples())
        self.assertEqual([
            ('x', 0, 0.0, 2.0, 'x', '["<a>", "b"]'),
            ('y', 0, 1.0, 2.0, 'y', '["<a>", "b"]')
        ], results)

    def test_index_serializes_translations_as_json(self):
        self.reindex()
        headword = FTSHeadword.get(FTSHeadword.rowid == 1)
        self.assertEqual(['home', 'house'], sorted(json.loads(headword.translations)))

    def test_translations_are_serialized_without_escape(self):
        self.reindex()
        manege = FTSHeadword.get(FTSHeadword.rowid == 3)
        self.assertEqual('manège', manege.headword)
        self.assertEqual('["manège"]', manege.translations)

    def reindex(self):
        for (word, score, translations, transliterations) in \
                [('cása', 0, ['house', 'home'], None),
                 ('mesa', 0, ['table'], None),
                 ('manège', 0, ['manège'], ['manesch'])]:

            data = {'transliterations': transliterations} if transliterations else None
            self.generator.create_headword(word, translations=translations, data=data, score=score)

        FTSHeadword.reindex()

    def test_ordered_translations(self):
        translations = [
            Translation(translation_text='bar'),
            Translation(translation_text='foo'),
            Translation(translation_text='foo'),
            Translation(translation_text='foo'),
            Translation(translation_text='baz'),
            Translation(translation_text='baz'),
        ]
        self.assertEqual(['foo', 'baz', 'bar'], FTSHeadword._ordered_translations(translations))


class FTSExpressionBuilderTest(TestCase):

    def test_none(self):
        self.assertIsNone(FTSExpressionBuilder.build(None))

    def test_empty_string(self):
        self.assertIsNone(FTSExpressionBuilder.build(""))

    def test_queries(self):
        tests = {
            'single word': ('foo', '"foo" OR "foo" *'),
            'two words': ('foo bar', '"foo bar" OR "foo bar" *'),
            'single quote': ('"', '"""" OR """" *'),
            'quoted string': ('"foo"', '"""foo""" OR """foo""" *')
        }
        for (test_name, (query, expected_result)) in tests.items():
            self.assertEqual(expected_result, FTSExpressionBuilder.build(query))


if __name__ == '__main__':
    from unittest import main

    main()
