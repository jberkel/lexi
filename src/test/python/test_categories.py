from base_test import BaseTest


from lexi.models import Category, HeadwordCategoryLink, EntryCategoryLink, SenseCategoryLink
from lexi.categories import top_headword_categories, top_sense_categories, top_entry_categories, \
    drop_categories


class TestCategories(BaseTest):
    def test_top_headword_categories(self):
        categories = top_headword_categories()
        self.assertEqual(0, len(categories))

    def test_top_sense_categories(self):
        categories = top_sense_categories()
        self.assertEqual(0, len(categories))

    def test_top_entry_categories(self):
        categories = top_entry_categories()
        self.assertEqual(0, len(categories))

    def test_drop_categories(self):
        headword = self.generator.create_headword()
        self.generator.create_headword_category_link(headword)
        self.generator.create_entry_category_link(headword)
        drop_categories()
        for model in [Category, HeadwordCategoryLink, EntryCategoryLink, SenseCategoryLink]:
            self.assertTrue(model.table_exists())
            self.assertEqual(0, model.select().count(), msg='%s still has entries' % model)
