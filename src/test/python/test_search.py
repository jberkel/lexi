from base_test import BaseTest
from lexi.search import single_search


class TestSearch(BaseTest):
    def setUp(self):
        super().setUp()
        self.reindex()

    def test_search(self):
        single_search('casa')

    def reindex(self):
        from lexi.models.fts import FTSHeadword
        for (word, translations) in [('casa', ['house', 'home']), ('mesa', ['table'])]:
            self.generator.create_headword(word,
                                           score=1,
                                           rank=1,
                                           translations=translations)

        FTSHeadword.reindex()


if __name__ == '__main__':
    from unittest import main

    main()
