from polyfactory import Use
from polyfactory.factories.msgspec_factory import MsgspecFactory

from lexi.serialization import Entry, Translation, Sense, Page, Relation, HeadwordLine


class SenseFactory(MsgspecFactory[Sense]):
    __random_seed__ = 10
    number = {
        "_1": 1,
        "_2": 0,
        "_3": 0,
        "_4": 0,
        "_5": 0,
    }


class RelationFactory(MsgspecFactory[Relation]):
    __random_seed__ = 10


class EntryFactory(MsgspecFactory[Entry]):
    __random_seed__ = 10
    senses = Use(SenseFactory.batch, size=1)
    partOfSpeech = Use(MsgspecFactory.__random__.choice,
                       ['Noun', 'Verb', 'Adjective', 'Phrase'])
    relations = Use(RelationFactory.batch, size=1)


class HeadwordLineFactory(MsgspecFactory[HeadwordLine]):
    __random_seed__ = 10


class TranslationFactory(MsgspecFactory[Translation]):
    __random_seed__ = 10
    partOfSpeech = "Noun"


class PageFactory(MsgspecFactory[Page]):
    __random_seed__ = 10
    entries = Use(EntryFactory.batch, size=1)
    translations = Use(TranslationFactory.batch, size=1)
    redirects: list[str] = []
