import unittest
from data_generator import DataGenerator


class BaseTest(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.generator = DataGenerator()
        self.generator.create_tables()

    def tearDown(self):
        super().tearDown()
        self.generator.close()
