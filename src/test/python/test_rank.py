import unittest

from lexi.models.jwktl import RelationType
from lexi.serialization import Page, Entry, Usex, Relation, Pronunciation
from lexi.rank import Ranker
from polyfactories import EntryFactory, SenseFactory, TranslationFactory


class TestRanker(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.ranker = Ranker()

    def test_score_empty_entry(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech="",
                                   pronunciations=[],
                                   etymology=None)
        self.assert_entry_score(entry, 0)

    def test_score_entry_with_pos_noun_lemma(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Noun',
                                   pronunciations=[],
                                   categories=['English lemmas'],
                                   etymology=None)
        self.assert_entry_score(entry, 1.0)

    def test_score_entry_with_pos_noun_non_lemma(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Noun',
                                   pronunciations=[],
                                   etymology=None)
        self.assert_entry_score(entry, 0.5)

    def test_score_pos_proper_noun(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Proper noun',
                                   pronunciations=[],
                                   categories=['English lemmas'],
                                   etymology=None)
        self.assert_entry_score(entry, 0)

    def test_score_pos_verb(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Verb',
                                   pronunciations=[],
                                   categories=['English lemmas'],
                                   etymology=None)
        self.assert_entry_score(entry, 0.8)

    def test_score_pos_verb_non_lemma(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Verb',
                                   pronunciations=[],
                                   categories=[],
                                   etymology=None)
        self.assert_entry_score(entry, 0.4)

    def test_score_pos_adjective(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Adjective',
                                   pronunciations=[],
                                   categories=['English lemmas'],
                                   etymology=None)
        self.assert_entry_score(entry, 0.7)

    def test_score_pos_adjective_non_lemma(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Adjective',
                                   pronunciations=[],
                                   categories=[],
                                   etymology=None)
        self.assert_entry_score(entry, .35)

    def test_score_pos_adverb(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Adverb',
                                   pronunciations=[],
                                   categories=['English lemmas'],
                                   etymology=None)
        self.assert_entry_score(entry, 0.7)

    def test_score_pos_phrase(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Phrase',
                                   pronunciations=[],
                                   categories=['English lemmas'],
                                   etymology=None)
        self.assert_entry_score(entry, 0.7)

    def test_score_pos_preposition(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Preposition',
                                   pronunciations=[],
                                   categories=['English lemmas'],
                                   etymology=None)
        self.assert_entry_score(entry, 0.7)

    def test_score_pos_pronoun(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Pronoun',
                                   pronunciations=[],
                                   categories=['English lemmas'],
                                   etymology=None)
        self.assert_entry_score(entry, 0.6)

    def test_score_pos_interjection(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech='Interjection',
                                   pronunciations=[],
                                   categories=['English lemmas'],
                                   etymology=None)
        self.assert_entry_score(entry, 0)

    def test_score_entry_entry_with_senses(self):
        entry = EntryFactory.build(senses=SenseFactory.batch(5, relations=[]),
                                   partOfSpeech="",
                                   pronunciations=[],
                                   categories=[],
                                   etymology=None)
        self.assert_entry_score(entry, 5.5)

    def test_score_entry_with_etymology(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech="",
                                   pronunciations=[],
                                   etymology="Etym")
        self.assert_entry_score(entry, 1.0)

    def test_score_entry_with_pronunciation(self):
        entry = EntryFactory.build(senses=[],
                                   partOfSpeech="",
                                   pronunciations=[Pronunciation(text="/foo/")],
                                   etymology=None)
        self.assert_entry_score(entry, 1)

    def test_score_entry_with_relations_synonyms(self):
        entry = EntryFactory.build(
            senses=SenseFactory.batch(1,
                                      relations=[
                                          Relation(type=RelationType.SYNONYM,
                                                   target="target")],
                                      usexes=[],
                                      categories=[]),
            partOfSpeech='',
            pronunciations=[],
            etymology=None)
        self.assert_entry_score(entry, 0.3)

    def test_score_entry_with_relations_antonyms(self):
        entry = EntryFactory.build(
            senses=SenseFactory.batch(1,
                                      relations=[
                                          Relation(type=RelationType.ANTONYM,
                                                   target="target")],
                                      usexes=[],
                                      categories=[]),
            partOfSpeech='',
            pronunciations=[],
            etymology=None)
        self.assert_entry_score(entry, 0.1)

    def test_score_entry_with_usex(self):
        entry = EntryFactory.build(
            senses=SenseFactory.batch(1,
                                      relations=[],
                                      usexes=[Usex("usex")],
                                      categories=[]),
            partOfSpeech='',
            pronunciations=[],
            categories=['English lemmas'],
            etymology=None)

        self.assert_entry_score(entry, 2.0)  # 1 sense w/ 1 example

    def test_score_entry_with_sense(self):
        entry = EntryFactory.build(
            senses=SenseFactory.batch(1,
                                      relations=[],
                                      usexes=[],
                                      categories=[]),
            partOfSpeech='',
            pronunciations=[],
            categories=['English lemmas'],
            etymology=None)

        self.assert_entry_score(entry, 1.0)  # 1 sense

    def test_score_page_with_translations(self):
        page = Page(
            pageTitle="foo",
            entries=[],
            redirects=[],
            translations=TranslationFactory.batch(2)
        )
        self.assert_page_score(page, 2.0)

    def assert_page_score(self, page: Page, expected_score):
        self.assertEqual(expected_score, self.ranker.score_page(page))

    def assert_entry_score(self, entry: Entry, expected_score):
        self.assertAlmostEqual(expected_score, self.ranker.score_entry(entry))


if __name__ == '__main__':
    from unittest import main

    main()
