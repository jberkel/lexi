import unittest
from unittest.mock import Mock
import os
from lexi.db import Database

dictionary_db = os.path.dirname(__file__) + '/../resources/dictionary.sqlite'


class TestDatabase(unittest.TestCase):

    def test_open_memory_db(self):
        db = Database(':memory:')
        db.connect()

    def test_open_as_memory_db(self):
        db = Database(dictionary_db, open_as_memory_db=True)
        self.assertTrue(db.open_as_memory_db)
        db.connect()
        connection = db.connection()
        self.assertEqual('', connection.filename)
        self.assertEqual(6, connection.open_flags & 6)

    def test_open_as_memory_db_defaults_to_false(self):
        db = Database(dictionary_db)
        self.assertFalse(db.open_as_memory_db)

    def test_s3client_option_enables_open_as_memory(self):
        s3client = Mock()
        db = Database("file://foo/test", s3client=s3client)
        self.assertTrue(db.open_as_memory_db)

    def test_s3client_enables_s3_vfs(self):
        s3client = Mock()
        db = Database("file://foo/test", s3client=s3client)
        self.assertIsNotNone(db.s3_vfs)
