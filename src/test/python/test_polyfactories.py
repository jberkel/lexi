import unittest

from lexi.models.jwktl import PartOfSpeech
from lexi.serialization import Entry
from polyfactories import EntryFactory


class EntryFactoryTest(unittest.TestCase):

    def test_build(self):
        entry: Entry = EntryFactory.build(
            pageTitle="test",
            partOfSpeech="Verb",
            usageNotes="note"
        )
        self.assertEqual("test", entry.pageTitle)
        self.assertEqual(PartOfSpeech.VERB, entry.pos())
        self.assertEqual("note", entry.usageNotes)
        self.assertEqual(1, len(entry.relations))
