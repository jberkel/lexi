import unittest

import boto3
import os
from io import BytesIO
from apsw import Connection, SQLITE_OPEN_READONLY, SQLITE_OPEN_URI
from lexi.db.vfs import S3VFS, LoggingVFS
from botocore.stub import Stubber
from botocore.response import StreamingBody
from unittest.mock import ANY


class S3VFSTest(unittest.TestCase):
    dictionary_bytes: bytes

    def setUp(self):
        s3 = boto3.client('s3')
        self.stubber = Stubber(s3)
        self.stubber.activate()
        self.s3_vfs = S3VFS(s3client=s3)
        self.logging_vfs = LoggingVFS()

        dictionary = os.path.dirname(
            __file__) + '/../resources/dictionary.sqlite'
        self.assertTrue(os.path.exists(dictionary))
        with open(dictionary, 'rb') as f:
            self.dictionary_bytes = f.read()
        super().setUp()

    def tearDown(self):
        super().tearDown()
        self.stubber.assert_no_pending_responses()
        self.stubber.deactivate()
        self.s3_vfs.unregister()

    def test_connect(self):
        self._stub_get(self.dictionary_bytes)
        self._stub_head(self.dictionary_bytes)
        self._stub_head(self.dictionary_bytes)
        connection = Connection('file://lexi-vfs-test/test.sqlite',
                                flags=SQLITE_OPEN_READONLY | SQLITE_OPEN_URI,
                                vfs=self.s3_vfs.vfsname)
        with connection:
            cursor = connection.cursor()
            count = list(cursor.execute("select count(*) from headwords"))[0]
            self.assertEqual((2,), count)

    def test_open_as_memory_db_s3_fs(self):
        self._stub_get(self.dictionary_bytes)
        self._stub_head(self.dictionary_bytes)
        self._stub_head(self.dictionary_bytes)

        connection = Connection('file://lexi-vfs-test/test.sqlite',
                                flags=SQLITE_OPEN_READONLY | SQLITE_OPEN_URI,
                                vfs=self.s3_vfs.vfsname)

        memory_con = Connection(':memory:', vfs=self.s3_vfs.vfsname)
        with memory_con.backup('main', connection, 'main') as backup:
            backup.step()

        cursor = memory_con.cursor()

        self._stub_put()
        cursor.execute("VACUUM INTO 'file://lexi-vfs-test/test-output.sqlite'")

    def test_open_connection_with_invalid_name_raises_exception(self):
        with self.assertRaisesRegex(Exception, r"^Invalid file:"):
            Connection('test.sqlite',
                       flags=SQLITE_OPEN_READONLY | SQLITE_OPEN_URI,
                       vfs=self.s3_vfs.vfsname)

    def _stub_get(self, data: bytes):
        self.stubber.add_response('get_object',
                                  service_response={
                                      'ContentType': 'application/x-sqlite3',
                                      'ContentLength': len(data),
                                      'Body': StreamingBody(
                                          raw_stream=BytesIO(data),
                                          content_length=len(data))
                                  },
                                  expected_params={'Bucket': 'lexi-vfs-test',
                                                   'Key': 'test.sqlite'})

    def _stub_head(self, data: bytes):
        self.stubber.add_response('head_object',
                                  service_response={
                                      'ContentType': 'application/x-sqlite3',
                                      'ContentLength': len(data),
                                  },
                                  expected_params={'Bucket': 'lexi-vfs-test',
                                                   'Key': 'test.sqlite'})

    def _stub_put(self):
        self.stubber.add_response('put_object',
                                  service_response={},
                                  expected_params={'Bucket': 'lexi-vfs-test',
                                                   'Key': 'test-output.sqlite',
                                                   'ContentType': 'application/octet-stream',
                                                   'Metadata': {},
                                                   'Body': ANY})


if __name__ == '__main__':
    unittest.main()
