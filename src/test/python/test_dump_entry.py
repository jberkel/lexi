from base_test import BaseTest
from lexi.dump_entry import print_entries


class TestDumpEntry(BaseTest):
    def test_print_entries(self):
        self.generator.create_headword('test')
        print_entries('test')
