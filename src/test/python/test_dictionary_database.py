import tempfile
import unittest

import apsw

from lexi.dictionary import DictionaryDatabase, SCHEMA_VERSION
from lexi.models import (Category, Example as LexiExample,
                         Entry as LexiEntry, EntryCategoryLink,
                         Headword, HeadwordCategoryLink, Metadata,
                         Relation as LexiRelation,
                         Sense as LexiSense, SenseCategoryLink,
                         Translation as LexiTranslation,
                         Pronunciation as LexiPronunciation)
from lexi.models.fts import FTSHeadword
from lexi.models.jwktl import PartOfSpeech, RelationType
from lexi.serialization import Relation
from lexi.rank import Ranker
from polyfactories import EntryFactory, PageFactory, SenseFactory


class DictionaryDatabaseTest(unittest.TestCase):

    def test_create_entry(self):
        with DictionaryDatabase(':memory:') as db:
            db.create_tables()
            db.create_entry(
                Headword.create(headword_text="test", pos=PartOfSpeech.NOUN,
                                score=0),
                EntryFactory.build(
                    pageTitle="test",
                    partOfSpeech="Verb",
                    usageNotes="note"
                )
            )

            entry: LexiEntry = LexiEntry.get(headword_text="test")

            self.assertEqual(entry.headword_text, "test")
            self.assertEqual(entry.usage_notes, "note")
            self.assertEqual(entry.pos, PartOfSpeech.VERB)

    def test_create_entry_with_int_pos(self):
        with DictionaryDatabase(':memory:') as db:
            db.create_tables()
            db.create_entry(
                Headword.create(headword_text="test", pos=PartOfSpeech.NOUN,
                                score=0),
                EntryFactory.build(
                    pageTitle="test",
                    partOfSpeech=-1,
                    usageNotes="note"
                )
            )

            entry: LexiEntry = LexiEntry.get(headword_text="test")
            self.assertEqual(entry.pos, -1)

    def test_make_dictionary(self):
        with tempfile.NamedTemporaryFile() as file:
            with DictionaryDatabase(':memory:') as db:
                pages = PageFactory.batch(3)
                db.make_dictionary(
                    dump_version='1234',
                    language_code='de',
                    language_name='German',
                    pages=pages
                )
                db.vacuum_into(file.name)

            with DictionaryDatabase(file.name):
                self.assertEqual(3, Headword.select().count())
                self.assertEqual(3, LexiEntry.select().count())
                self.assertEqual(3, LexiSense.select().count())
                self.assertEqual(3, LexiExample.select().count())
                self.assertEqual(6, Category.select().count())
                self.assertEqual(0, HeadwordCategoryLink.select().count())
                self.assertEqual(3, EntryCategoryLink.select().count())
                self.assertEqual(3, SenseCategoryLink.select().count())
                self.assertEqual(3, LexiPronunciation.select().count())
                self.assertEqual(3, LexiTranslation.select().count())
                self.assertEqual(6, Metadata.select().count())

    def test_make_dictionary_sets_metadata(self):
        with tempfile.NamedTemporaryFile() as file:
            with DictionaryDatabase(':memory:') as db:
                pages = PageFactory.batch(3)
                db.make_dictionary(
                    dump_version='1234',
                    language_code='de',
                    language_name='German',
                    pages=pages
                )
                db.vacuum_into(file.name)

            with DictionaryDatabase(file.name) as db:
                metadata = Metadata.as_dictionary()
                self.assertEqual('1234', metadata['dumpversion'])
                self.assertEqual(SCHEMA_VERSION, metadata['db-schema'])
                self.assertEqual('de', metadata['language'])
                self.assertEqual('German', metadata['language-name'])
                self.assertEqual(int(SCHEMA_VERSION[0:8]), db.pragma('user_version'))

    def test_make_dictionary_with_ranker(self):
        with DictionaryDatabase(':memory:') as db:
            pages = PageFactory.batch(3)
            db.make_dictionary(
                dump_version='1234',
                language_code='de',
                language_name='German',
                pages=pages,
                ranker=Ranker()
            )
            for headword in Headword.select():
                self.assertGreater(headword.score, 0)

    def test_entry_relations_are_added_to_single_sense(self):
        senses = SenseFactory.batch(1, relations=[
            Relation("entry2", RelationType.SYNONYM)])
        entry_relations = [Relation("entry3", RelationType.SYNONYM)]
        entries = EntryFactory.batch(1, senses=senses,
                                     relations=entry_relations)
        pages = [PageFactory.build(pageTitle="entry1", entries=entries)]

        with DictionaryDatabase(':memory:') as db:
            db.make_dictionary(
                dump_version='1234',
                language_code='de',
                language_name='German',
                pages=pages
            )

            sense = LexiSense.select()[0]
            self.assertEqual(['entry2', 'entry3'],
                             [r.target_headword_text for r in sense.relations])

    def test_link_relations(self):
        senses1 = SenseFactory.batch(1, relations=[
            Relation("entry2", RelationType.SYNONYM)])
        senses2 = SenseFactory.batch(1, relations=[
            Relation("entry1", RelationType.SYNONYM),
            Relation("entry666", RelationType.SYNONYM)])
        entries1 = EntryFactory.batch(1, senses=senses1, relations=[])
        entries2 = EntryFactory.batch(1, senses=senses2, relations=[])

        pages = [
            PageFactory.build(pageTitle="entry1", entries=entries1),
            PageFactory.build(pageTitle="entry2", entries=entries2)
        ]

        with DictionaryDatabase(':memory:') as db:
            db.make_dictionary(
                dump_version='1234',
                language_code='de',
                language_name='German',
                pages=pages
            )
            db.link_relations()

            rel1, rel2, rel3 = LexiRelation.select()[:3]

            self.assertIsNotNone(rel1.target_headword)
            self.assertEqual('entry2', rel1.target_headword.headword_text)

            self.assertIsNotNone(rel2.target_headword)
            self.assertEqual('entry1', rel2.target_headword.headword_text)

            self.assertIsNone(rel3.target_headword)

    def test_insert_redirects(self):
        page1 = PageFactory.build(pageTitle="page1",
                                  redirects=["page1_redirect1",
                                             "page1_redirect2"])

        with DictionaryDatabase(':memory:') as db:
            db.make_dictionary(
                dump_version='1234',
                language_code='de',
                language_name='German',
                pages=[page1]
            )
            self.assertEqual(3, Headword.select().count())

            h1, h2, h3 = Headword.select()[:3]
            self.assertEqual('page1', h1.headword_text)
            self.assertEqual('page1_redirect1', h2.headword_text)
            self.assertIsNone(h2.pos)
            self.assertIsNone(h2.score)
            self.assertEqual(h1, h2.redirect_headword)

            self.assertEqual('page1_redirect2', h3.headword_text)
            self.assertIsNone(h3.pos)
            self.assertIsNone(h3.score)
            self.assertEqual(h1, h3.redirect_headword)

    def test_register_ranking_function(self):
        self.custom_rank_called = False

        def custom_rank(api: apsw.FTS5ExtensionApi, *args) -> float:
            self.custom_rank_called = True
            return 0.0

        pages = PageFactory.batch(3)

        with DictionaryDatabase(':memory:') as db:
            db.register_ranking_function("custom_rank", custom_rank)
            db.make_dictionary(dump_version='1234', language_code='de', language_name='German', pages=pages)
            db.reindex()

            headwords = list(Headword.select())
            self.assertEqual(3, len(headwords))

            results = FTSHeadword.select(FTSHeadword.headword) \
                .where(FTSHeadword.match(headwords[0].headword_text)
                       & FTSHeadword.fts_rank('custom_rank()')) \
                .order_by(FTSHeadword.rank.desc())

            self.assertEqual(1, len(results))
            self.assertTrue(self.custom_rank_called)


if __name__ == '__main__':
    from unittest import main

    main()
