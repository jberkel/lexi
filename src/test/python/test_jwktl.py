from unittest import TestCase
from lexi.models.jwktl import PartOfSpeech, GrammaticalGender, PronunciationType, RelationType


class TestGrammaticalGender(TestCase):
    def test_from_string_m(self):
        self.assertEqual(GrammaticalGender.MASCULINE, GrammaticalGender.from_string('m'))

    def test_from_string_masculine(self):
        self.assertEqual(GrammaticalGender.MASCULINE, GrammaticalGender.from_string('masculine'))

    def test_from_string_f(self):
        self.assertEqual(GrammaticalGender.FEMININE, GrammaticalGender.from_string('f'))

    def test_from_string_feminine(self):
        self.assertEqual(GrammaticalGender.FEMININE, GrammaticalGender.from_string('feminine'))

    def test_from_string_n(self):
        self.assertEqual(GrammaticalGender.NEUTER, GrammaticalGender.from_string('n'))

    def test_from_string_neuter(self):
        self.assertEqual(GrammaticalGender.NEUTER, GrammaticalGender.from_string('neuter'))

    def test_from_string_common_gender(self):
        self.assertEqual(GrammaticalGender.COMMON, GrammaticalGender.from_string('c'))

    def test_from_string_animate(self):
        self.assertEqual(GrammaticalGender.ANIMATE, GrammaticalGender.from_string('anim'))

    def test_from_string_inanimate(self):
        self.assertEqual(GrammaticalGender.INANIMATE, GrammaticalGender.from_string('inan'))

    def test_from_string_not_found_returns_none(self):
        self.assertIsNone(GrammaticalGender.from_string('xxx'))

    def test_str(self):
        self.assertEqual('masculine', str(GrammaticalGender.MASCULINE))


class TestPartOfSpeech(TestCase):
    def test_from_string_noun(self):
        self.assertEqual(PartOfSpeech.NOUN, PartOfSpeech.from_string('noun'))
        self.assertEqual(PartOfSpeech.NOUN, PartOfSpeech.from_string('Noun'))

    def test_from_string_proper_noun(self):
        self.assertEqual(PartOfSpeech.PROPER_NOUN, PartOfSpeech.from_string('Proper noun'))

    def test_from_string_lowercase(self):
        self.assertEqual(PartOfSpeech.INTERJECTION, PartOfSpeech.from_string('interjection'))

    def test_from_string_mixed_case(self):
        self.assertEqual(PartOfSpeech.INTERJECTION, PartOfSpeech.from_string('InterJection'))

    def test_from_string_additions(self):
        self.assertEqual(PartOfSpeech.ROOT, PartOfSpeech.from_string('Root'))

    def test_from_string_not_found_returns_none(self):
        self.assertIsNone(PartOfSpeech.from_string('xxx'))

    def test_from_string_none_returns_none(self):
        self.assertIsNone(PartOfSpeech.from_string(None))

    def test_str(self):
        self.assertEqual('noun', str(PartOfSpeech.NOUN))
        self.assertEqual('proper_noun', str(PartOfSpeech.PROPER_NOUN))


class TestRelationType(TestCase):
    def test_from_string_synonym(self):
        self.assertEqual(RelationType.SYNONYM, RelationType.from_string('synonym'))

    def test_from_string_not_found_returns_none(self):
        self.assertIsNone(RelationType.from_string('xxx'))

    def test_str(self):
        self.assertEqual('synonym', str(RelationType.SYNONYM))


class TestPronunciationType(TestCase):
    def test_from_string_ipa(self):
        self.assertEqual(PronunciationType.IPA, PronunciationType.from_string('ipa'))

    def test_from_string_sampa(self):
        self.assertEqual(PronunciationType.SAMPA, PronunciationType.from_string('sampa'))

    def test_from_string_audio(self):
        self.assertEqual(PronunciationType.AUDIO, PronunciationType.from_string('audio'))

    def test_from_string_not_found_returns_none(self):
        self.assertIsNone(PronunciationType.from_string('xxx'))

    def test_str(self):
        self.assertEqual('IPA', str(PronunciationType.IPA))
        self.assertEqual('SAMPA', str(PronunciationType.SAMPA))
        self.assertEqual('audio', str(PronunciationType.AUDIO))


if __name__ == '__main__':
    from unittest import main

    main()
