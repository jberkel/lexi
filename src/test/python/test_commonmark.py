import unittest
from lexi.commonmark import link_target, FormatException


class CommonMarkTestCase(unittest.TestCase):

    def test_plain_target(self):
        self.assertEqual(('foo', 'foo'), link_target('foo'))

    def test_link(self):
        self.assertEqual(
            ('πᾰλῠ́νω', 'παλύνω'),
            link_target('[πᾰλῠ́νω](./παλύνω#Ancient_Greek "παλύνω")')
        )

    def test_encoded_link(self):
        self.assertEqual(
            ('ῥᾰντῐ́ζω', 'ῥαντίζω'),
            link_target('[ῥᾰντῐ́ζω](./%E1%BF%A5%CE%B1%CE%BD%CF%84%CE%AF%CE%B6%CF%89#Ancient_Greek "ῥαντίζω")')
        )

    def test_link_with_escaped_sequence(self):
        self.assertEqual(
            ('m/w/*', 'm/w/*'),
            link_target("""[m/w/\\*](./m/w/*#German "m/w/*")""")
        )

    def test_link_with_emphasis(self):
        self.assertEqual(
            ('Oh wench, disattire thyself', 'Oh_wench,_disattire_thyself'),
            link_target('[Oh wench, *disattire* thyself](./Oh_wench,_disattire_thyself)')
        )

    def test_link_with_strong(self):
        self.assertEqual(
            ('Oh wench, disattire thyself', 'Oh_wench,_disattire_thyself'),
            link_target('[Oh wench, **disattire** thyself](./Oh_wench,_disattire_thyself)')
        )

    def test_link_with_inline_code(self):
        self.assertEqual(
            ('Oh wench, disattire thyself', 'Oh_wench,_disattire_thyself'),
            link_target('[Oh wench, `disattire` thyself](./Oh_wench,_disattire_thyself)')
        )

    def test_link_with_html(self):
        self.assertEqual(
            ('strike', 'strike'),
            link_target('[<s>strike</s>](./strike)')
        )

    def test_empty_link_raises_format_exception(self):
        with self.assertRaises(FormatException):
            link_target('[](./-1#English "-1")')

    def test_list_is_returned_as_text(self):
        self.assertEqual(('5.', '5.'), link_target('5.'))

    def test_multiple_link_returns_none(self):
        self.assertIsNone(link_target('[a](./a) [b](./b)'))

    def test_mixed_link_and_text_returns_none(self):
        self.assertIsNone(link_target("""[oder](./oder#German "oder") **nicht**"""))

    def test_emphasis_returns_none(self):
        self.assertIsNone(link_target('**a**'))


if __name__ == '__main__':
    unittest.main()
