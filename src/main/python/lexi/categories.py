#!/usr/bin/env python
from peewee import fn

from .models import Category, Headword, Entry, Sense, HeadwordCategoryLink, \
    EntryCategoryLink, SenseCategoryLink


def top_headword_categories(n=20):
    return HeadwordCategoryLink.select(Category.category_text,
                                       fn.Count(Headword.id).alias('count')) \
        .join(Headword, on=Headword.id == HeadwordCategoryLink.headword) \
        .join(Category, on=Category.id == HeadwordCategoryLink.category) \
        .group_by(Category.category_text) \
        .order_by(fn.Count(Headword.id).desc()) \
        .limit(n) \
        .tuples()


def top_sense_categories(n=20):
    return SenseCategoryLink.select(Category.category_text,
                                    fn.Count(Sense.id).alias('count')) \
        .join(Sense, on=Sense.id == SenseCategoryLink.sense) \
        .join(Category, on=Category.id == SenseCategoryLink.category) \
        .group_by(Category.category_text) \
        .order_by(fn.Count(Sense.id).desc()) \
        .limit(n) \
        .tuples()


def top_entry_categories(n=20):
    return EntryCategoryLink.select(Category.category_text,
                                    fn.Count(Entry.id).alias('count')) \
        .join(Entry, on=Entry.id == EntryCategoryLink.entry) \
        .join(Category, on=Category.id == EntryCategoryLink.category) \
        .group_by(Category.category_text) \
        .order_by(fn.Count(Entry.id).desc()) \
        .limit(n) \
        .tuples()


def drop_categories():
    for model in [EntryCategoryLink, HeadwordCategoryLink, SenseCategoryLink, Category]:
        model.drop_table()
        model.create_table()


if __name__ == '__main__':
    import sys
    from .models import database_connection

    if len(sys.argv) > 1:
        with database_connection(sys.argv[1]):
            print("top headword categories\n")
            for (category, count) in top_headword_categories(20):
                print("\t%s => %d" % (category, count))

            print("\ntop entry categories\n")
            for (category, count) in top_entry_categories(20):
                print("\t%s => %d" % (category, count))

            print("\ntop sense categories\n")
            for (category, count) in top_sense_categories(20):
                print("\t%s => %d" % (category, count))
