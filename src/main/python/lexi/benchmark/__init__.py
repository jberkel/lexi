import cProfile
import io
import pstats
import time
import os

enabled = 'BENCHMARK' in os.environ


class Benchmark:
    def __init__(self, profile=False):
        self._last_run = 0.0
        self._iteration = 0
        self._size = 0
        self._total_size = 0
        self._profile: cProfile.Profile | None = None
        if profile:
            self._profile = cProfile.Profile()

    def __call__(self, size):
        self._size = size
        self._total_size += size
        return self

    def __enter__(self):
        if not enabled:
            return
        if self._last_run is None:
            self._last_run = time.time()

        if self._profile:
            self._profile.enable()

    def __exit__(self, exception_type, exception_val, trace):
        if not enabled:
            return
        now = time.time()
        entries_sec = self._size / (now - self._last_run)
        print("iteration #%d %.2f entries/s" % (self._iteration, entries_sec))
        self._last_run = now
        self._iteration += 1

        if self._profile:
            self._profile.disable()
            string_io = io.StringIO()
            stats = pstats.Stats(self._profile,
                                 stream=string_io).sort_stats(pstats.SortKey.CUMULATIVE)
            stats.print_stats()
            print(string_io.getvalue())
