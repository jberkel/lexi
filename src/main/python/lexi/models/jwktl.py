from enum import IntEnum


# from de.tudarmstadt.ukp.jwktl.api.PartOfSpeech
class PartOfSpeech(IntEnum):
    NOUN = 0
    PROPER_NOUN = 1
    FIRST_NAME = 2
    LAST_NAME = 3
    TOPONYM = 4
    SINGULARE_TANTUM = 5
    PLURALE_TANTUM = 6
    MEASURE_WORD = 7
    VERB = 8
    AUXILIARY_VERB = 9
    ADJECTIVE = 10
    ADVERB = 11
    INTERJECTION = 12
    SALUTATION = 13
    ONOMATOPOEIA = 14
    PHRASE = 15
    IDIOM = 16
    COLLOCATION = 17
    PROVERB = 18
    MNEMONIC = 19
    PRONOUN = 20
    PERSONAL_PRONOUN = 21
    REFLEXIVE_PRONOUN = 22
    POSSESSIVE_PRONOUN = 23
    DEMONSTRATIVE_PRONOUN = 24
    RELATIVE_PRONOUN = 25
    INDEFINITE_PRONOUN = 26
    INTERROGATIVE_PRONOUN = 27
    INTERROGATIVE_ADVERB = 28
    PARTICLE = 29
    ANSWERING_PARTICLE = 30
    NEGATIVE_PARTICLE = 31
    COMPARATIVE_PARTICLE = 32
    FOCUS_PARTICLE = 33
    INTENSIFYING_PARTICLE = 34
    MODAL_PARTICLE = 35
    ARTICLE = 36
    DETERMINER = 37
    ABBREVIATION = 38
    ACRONYM = 39
    INITIALISM = 40
    CONTRACTION = 41
    CONJUNCTION = 42
    SUBORDINATOR = 43
    PREPOSITION = 44
    POSTPOSITION = 45
    AFFIX = 46
    PREFIX = 47
    SUFFIX = 48
    PLACE_NAME_ENDING = 49
    LEXEME = 50
    CHARACTER = 51
    LETTER = 52
    NUMBER = 53
    NUMERAL = 54
    PUNCTUATION_MARK = 55
    SYMBOL = 56
    HANZI = 57
    KANJI = 58
    KATAKANA = 59
    HIRAGANA = 60
    GISMU = 61
    WORD_FORM = 62
    PARTICIPLE = 63
    TRANSLITERATION = 64
    # deprecated in JWKTL, but still used
    COMBINING_FORM = 65
    EXPRESSION = 66
    NOUN_PHRASE = 67
    # new additions
    PREPOSITIONAL_PHRASE = 68
    INTERFIX = 69
    INFIX = 70
    DIACRITICAL_MARK = 71
    ROOT = 72

    def __str__(self):
        return self.name.lower()

    @classmethod
    def from_string(cls, pos_string):
        if pos_string:
            return cls.__members__.get(pos_string.upper().replace(' ', '_'), None)
        else:
            return None


# de.tudarmstadt.ukp.jwktl.api.RelationType
class RelationType(IntEnum):
    SYNONYM = 0
    ANTONYM = 1
    HYPERNYM = 2
    HYPONYM = 3
    HOLONYM = 4
    MERONYM = 5
    COORDINATE_TERM = 6
    TROPONYM = 7
    SEE_ALSO = 8
    DERIVED_TERM = 9
    ETYMOLOGICALLY_RELATED_TERM = 10
    DESCENDANT = 11
    CHARACTERISTIC_WORD_COMBINATION = 12

    def __str__(self):
        return self.name.lower()

    @classmethod
    def from_string(cls, type_string):
        if type_string:
            return cls.__members__.get(type_string.upper(), None)
        else:
            return None


# de.tudarmstadt.ukp.jwktl.api.util.GrammaticalGender
class GrammaticalGender(IntEnum):
    MASCULINE = 0
    FEMININE = 1
    NEUTER = 2
    COMMON = 3
    INANIMATE = 4
    ANIMATE = 5

    @classmethod
    def from_string(cls, type_string):
        if type_string:
            if type_string == "m":
                return GrammaticalGender.MASCULINE
            elif type_string == "f":
                return GrammaticalGender.FEMININE
            elif type_string == "n":
                return GrammaticalGender.NEUTER
            elif type_string == "c":
                return GrammaticalGender.COMMON
            elif type_string == "inan":
                return GrammaticalGender.INANIMATE
            elif type_string == "anim":
                return GrammaticalGender.ANIMATE

            return cls.__members__.get(type_string.upper(), None)
        else:
            return None

    def __str__(self):
        return self.name.lower()


# de.tudarmstadt.ukp.jwktl.api.IPronunciation.PronunciationType
class PronunciationType(IntEnum):
    IPA = 0
    SAMPA = 1
    AUDIO = 2
    RHYME = 3

    def __str__(self):
        if self == PronunciationType.IPA:
            return "IPA"
        elif self == PronunciationType.SAMPA:
            return "SAMPA"
        elif self == PronunciationType.AUDIO:
            return "audio"
        else:
            return "none"

    @classmethod
    def from_string(cls, type_string):
        return cls.__members__.get(type_string.upper(), None)
