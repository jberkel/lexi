from peewee import sort_models
from lexi import models
from lexi.models import fts
import sys


if sys.getdefaultencoding() != 'utf-8':
    raise Exception('Need UTF-8 default encoding')


for model in sort_models(models.Models):
    for sql in model.sqlall():
        print('%s;' % sql)

for model in sort_models(fts.Models):
    for sql in model.sqlall():
        print('-- %s;' % sql)
