import os
from collections.abc import Generator
from contextlib import contextmanager
from typing import Any

import peewee
from peewee import Model, BooleanField, ForeignKeyField, \
    IntegerField, \
    TextField, CompositeKey, FloatField
from playhouse.shortcuts import model_to_dict
from playhouse.sqlite_ext import JSONField

from lexi.db import Database

if 'SHOW_SQL' in os.environ:
    import logging

    logger = logging.getLogger('peewee')
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter('%(asctime)-15s  %(message)s'))
    logger.addHandler(handler)

database = Database(None)
default_database_opts = {
    'pragmas': [
        ('foreign_keys', 'ON'),
        # delay until outermost transaction commits
        ('defer_foreign_keys', 'ON'),
        ('temp_store', 2),        # MEMORY, default 0 (FILE)
        ('synchronous', 0),       # OFF, default FULL (2)
        ('journal_mode', 'OFF'),  # default MEMORY
        ('cache_size', -16384),   # default -2000 (2 MB)
    ]
}

# https://gitlab.com/jberkel/olha/blob/master/CMakeLists.txt#L6
SQLITE_LIMIT_VARIABLE_NUMBER = 250000


@contextmanager
def database_connection(db_path: str, s3client: Any = None, **kwargs) -> Generator[Database, None, None]:
    database.init(db_path, s3client=s3client, **{**default_database_opts, **kwargs})
    database.connect()
    yield database
    database.close()


class BaseModel(Model):
    class Meta:
        database = database
        legacy_table_names = False

    @classmethod
    def just_insert(cls, __data=None, **insert) -> int:
        """Insert data and return id"""
        return cls.insert(__data, **insert).execute()

    def copy(self, **kwargs):
        """Creates a copy of the object, and inserts it into the database."""
        dict = model_to_dict(self, recurse=False, exclude=[self.__class__.id])
        dict.update(**kwargs)
        return self.__class__.create(**dict)

    @classmethod
    def sqlall(cls, safe=False):
        create_sql, _ = cls._schema._create_table(safe=safe).query()
        index_sql = [ctxt.query()[0] for ctxt in
                     cls._schema._create_indexes(safe=safe)]
        return [create_sql] + index_sql


class Headword(BaseModel):
    headword_text = TextField(unique=True)
    pos = IntegerField(null=True)
    score = FloatField(null=True)
    redirect_headword = ForeignKeyField('self', backref='redirects', null=True)

    @classmethod
    def query(cls, headword_text: str | None = None) -> peewee.ModelSelect:
        query = cls.select().where(cls.redirect_headword.is_null())
        if headword_text:
            query = query.where(Headword.headword_text == headword_text)
        return query

    @classmethod
    def headwords_with_translations(cls, headword=None) -> peewee.Query:
        return cls.query(headword).prefetch(Translation, Entry)

    class Meta:
        table_name = 'headwords'


class Etymology(BaseModel):
    etymology_text = TextField(null=False)

    class Meta:
        table_name = 'etymologies'


class Entry(BaseModel):
    headword_text = TextField(index=True, null=False)
    headword = ForeignKeyField(Headword, backref='entries', null=True)
    data = JSONField(null=True)
    etymology = ForeignKeyField(Etymology, null=True)
    pos = IntegerField(null=False)
    usage_notes = TextField(null=True)

    class Meta:
        table_name = 'entries'


class Pronunciation(BaseModel):
    pronunciation_text = TextField(null=False)
    type = IntegerField(null=False)
    note = TextField(null=True)
    headword = ForeignKeyField(Headword, backref='pronunciations', null=False)

    class Meta:
        table_name = 'pronunciations'

    def __str__(self):
        return "%s: %s %s" % (
            str(self.type),
            self.pronunciation_text,
            '(' + self.note + ')' if self.note else '')


class Sense(BaseModel):
    entry = ForeignKeyField(Entry, backref='senses', null=False)
    sense_text = TextField(null=False)
    # (1, 0, 0, 0, 0) = 1
    # (1, 1, 0, 0, 0) = 1 + (1 << 8) = 1 + 256 = 257
    # (2, 0, 0, 0, 0) = 2
    # (2, 1, 0, 0, 0) = 2 + (1 << 8) = 2 + 256 = 258
    # (2, 1, 1, 0, 0) = 2 + (1 << 8) + (1 << 16)  = 2 + 256 + 65536 = 65794
    number = IntegerField(null=True)

    class Meta:
        table_name = 'senses'

    @classmethod
    def pack_number(cls, number: tuple[int, int, int, int, int]) -> int:
        return (number[0] +
                (number[1] << 8) +
                (number[2] << 16) +
                (number[3] << 24) +
                (number[4] << 32)
                )

    @classmethod
    def unpack_number(cls, number: int) -> tuple[int, int, int, int, int]:
        return (number & 0x00000000ff,
                ((number & 0x000000ff00) >> 8),
                ((number & 0x0000ff0000) >> 16),
                ((number & 0x00ff000000) >> 24),
                ((number & 0xff00000000) >> 32),
                )


class Relation(BaseModel):
    entry = ForeignKeyField(Entry, backref='relations', null=False)
    sense = ForeignKeyField(Sense, backref='relations', null=True)
    type = IntegerField(null=False)
    target_headword_text = TextField(null=False)
    target_headword = ForeignKeyField(Headword, null=True)
    transliteration = TextField(null=True)

    class Meta:
        table_name = 'relations'
        indexes = (
            (('entry', 'sense', 'type', 'target_headword_text',
              'transliteration'), True),
        )


class Inflection(BaseModel):
    entry = ForeignKeyField(Entry, backref='inflections', null=False)
    type = IntegerField(null=False)
    data = JSONField(null=False)

    class Meta:
        table_name = 'inflections'


class Category(BaseModel):
    category_text = TextField(unique=True)

    class Meta:
        table_name = 'categories'


class HeadwordCategoryLink(BaseModel):
    headword = ForeignKeyField(Headword, backref='categories', null=False)
    category = ForeignKeyField(Category, null=False)

    class Meta:
        table_name = 'headword_category_links'
        primary_key = CompositeKey('headword', 'category')
        without_rowid = True


class EntryCategoryLink(BaseModel):
    entry = ForeignKeyField(Entry, backref='categories', null=False)
    category = ForeignKeyField(Category, null=False)

    class Meta:
        table_name = 'entry_category_links'
        primary_key = CompositeKey('entry', 'category')
        without_rowid = True


class SenseCategoryLink(BaseModel):
    sense = ForeignKeyField(Sense, backref='categories', null=False)
    category = ForeignKeyField(Category, null=False)

    class Meta:
        table_name = 'sense_category_links'
        primary_key = CompositeKey('sense', 'category')
        without_rowid = True


class Example(BaseModel):
    example_text = TextField(null=False)
    translation = TextField(null=True)
    transliteration = TextField(null=True)
    literally = TextField(null=True)
    sense = ForeignKeyField(Sense, backref='examples', null=False)

    class Meta:
        table_name = 'examples'


class Translation(BaseModel):
    # english translation
    translation_text = TextField(null=False)
    headword_text = TextField(null=False)
    headword = ForeignKeyField(Headword, backref='translations', null=True)
    pos = IntegerField(null=False)
    sense = TextField(null=True)
    check_needed = BooleanField(index=True, null=False, default=False)

    class Meta:
        table_name = 'translations'


class Metadata(BaseModel):
    DB_SCHEMA_KEY = 'db-schema'
    DUMP_VERSION = 'dumpversion'
    HEADWORDS = 'headwords'
    LANGUAGE = 'language'
    LANGUAGE_NAME = 'language-name'
    TRANSLATIONS = 'translations'

    key = TextField(null=False, unique=True, index=True)
    value = TextField(null=False)

    @classmethod
    def as_dictionary(cls) -> dict[str, str]:
        return dict([(data.key, data.value) for data in
                     cls.select(cls.key, cls.value)])

    class Meta:
        table_name = 'metadata'


Models: list[type[BaseModel]] = [Category, HeadwordCategoryLink, EntryCategoryLink, SenseCategoryLink,
                                 Example,
                                 Entry, Etymology, Headword, Inflection, Metadata, Pronunciation, Relation, Sense,
                                 Translation]
