from peewee import prefetch

from . import Entry, Etymology, Example, Headword, Pronunciation, \
    Relation, Sense, Translation


def prefetch_headwords(headword_query):
    return prefetch(headword_query,
                    Translation,
                    Pronunciation,
                    Entry,
                    Etymology.select(Etymology.id),
                    # preload relations with corresponding headword
                    Relation, (Headword, Relation),
                    # preload senses for entries with examples
                    (Sense, Entry), Example
                    )
