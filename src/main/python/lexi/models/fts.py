import json
from contextlib import contextmanager

from peewee import fn, SQL, Entity, Query, EnclosedNodeList, NodeList

from playhouse.sqlite_ext import FTS5Model, SearchField, match
from . import database, Headword
from ..benchmark import Benchmark
from itertools import groupby

DEFAULT_BATCH_SIZE = 1000


# subclass from FTSModel to use FTS4 instead of FTS5.
class BaseFTSModel(FTS5Model):
    @classmethod
    def sqlall(cls, safe=False):
        create_sql, _ = cls._schema._create_table(safe=safe).query()
        index_sql = [ctxt.query()[0] for ctxt in cls._schema._create_indexes(safe=safe)]
        return [create_sql] + index_sql

    @classmethod
    def recreate(cls):
        cls.drop_table(fail_silently=True)
        cls.create_table()

    @classmethod
    def reindex(cls, batch_size=DEFAULT_BATCH_SIZE):
        cls.recreate()
        cls.index(batch_size)
        cls.optimize()

    @classmethod
    @contextmanager
    def with_options(cls, **options):
        from copy import deepcopy
        copied_options = deepcopy(cls._meta.options)
        cls._meta.options.update(**options)
        try:
            yield
        finally:
            cls._meta.options = copied_options

    @classmethod
    def index(cls, batch_size=DEFAULT_BATCH_SIZE):
        raise NotImplementedError

    @classmethod
    def quote_rank_params(cls, params):
        def q(s):
            if isinstance(s, str):
                return "'%s'" % s
            else:
                return str(s)
        return ', '.join([q(x) for p in params for x in p])

    @classmethod
    def fts_rank(cls, rank_function):
        """
        Sorting by Auxiliary Function Results
            https://www.sqlite.org/fts5.html#section_5_2
        :param rank_function: the rank function to use
        :return:
        """
        return match(Entity('rank'), Entity(rank_function))

    # The 'optimize' Command: https://www.sqlite.org/fts5.html#section_6_7
    @classmethod
    def optimize(cls):
        return cls._fts_cmd('optimize')

    @classmethod
    def _fts_cmd_sql(cls, cmd, **extra_params):
        tbl = cls._meta.entity
        columns = [tbl]
        values = [cmd]
        for key, value in extra_params.items():
            columns.append(Entity(key))
            values.append(value)

        return NodeList((
            SQL('INSERT INTO'),
            cls._meta.entity,
            EnclosedNodeList(columns),
            SQL('VALUES'),
            EnclosedNodeList(values)))

    @classmethod
    def _fts_cmd(cls, cmd, **extra_params):
        query = cls._fts_cmd_sql(cmd, **extra_params)
        return cls._meta.database.execute(query)


class FTSHeadword(BaseFTSModel):
    headword = SearchField()
    # JSON array of translations, ordered by number of occurrences, alphabetical
    translations = SearchField()
    transliterations = SearchField()  # JSON array of transliterations
    pos = SearchField(unindexed=True)
    score = SearchField(unindexed=True)
    # ⚠️ add new fields after score
    # spellings = SearchField()         # JSON array of spellings
    rank = SQL('rank')  # computed by custom rank function
    olhaDefaultRankParams = [
        ('scoreColumn', 4),
        ('phraseWeightings', 5.0)
    ]

    class Meta:
        table_name = 'fts_headwords'
        database = database
        options = {
            'prefix': "'1 2 3 4'",
            'tokenize': "unicode61"
        }

    @classmethod
    def headword_query(cls, query: str, open='<', close='>', limit=10, offset=0) -> Query:
        return Headword \
            .select(Headword.headword_text,
                    Headword.pos,
                    Headword.score,
                    fn.highlight(cls._meta.entity, 0, open, close).alias('highlight'),
                    fn.highlight(cls._meta.entity, 1, open, close).alias('highlight_translation')) \
            .join(cls, on=Headword.id == cls.rowid) \
            .where(cls.match(query)) \
            .limit(limit) \
            .offset(offset) \
            .order_by(Headword.score.desc())

    @classmethod
    def headword_olha_query(cls, query: str, open_tag='<', close_tag='>', limit=10, offset=0,
                            rank_param=olhaDefaultRankParams, rank_function='olhaRank') -> Query:
        fts_expression = FTSExpressionBuilder.build(query)
        entity = cls._meta.entity
        return cls \
            .select(cls.headword,
                    cls.pos,
                    cls.score,
                    cls.rank,
                    fn.highlight(entity, 0, open_tag, close_tag).alias('highlight'),
                    fn.highlight(entity, 1, open_tag, close_tag).alias('highlight_translation')) \
            .where(cls.match(fts_expression) & cls.fts_rank('%s(%s)' %
                                                            (rank_function, cls.quote_rank_params(rank_param)))) \
            .limit(limit) \
            .order_by(cls.rank.desc()) \
            .offset(offset)

    @classmethod
    def headword_subquery(cls, query: str, open='<', close='>', limit=10, offset=0) -> Query:
        # Performance optimization, as recommended per https://www.sqlite.org/fts3.html#appendix_a
        #
        # > %timeit list(FTSHeadword.headword_query('a*').tuples())
        # 10 loops, best of 3: 114 ms per loop
        # > %timeit list(FTSHeadword.headword_subquery('a*'))
        # 10 loops, best of 3: 47.4 ms per loop

        # SELECT headword, pos, rank_table.score FROM fts_headwords
        # JOIN (
        #    SELECT fts_headwords.rowid, rank.score AS score FROM fts_headwords
        #       JOIN headwords
        #       ON fts_headwords.rowid = headword.id
        #       WHERE fts_headwords MATCH '<query>'
        #       ORDER BY score DESC
        #       LIMIT 10
        #       OFFSET 0
        # ) AS rank_table ON rank_table.rowid = fts_headwords.rowid
        # WHERE fts_headwords MATCH '<query>'
        # ORDER BY rank_table.score DESC
        join_q = cls.select(cls.rowid, Headword.score) \
            .join(Headword, on=(Headword.id == cls.rowid)) \
            .where(cls.match(query)) \
            .order_by(Headword.score.desc()) \
            .limit(limit) \
            .offset(offset)

        return cls \
            .select(
                cls.headword,
                cls.pos,
                join_q.c.score,
                fn.highlight(cls._meta.entity, 0, open, close),
                fn.highlight(cls._meta.entity, 1, open, close)
            ) \
            .join(join_q, on=(cls.rowid == join_q.c.rowid)) \
            .where(cls.match(query)) \
            .order_by(join_q.c.score.desc()) \
            .tuples()

    @classmethod
    def index(cls, batch_size=DEFAULT_BATCH_SIZE):
        with database.atomic():
            benchmark = Benchmark()
            for batch in cls._batched(cls._index_generator(), batch_size=batch_size):
                with benchmark(len(batch)):
                    cls.insert_many(batch).execute()

    @classmethod
    def _index_generator(cls):
        for headword in Headword.headwords_with_translations():
            translations = cls._ordered_translations(headword.translations)
            yield cls._headword_dict(headword.id,
                                     headword=headword,
                                     translations=translations,
                                     transliterations={transliteration
                                                       for entry in headword.entries
                                                       if entry.data is not None
                                                       for transliteration in
                                                       entry.data.get('transliterations', [])},
                                     spellings={spelling
                                                for entry in headword.entries
                                                if entry.data is not None
                                                for spelling in
                                                entry.data.get('spellings', [])}
                                     )

    @staticmethod
    def _ordered_translations(translations):
        """translations, ordered by number of translations"""
        return [e[1] for e in
                sorted([(-len(list(elements)), key) for (key, elements) in
                        groupby(translations, key=lambda x: x.translation_text)])]

    @classmethod
    def _headword_dict(cls, rowid, headword, translations, transliterations, spellings):
        return {
            cls.rowid: rowid,
            cls.headword: headword.headword_text,
            cls.pos: headword.pos,
            cls.score: headword.score,
            cls.translations: cls._format_list(translations),
            cls.transliterations: cls._format_list(transliterations),
            # cls.spellings: cls._format_list(spellings)
        }

    @staticmethod
    def _format_list(items):
        if len(items) > 0:
            return json.dumps([item for item in items], ensure_ascii=False)
        else:
            return None

    @staticmethod
    def _batched(generator, batch_size):
        from itertools import count, groupby
        c = count()
        for _, g in groupby(generator, lambda x: next(c) // batch_size):
            yield list(g)


class FTSExpressionBuilder(object):
    """
        /*
        <phrase>    := string [*]
        <phrase>    := <phrase> + <phrase>
        <neargroup> := NEAR ( <phrase> <phrase> ... [, N] )
        <query>     := [ [-] <colspec> :] <phrase>
        <query>     := [ [-] <colspec> :] <neargroup>
        <query>     := ( <query> )
        <query>     := <query> AND <query>
        <query>     := <query> OR <query>
        <query>     := <query> NOT <query>
        <colspec>   := colname
        <colspec>   := { colname1 colname2 ... }

        Within an FTS expression a string may be specified in one of two ways:
            * Within a string, any embedded double quote characters may be escaped SQL-style
               - by adding a second double-quote character.
            * As an FTS5 bareword that is not "AND", "OR" or "NOT" (case sensitive).

        https://www.sqlite.org/fts5.html#section_3
    */
    """
    @classmethod
    def build(cls, user_input: str | None) -> str | None:
        """
            :param user_input the raw user input
            :return the query or None
        """
        if user_input is None or len(user_input.strip()) == 0:
            return None

        quoted = user_input.replace('"', '""')
        return '"%s" OR "%s" *' % (quoted, quoted)


Models: list[type[FTS5Model]] = [FTSHeadword]
