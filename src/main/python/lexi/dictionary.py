#!/usr/bin/env python
import itertools
import os.path
from collections.abc import Iterator, Iterable
from io import open
from itertools import chain
from lexi.benchmark import Benchmark
from typing import Callable
import apsw
import apsw.fts5
import peewee

from lexi.models import (Category as LexiCategory, Headword, Entry as LexiEntry,
                         EntryCategoryLink,
                         Example, Etymology,
                         Inflection as LexiInflection,
                         Metadata,
                         Translation as LexiTranslation,
                         Relation as LexiRelation,
                         Sense as LexiSense,
                         SenseCategoryLink,
                         Pronunciation as LexiPronunciation, database, Models)
from lexi.rank import Ranker
from .commonmark import link_target
from .models.fts import Models as FTSModels, FTSHeadword
from .models.jwktl import PartOfSpeech, PronunciationType
from .serialization import Page, Entry, Translation, Sense, Relation, Usex, Inflections

SCHEMA_VERSION = "202409060000"


class DictionaryDatabase:
    def __init__(self, path: str, **kwargs):
        self._database = database
        self._database.init(path, **kwargs)
        self._categories: dict[str, int] = {}
        self._headwords: set[str] = set()

    def __enter__(self):
        self._database.connect()
        return self

    def __exit__(self, exception_type, exception_val, trace):
        self._database.close()

    def register_ranking_function(self, name: str, function: str | Callable) -> None:
        apsw.fts5.register_functions(self._database.connection(), {name: function})

    def make_dictionary(self,
                        dump_version: str,
                        language_code: str,
                        language_name: str,
                        pages: Iterable[Page],
                        ranker: Ranker | None = None,
                        batch_size=15_000,
                        allow_empty=False):
        self.create_tables()
        self.drop_indexes()

        benchmark = Benchmark()
        for (i, batch) in zip(itertools.count(1), itertools.batched(pages, batch_size)):
            with benchmark(batch_size):
                self._database.begin()

                for page in batch:
                    if not page.entries:
                        print(f"no entries in {page.pageTitle}")
                        continue

                    if page.is_translation():
                        print(f"skipping translation page {page.pageTitle}")
                        continue

                    pos = page.entries[0].pos()
                    if pos is None:
                        print(f"unknown POS: {page.entries[0].partOfSpeech}")
                        pos = PartOfSpeech.NOUN

                    score = ranker.score_page(page) if ranker else 0

                    headword_id = self._add_headword(headword_text=page.pageTitle,
                                                     pos=pos,
                                                     score=score)
                    if not headword_id:
                        continue

                    for redirect in page.redirects:
                        self._add_headword(headword_text=redirect,
                                           redirect_headword=headword_id)

                    # ensure that forms-only entries are inserted last
                    created_entries = [
                        created
                        for entry in sorted(page.entries, key=Entry.forms_sort_key) if
                        (created := self.create_entry(headword_id, entry, allow_empty=allow_empty))
                    ]

                    if created_entries and page.translations:
                        for translation in page.translations:
                            self._add_translation(headword_id, page.pageTitle, translation)

                self._database.commit()

            print(f"imported {i * batch_size} entries")

        self._insert_metadata(dump_version=dump_version,
                              schema_version=SCHEMA_VERSION,
                              language_code=language_code,
                              language_name=language_name)
        self.create_indexes()

        self.pragma('user_version', int(SCHEMA_VERSION[:8]))

    def create_tables(self):
        for models in [Models, FTSModels]:
            self._database.drop_tables(models, safe=True)
            self._database.create_tables(models)

    @staticmethod
    def drop_indexes():
        for models in [Models, FTSModels]:
            for model in models:
                model._schema.drop_indexes()

    @staticmethod
    def create_indexes():
        for models in [Models, FTSModels]:
            for model in models:
                model._schema.create_indexes()

    def reindex(self, **options):
        with FTSHeadword.with_options(**options):
            FTSHeadword.reindex()

    def pragma(self, key: str, value=peewee.SENTINEL):
        return self._database.pragma(key, value)

    def vacuum_into(self, output: str):
        if os.path.exists(output):
            os.unlink(output)
        self._database.vacuum_into(output)

    def vacuum(self):
        self._database.execute_sql('VACUUM')

    def create_entry(self, headword_id: int, entry: Entry, allow_empty=False) -> int | None:
        if entry.is_translation_page() or entry.is_translation_hub():
            return None

        pos = entry.pos()
        if pos is None:
            return None
        # assert pos is not None, f"unknown pos {entry.partOfSpeech} for {entry}"

        if not entry.headwordLines and not allow_empty:
            print(f"no headword lines in <{entry.pageTitle}>, skipping")
            return None

        if not entry.senses and not allow_empty:
            print(f"no senses in <{entry.pageTitle}>, skipping")
            return None

        self._insert_pronunciations(entry, headword_id)

        entry_id = LexiEntry.just_insert(headword_text=entry.pageTitle,
                                         pos=pos,
                                         data=entry.data(),
                                         etymology=self._insert_etymology(entry),
                                         usage_notes=entry.usageNotes,
                                         headword=headword_id)

        if len(entry.categories) > 0:
            self._insert_entry_categories(entry_id, entry.categories)

        self._insert_inflections(entry_id, entry.inflections)
        self._insert_senses(entry_id, entry.senses_with_entry_relations())

        return entry_id

    def link_relations(self, batch_size=2000):
        for batch in self._fetch_relations(batch_size=batch_size):
            self._database.begin()

            for relation in batch:
                match link_target(relation.target_headword_text):
                    case (title, target):
                        if headword := Headword.get_or_none(headword_text=target):
                            relation.target_headword = headword
                            relation.target_headword_text = title
                    case None:
                        print(
                            f"no link target for <{relation.target_headword_text}> in "
                            f"<{relation.entry.headword_text}>")

            # can't use bulk_update() here because we need to handle duplicate keys
            for relation in batch:
                try:
                    relation.save()
                except peewee.IntegrityError:
                    print(
                        f"duplicate relation <{relation.target_headword_text}> in "
                        f"entry <{relation.entry.headword_text}>")

            self._database.commit()

    def _add_headword(self, **args) -> int | None:
        headword_text = args['headword_text']
        if headword_text in self._headwords:
            print(f"duplicate headword {headword_text}, skipping insert")
            return None
        else:
            self._headwords.add(headword_text)
            return Headword.just_insert(**args)

    @staticmethod
    def _add_translation(headword_id: int, headword_text: str, translation: Translation):
        LexiTranslation.just_insert(headword=headword_id,  # dictionary language
                                    headword_text=headword_text,
                                    pos=PartOfSpeech.from_string(translation.partOfSpeech),
                                    translation_text=translation.headword,  # English
                                    sense=translation.gloss)

    @staticmethod
    def _insert_etymology(entry: Entry) -> int | None:
        return Etymology.just_insert(etymology_text=etymology) if (
            etymology := entry.etymology
        ) else None

    @staticmethod
    def _insert_pronunciations(entry: Entry, headword_id: int):
        for pronunciation in entry.pronunciations:
            if pronunciation.qualifier:
                for qualifier in pronunciation.qualifier:
                    LexiPronunciation \
                        .just_insert(type=PronunciationType.IPA,
                                     pronunciation_text=pronunciation.text,
                                     note=qualifier,
                                     headword=headword_id)
            else:
                LexiPronunciation.just_insert(type=PronunciationType.IPA,
                                              pronunciation_text=pronunciation.text,
                                              headword=headword_id)

    def _insert_inflections(self, entry_id: int, inflections: Inflections):
        if data := [x.to_data() for x in inflections.conjugations]:
            LexiInflection.just_insert(entry_id=entry_id, type=0, data=data)

    def _insert_senses(self, entry_id: int, senses: list[Sense]):
        # use raw SQL for better performance
        sql = "INSERT INTO senses (entry_id, sense_text, number) VALUES (?, ?, ?)"

        for sense in senses:
            packed_number = LexiSense.pack_number(sense.number_tuple())
            cursor = self._database.execute_sql(sql, (entry_id,
                                                      sense.definition,
                                                      packed_number))
            lexi_sense = self._database.last_insert_id(cursor)
            cursor.close()

            if len(sense.relations) > 0:
                self._insert_relations(entry_id, lexi_sense, sense.relations)
            if len(sense.usexes) > 0:
                self._insert_usexes(lexi_sense, sense.usexes)
            if len(sense.categories) > 0:
                self._insert_sense_categories(lexi_sense, sense.categories)

    @staticmethod
    def _insert_relations(entry_id: int,
                          lexi_sense: int,
                          relations: list[Relation]):
        uniq_relations = []
        for relation in relations:
            if relation not in uniq_relations:
                uniq_relations.append(relation)

        if len(relations) > 2:
            rows = [(relation.type, entry_id, lexi_sense,
                     relation.target, relation.transliteration) for relation in uniq_relations]
            LexiRelation.insert_many(
                rows=rows,
                fields=[LexiRelation.type,
                        LexiRelation.entry,
                        LexiRelation.sense,
                        LexiRelation.target_headword_text,
                        LexiRelation.transliteration]
            ).execute()
        else:
            for relation in uniq_relations:
                LexiRelation.just_insert(type=relation.type,
                                         entry=entry_id,
                                         sense_id=lexi_sense,
                                         target_headword_text=relation.target,
                                         transliteration=relation.transliteration)

    @staticmethod
    def _insert_usexes(lexi_sense: int, usexes: list[Usex]):
        for usex in usexes:
            Example.just_insert(example_text=usex.text,
                                transliteration=usex.transliteration,
                                translation=usex.translation,
                                literally=usex.literally,
                                sense=lexi_sense)

    def _insert_sense_categories(self,
                                 sense_id: int,
                                 categories: list[str]):
        self._insert_categories(SenseCategoryLink, sense_id, categories,
                                (SenseCategoryLink.sense, SenseCategoryLink.category))

    def _insert_entry_categories(self,
                                 entry_entry: int,
                                 categories: list[str]):
        self._insert_categories(EntryCategoryLink, entry_entry, categories,
                                (EntryCategoryLink.entry, EntryCategoryLink.category))

    def _insert_categories(self,
                           category_type,
                           id: int,
                           categories: list[str],
                           fields):
        if not categories:
            return
        category_ids = (self._create_category(name) for name in set(categories))
        rows = ((id, category_id) for category_id in category_ids)

        columns = ', '.join([field.column_name for field in fields])
        placeholders = ', '.join(['?' for _ in fields])
        sql = f"INSERT INTO {category_type._meta.table_name} ({columns}) VALUES ({placeholders})"

        for row in rows:
            cursor = self._database.execute_sql(sql, row)
            cursor.close()

    def _create_category(self, category_name: str) -> int:
        if category_name in self._categories:
            return self._categories[category_name]
        else:
            category_id = LexiCategory.just_insert(category_text=category_name)
            self._categories[category_name] = category_id
            return category_id

    @staticmethod
    def _insert_metadata(dump_version: str,
                         schema_version: str,
                         language_code: str,
                         language_name: str):
        metadata = {
            Metadata.DB_SCHEMA_KEY: schema_version,
            Metadata.LANGUAGE: language_code,
            Metadata.LANGUAGE_NAME: language_name,
            Metadata.DUMP_VERSION: dump_version,
            Metadata.HEADWORDS: Headword.select().count(),
            Metadata.TRANSLATIONS: LexiTranslation.select().count()
        }
        for (key, value) in metadata.items():
            Metadata.create(key=key, value=value)

    @staticmethod
    def _fetch_relations(batch_size: int) -> Iterator[list[LexiRelation]]:
        page = 1
        while True:
            if relations := (
                LexiRelation.select()
                    .order_by(LexiRelation.id)
                    .paginate(page, paginate_by=batch_size)):
                page += 1
                yield list(relations)
            else:
                break


def _load_data(json_path: str) -> Iterator[Page]:
    with open(json_path, 'r') as fileinput:
        print(f"reading {json_path}")
        if json_path.endswith(".ndjson"):
            for line in fileinput:
                yield Page.from_json(line)
        else:
            yield Page.from_json(fileinput.read())


def _load_pages(paths: list[str]) -> Iterator[Page]:
    return chain.from_iterable(_load_data(p) for p in paths)


if __name__ == '__main__':
    from lexi import LANGUAGES
    import argparse

    parser = argparse.ArgumentParser(description='Create dictionary')
    parser.add_argument('content.ndjson', help='The dictionary content',
                        nargs='*')
    parser.add_argument('--language', help='The language code', required=True)
    parser.add_argument('--dump-version', help='The dump version',
                        required=True)
    parser.add_argument('--index', help='Index the content',
                        action='store_true')
    parser.add_argument('--rank', help='Run the ranking',
                        action='store_true')
    parser.add_argument('--link', help='Link relations',
                        action='store_true')
    parser.add_argument('--allow-empty', help='Allow entries without senses',
                        action='store_true')
    parser.add_argument('--out', help='The database output', required=True)
    parser.add_argument('--tokenize-options')

    args = vars(parser.parse_args())

    with DictionaryDatabase(':memory:') as db:
        db.make_dictionary(
            dump_version=args['dump_version'],
            language_code=args['language'],
            language_name=LANGUAGES.get(args['language']),
            pages=_load_pages(args['content.ndjson']),
            ranker=Ranker() if args['rank'] else None,
            allow_empty=args['allow_empty']
        )

        if args['link']:
            db.link_relations()

        if args['index']:
            options = {}
            if args['tokenize_options']:
                options['tokenize'] = args['tokenize_options']
            db.reindex(**options)

        db.vacuum_into(args['out'])
