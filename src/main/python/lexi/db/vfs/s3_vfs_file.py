from io import BytesIO
from apsw import SQLITE_OPEN_CREATE
from botocore.exceptions import ClientError


# noinspection PyPep8Naming
class S3VFSFile:
    bucket: str
    key: str
    body: bytes | None

    def __init__(self, bucket: str, key: str, s3client, flags: int,
                 content_type: str | None = None, metadata: dict | None = None):
        self.bucket = bucket
        self.key = key
        self.s3client = s3client
        self.content_type = content_type or 'application/octet-stream'
        self.flags = flags
        self.metadata = metadata or {}
        self.output_buffer: BytesIO = BytesIO()
        self.body = None

        print(f"Initiated S3 VFS for file: s3://{self.bucket}/{self.key} ({flags=} {content_type=}, {metadata=})")

    def xRead(self, amount: int, offset: int) -> bytes:
        """
        Read the specified *amount* of data starting at *offset*. You
        should make every effort to read all the data requested, or return an
        error. If you have the file open for non-blocking I/O or if signals
        happen then it is possible for the underlying operating system to do
        a partial read. You will need to request the remaining data. Except
        for empty files SQLite considers short reads to be a fatal error.

        :param amount: Number of bytes to read
        :param offset: Where to start reading. This number may be 64 bit once
                the database is larger than 2GB.
        :return: bytes, buffer
        """

        # print(f"S3: xRead({self.key=}, {amount=}, {offset=})")
        if self.flags & SQLITE_OPEN_CREATE:
            self.output_buffer.seek(offset)
            return self.output_buffer.read(amount)
        elif self.body is None:
            try:
                print(f"S3: s3client.get_object({self.bucket=}, {self.key=})")
                response = self.s3client.get_object(Bucket=self.bucket, Key=self.key)
                self.body = response['Body'].read()
            except ClientError as e:
                if 'Error' in e.response and e.response['Error'].get('Code', None) == 'InvalidRange':
                    return b""  # short read
                else:
                    raise e

        return self.body[offset:offset + amount]

    def xWrite(self, data: bytes, offset: int) -> None:
        """
        Write the *data* starting at absolute *offset*. You must write all
        the data requested, or return an error. If you have the file open for
        non-blocking I/O or if signals happen then it is possible for the
        underlying operating system to do a partial write. You will need to
        write the remaining data.

        :param data: bytes
        :param offset: Where to start writing. This number may be 64 bit once
        the database is larger than 2GB.
        """
        # print(f"S3: xWrite({self.key=}, {len(data)=}, {offset=})")
        self.output_buffer.seek(offset)
        self.output_buffer.write(data)

    def xFileSize(self) -> int:
        """
        :return: the size of the file in bytes. Remember that file sizes are 64
                 bit quantities even on 32 bit operating systems.
        """
        print(f"S3: xFileSize({self.key=})")
        if self.flags & SQLITE_OPEN_CREATE:
            fileSize = len(self.output_buffer.getbuffer())
        else:
            print(f"S3: s3client.head_object({self.key=})")
            response = self.s3client.head_object(Bucket=self.bucket, Key=self.key)
            fileSize = response['ContentLength']

        print(f"=>{fileSize=}")
        return fileSize

    def xClose(self) -> None:
        """
        Close the database. Note that even if you return an error you should
        still close the file. It is safe to call this method multiple times.
        """
        print(f"S3: xClose({self.bucket=}, {self.key=})")
        if len(self.output_buffer.getbuffer()) > 0:
            print(f"S3: s3client.put({self.bucket=}, {self.key=})")
            self.output_buffer.seek(0)
            self.s3client.put_object(Bucket=self.bucket, Key=self.key,
                                     Body=self.output_buffer,
                                     Metadata=self.metadata,
                                     ContentType=self.content_type)
            self.output_buffer.close()

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def xFileControl(self, op: int, ptr: int) -> bool:
        """
        Receives file control request typically issued by Connection.filecontrol().
        :param op: A numeric code. Codes below 100 are reserved for SQLite internal use.
            https://sqlite.org/c3ref/c_fcntl_begin_atomic_write.html

            SQLITE_FCNTL_SIZE_HINT        5  # Give the VFS layer a hint of how large the database file will grow to
                                               be during the current transaction. This hint is not guaranteed to be
                                               accurate but it is often close.
                                               The underlying VFS might choose to preallocate database file space
                                               based on this hint in order to help writes to the database file run
                                               faster.
            SQLITE_FCNTL_HAS_MOVED       20  # File control interprets its argument as a pointer to an integer and it
                                               writes a boolean into that integer depending on whether or not the file
                                               has been renamed, moved, or deleted since it was first opened.
            SQLITE_FCNTL_SYNC            21  # Sent before the xSync method is invoked on a database file descriptor
            SQLITE_FCNTL_COMMIT_PHASETWO 22  # Sent to the VFS after a transaction has been committed immediately but
                                               before the database is unlocked.
        :param ptr: An integer corresponding to a pointer at the C level.
        :return: A boolean indicating if the op was understood
        """
        print(f"S3: xFileControl({self.key=}, {op=}, {ptr=})")
        return False

    def xLock(self, level: int) -> None:
        """
        Increase the lock to the level specified which is one of the SQLITE_LOCK family of constants.
        If you can’t increase the lock level because someone else has locked it, then raise BusyError.
        :param level:
        """
        print(f"S3: xLock({self.key=}, {level=})")

    def xUnlock(self, level: int) -> None:
        """
        Decrease the lock to the level specified which is one of the SQLITE_LOCK family of constants.
        """
        print(f"S3: xUnlock({self.key=}, {level=})")

    def xTruncate(self, newsize: int) -> None:
        """
        Set the file length to *newsize* (which may be more or less than the current length).
        :param newsize:
        """
        print(f"S3: xTruncate({self.key=}, {newsize=})")
        if self.flags & SQLITE_OPEN_CREATE:
            print(f"S3: s3client.put_object({self.bucket=}, {self.key=}, {newsize=})")
            created_object = self.s3client.put_object(Bucket=self.bucket, Key=self.key, Body=bytearray(newsize))
            print(f"created {created_object}")

    def xSync(self, flags: int) -> None:
        """
        Ensure data is on the disk platters (ie could survive a power failure
        immediately after the call returns)
        with the sync flags
        :param flags:
        """
        print(f"S3: xSync({self.key=}, {flags=})")
        pass

    def xSectorSize(self) -> int:
        """
        :return: the native underlying sector size. SQLite uses the value returned
            in determining the default database page size.
        """
        print(f"S3: xSectorSize({self.key=})")
        return 8192

    def xDeviceCharacteristics(self) -> int:
        """
        :return I/O capabilities (https://sqlite.org/c3ref/c_iocap_atomic.html)
                If you do not implement the function or have an error then 0 (the SQLite default) is returned.
        """
        print(f"S3: xDeviceCharacteristics({self.key=})")
        return 0

    def xCheckReservedLock(self) -> bool:
        """
        :return: True if any database connection (in this or another process) has a lock other
                 than `SQLITE_LOCK_NONE or SQLITE_LOCK_SHARED
        """
        print(f"S3: xCheckReservedLock({self.key=})")
        return False

    def excepthook(self, etype, evalue, etraceback) -> None:
        """
        Called when there has been an exception in a :class:`VFSFile` routine.
        The default implementation calls ``sys.excepthook`` and if that
        fails then ``PyErr_Display``.  The three arguments correspond to
        what ``sys.exc_info()`` would return.

        :param etype: The exception type
        :param evalue: The exception  value
        :param etraceback: The exception traceback. Note this includes all frames
        all the way up to the thread being started.
        """
        print(f"S3: excepthook({self.key=}, {etype=}, {evalue=}, {etraceback=})")
