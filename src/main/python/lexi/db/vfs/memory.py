from io import BytesIO
from apsw import SQLITE_IOCAP_ATOMIC, SQLITE_IOCAP_SEQUENTIAL


# noinspection PyPep8Naming
class MemoryVFSFile:
    def __init__(self, filename, flags):
        self.filename = filename
        self.flags = flags
        self.buffer: BytesIO = BytesIO()

    def excepthook(self, etype, evalue, etraceback):
        print(f"MEM: excepthook({self.filename=}, {etype=}, {evalue=}, {etraceback=})")

    def xRead(self, amount: int, offset: int) -> bytes:
        # print(f"MEM: xRead({self.filename=}, {amount=}, {offset=})")
        self.buffer.seek(offset)
        return self.buffer.read(amount)

    def xWrite(self, data, offset) -> None:
        # print(f"MEM: xWrite({self.filename=}, {len(data)=}, {offset=})")
        self.buffer.seek(offset)
        self.buffer.write(data)

    def xClose(self) -> None:
        print(f"MEM: xClose({self.filename=})")
        self.buffer.close()

    def xFileSize(self) -> int:
        print(f"MEM: xFileSize({self.filename=})")
        fileSize = len(self.buffer.getbuffer())
        print(f"=> {fileSize}")
        return fileSize

    def xSectorSize(self) -> int:
        print(f"MEM: xSectorSize({self.filename=})")
        return 4096

    def xFileControl(self, op: int, ptr: int) -> bool:
        print(f"MEM: xFileControl({self.filename=}, {op=}, {ptr=})")
        return False

    def xLock(self, level) -> None:
        print(f"MEM: xLock({self.filename=}, {level=})")

    def xUnlock(self, level) -> None:
        print(f"MEM: xUnlock({self.filename=}, {level=})")

    def xTruncate(self, newsize) -> None:
        print(f"MEM: xTruncate({self.filename=}, {newsize=})")
        self.buffer.truncate(newsize)

    def xSync(self) -> None:
        print(f"MEM: xSync({self.filename=})")

    def xCheckReservedLock(self) -> bool:
        print(f"MEM: xCheckReservedLock({self.filename=})")
        return False

    def xDeviceCharacteristics(self) -> int:
        print(f"MEM: xDeviceCharacteristics({self.filename=})")
        return SQLITE_IOCAP_ATOMIC | SQLITE_IOCAP_SEQUENTIAL
