# noinspection PyPep8Naming
class NoOpVFSFile:
    def __init__(self, filename, flags):
        self.filename = filename
        self.flags = flags

    def excepthook(self, etype, evalue, etraceback):
        print(f"NOP: excepthook({self.filename=}, {etype=}, {evalue=}, {etraceback=})")

    def xRead(self, amount: int, offset: int) -> bytes:
        print(f"NOP: xRead({self.filename=}, {amount=}, {offset=})")
        return b''

    def xWrite(self, data, offset) -> None:
        print(f"NOP: xWrite({self.filename=}, {len(data)=}, {offset=})")

    def xClose(self) -> None:
        print(f"NOP: xClose({self.filename=})")

    def xFileSize(self) -> int:
        print(f"NOP: xFileSize({self.filename=})")
        return 0

    def xSectorSize(self) -> int:
        print(f"NOP: xSectorSize({self.filename=})")
        return 42

    def xFileControl(self, op: int, ptr: int) -> bool:
        print(f"NOP: xFileControl({self.filename=}, {op=}, {ptr=})")
        return False

    def xLock(self, level) -> None:
        print(f"NOP: xLock({self.filename=}, {level=})")

    def xUnlock(self, level) -> None:
        print(f"NOP: xUnlock({self.filename=}, {level=})")

    def xTruncate(self, newsize) -> None:
        print(f"NOP: xTruncate({self.filename=}, {newsize=})")

    def xSync(self) -> None:
        print(f"NOP: xSync({self.filename=})")

    def xCheckReservedLock(self) -> bool:
        print(f"NOP: xCheckReservedLock({self.filename=})")
        return False

    def xDeviceCharacteristics(self) -> int:
        print(f"NOP: xDeviceCharacteristics({self.filename=})")
        return 0
