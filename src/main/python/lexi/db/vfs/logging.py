from apsw import VFS, VFSFile, URIFilename


class LoggingVFS(VFS):
    def __init__(self, vfsname='logging', basevfs=''):
        self.vfsname = vfsname
        self.basevfs = basevfs
        super().__init__(self.vfsname, self.basevfs)

    def xOpen(self, name, flags):
        (input_flags, output_flags) = flags
        if isinstance(name, URIFilename):
            print(f"Logging: xOpen({name.filename()}, {input_flags=:>08b}, {output_flags=:>08b})")
        else:
            print(f"Logging: xOpen({name}, {input_flags=:>08b}, {output_flags=:>08b})")
        return LoggingVFSFile(self.basevfs, name, flags)

    def xDelete(self, filename, syncdir):
        print(f"Logging: xDelete({filename=}, {syncdir=})")
        return super().xDelete(filename, syncdir)

    def xAccess(self, name, flag):
        print(f"Logging: xAccess({name=}, {flag=:>08b})")
        return super().xAccess(name, flag)

    def xGetLastError(self):
        print("Logging: xGetLastError")
        return super().xGetLastError()

    def xFullPathname(self, name):
        print(f"Logging: xFullPathname({name=})")
        return super().xFullPathname(name)

    def xSetSystemCall(self, name, pointer):
        print(f"Logging: xSetSystemCall({name=}, {pointer=})")
        return super().xSetSystemCall(name, pointer)

    def xGetSystemCall(self, name):
        print(f"Logging: xSetSystemCall({name=}")
        return super().xGetSystemCall(name)

    def xNextSystemCall(self, name):
        print(f"Logging: xNextSystemCall({name=}")
        return super().xNextSystemCall(name)

    def excepthook(self, etype, evalue, etraceback):
        print(f"Logging: excepthook({etype=}, {evalue=}, {etraceback=})")
        return super().excepthook(etype, evalue, etraceback)


class LoggingVFSFile(VFSFile):
    def __init__(self, inheritfromvfsname, filename, flags):
        if isinstance(filename, URIFilename):
            self.used_filename = filename.filename()
        else:
            self.used_filename = filename
        super().__init__(inheritfromvfsname, filename, flags)

    def excepthook(self, etype, evalue, etraceback) -> None:
        print(f"Logging: excepthook({self.used_filename=}, {etype=}, {evalue=}, {etraceback=})")
        return super().excepthook(etype, evalue, etraceback)

    def xRead(self, amount, offset) -> bytes:
        print(f"Logging: xRead({self.used_filename=}, {amount=}, {offset=})")
        return super().xRead(amount, offset)

    def xWrite(self, data, offset) -> None:
        print(f"Logging: xWrite({self.used_filename=}, {len(data)=}, {offset=})")
        return super().xWrite(data, offset)

    def xClose(self) -> None:
        print(f"Logging: xClose({self.used_filename=})")
        return super().xClose()

    def xFileSize(self) -> int:
        print(f"Logging: xFileSize({self.used_filename=})")
        return super().xFileSize()

    def xSectorSize(self) -> int:
        print(f"Logging: xSectorSize({self.used_filename=})")
        return super().xSectorSize()

    def xFileControl(self, op: int, ptr: int) -> bool:
        print(f"Logging: xFileControl({self.used_filename=}, {op=}, {ptr=})")
        return super().xFileControl(op, ptr)

    def xLock(self, level) -> None:
        print(f"Logging: xLock({self.used_filename=}, {level=})")
        return super().xLock(level)

    def xUnlock(self, level) -> None:
        print(f"Logging: xUnlock({self.used_filename=}, {level=})")
        return super().xUnlock(level)

    def xTruncate(self, newsize) -> None:
        print(f"Logging: xTruncate({self.used_filename=}, {newsize=})")
        return super().xTruncate(newsize)

    def xSync(self, flags: int) -> None:
        print(f"Logging: xSync({self.used_filename=} {flags=})")
        return super().xSync(flags)

    def xCheckReservedLock(self) -> bool:
        print(f"Logging: xCheckReservedLock({self.used_filename=})")
        return super().xCheckReservedLock()

    def xDeviceCharacteristics(self) -> int:
        print(f"Logging: xDeviceCharacteristics({self.used_filename=})")
        return super().xDeviceCharacteristics()
