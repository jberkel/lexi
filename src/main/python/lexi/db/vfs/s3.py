import json
import types
from typing import Tuple, Any, Type

from apsw import VFS, URIFilename, SQLITE_OPEN_MAIN_JOURNAL, \
    SQLITE_OPEN_TEMP_JOURNAL, \
    SQLITE_OPEN_SUBJOURNAL, SQLITE_OPEN_SUPER_JOURNAL

from .memory import MemoryVFSFile
from .noop import NoOpVFSFile
from .s3_vfs_file import S3VFSFile

JOURNAL_FLAGS = SQLITE_OPEN_MAIN_JOURNAL | SQLITE_OPEN_TEMP_JOURNAL | SQLITE_OPEN_SUBJOURNAL | SQLITE_OPEN_SUPER_JOURNAL
NOOP_FLAGS = JOURNAL_FLAGS ^ SQLITE_OPEN_TEMP_JOURNAL


class S3VFS(VFS):
    """
    A VFS implementation which reads and writes to S3 buckets. The S3 objects
    need to be specified in the form of ``file://bucket/key``, otherwise an
    error is raised.

    Based on
    https://github.com/dacort/athena-sqlite
    https://rogerbinns.github.io/apsw/vfs.html
    """
    def __init__(self, s3client, vfsname='s3', basevfs=''):
        self.vfsname = vfsname
        self.basevfs = basevfs
        self.s3client = s3client
        self.openFiles = {}
        super().__init__(self.vfsname, self.basevfs)

    def xOpen(self, name: str | URIFilename | None, flags: list[int]) -> Any:
        """
        :param name: File to open. Note that name may be None in which case you should open a temporary file with a
                     name of your choosing. May be an instance of URIFilename.

        :param flags: A list of two integers [inputflags, outputflags]. Each integer is one or more of the open flags
                      binary orred together. The inputflags tells you what SQLite wants.
                      SQLITE_OPEN_READONLY      0x00000001
                      SQLITE_OPEN_READWRITE     0x00000002
                      SQLITE_OPEN_CREATE        0x00000004
                      SQLITE_OPEN_DELETEONCLOSE 0x00000008  # will be set for TEMP databases and their journals,
                                                              transient databases, and subjournals.
                      SQLITE_OPEN_EXCLUSIVE     0x00000010  # always used in conjunction with the SQLITE_OPEN_CREATE
                      SQLITE_OPEN_AUTOPROXY     0x00000020
                      SQLITE_OPEN_URI           0x00000040
                      SQLITE_OPEN_MEMORY        0x00000080

                      SQLITE_OPEN_MAIN_DB       0x00000100
                      SQLITE_OPEN_TEMP_DB       0x00000200 # - used for explicitly declared and named TEMP tables
                                                               (using the CREATE TEMP TABLE syntax) or for named tables
                                                               in a temporary database that is created by opening a
                                                               database with a filename that is an empty string
                                                             - last for the duration of the database connection
                      SQLITE_OPEN_TRANSIENT_DB  0x00000400 # - database table that SQLite creates automatically in order
                                                               to evaluate a subquery or ORDER BY or GROUP BY clause
                                                             - last only for the duration of a single SQL statement
                      SQLITE_OPEN_MAIN_JOURNAL  0x00000800

                      SQLITE_OPEN_TEMP_JOURNAL  0x00001000   - used by vdbeSorterOpenTempFile
                      SQLITE_OPEN_SUBJOURNAL    0x00002000
                      SQLITE_OPEN_SUPER_JOURNAL 0x00004000

                      The outputflags describes how you actually did open the file.
                      For example if you opened it read only then SQLITE_OPEN_READONLY should be set.
        :return: a new file object based on name. You can return a VFSFile from a completely different VFS.
        """
        (input_flags, output_flags) = flags
        vfs_file = self._make_vfs_file(input_flags, name)
        self.openFiles[name] = vfs_file
        return vfs_file

    def _make_vfs_file(self, flags: int, name: str | URIFilename | None) -> MemoryVFSFile | S3VFSFile | NoOpVFSFile:
        if flags & NOOP_FLAGS:
            print(f"S3: xOpen({name=}, {flags=:>08b}), skipping journal")
            return NoOpVFSFile(name, flags)
        else:
            match name:
                case None:
                    print(f"S3: xOpen({name=}, {flags=:>08b}), temp file")
                    return MemoryVFSFile(name, flags)
                case URIFilename() as uri_filename if uri_filename.filename().startswith('//'):
                    print(f"S3: xOpen({uri_filename.filename()=}, {flags=:>08b})")
                    filename: str = uri_filename.filename()

                    bucket, key = filename[2:].split('/', 1)
                    if metadata_parameter := uri_filename.uri_parameter('Metadata'):
                        metadata = json.loads(metadata_parameter)
                    else:
                        metadata = None

                    return S3VFSFile(bucket, key,
                                     s3client=self.s3client,
                                     flags=flags,
                                     content_type=uri_filename.uri_parameter('ContentType'),
                                     metadata=metadata)
                case _:
                    raise Exception(f'Invalid file: {name} ({flags=:>08b})'
                                    f', S3 VFS filenames have the format file://bucket/key')

    def xAccess(self, name: str, flags: int) -> bool:
        """
        :param name:
        :param flags: SQLITE_ACCESS_EXISTS (0) to test for the existence of a file,
                      or SQLITE_ACCESS_READWRITE (1) to test whether a file is readable and writable,
                      or SQLITE_ACCESS_READ (2) to test whether a file is at least readable
        :return: True if file is accessible
        """
        print(f"S3: xAccess({name=}, {flags=:>08b})")
        return False

    def xDelete(self, filename: str, syncdir: bool):
        """
        :param filename: file to delete
        :param syncdir: if True then the directory should be synced ensuring that the file deletion has been recorded
                        on the disk platters. ie if there was an immediate power failure after this call returns,
                        on a reboot the file would still be deleted.
        """
        print(f"S3: xDelete({filename=}, {syncdir=})")
        if filename not in self.openFiles:
            raise Exception(f'File {filename} not open')

    def xFullPathname(self, name: str) -> str:
        """
        :param name: the filename
        :return: the absolute pathname for name.  You can use ``os.path.abspath`` to do this.
        """
        print(f"S3: xFullPathname({name=})")
        return name

    def xRandomness(self, numbytes: int) -> bytes:
        """
        This method is called once when SQLite needs to seed the random number generator.
        It is called on the default VFS only.
        You can return less than the number of bytes requested including None. If
        you return more then the surplus is ignored.

        :param numbytes: number of requested bytes
        :return: bytes, buffer
        """
        print(f"S3: xRandomness({numbytes=})")
        return super().xRandomness(numbytes)

    def xSleep(self, microseconds: int) -> int:
        """
        Pause execution of the thread for at least the specified number of microseconds (millionths of a second).
        This routine is typically called from the busy handler.

        :param microseconds:
        :return: How many microseconds you actually requested the operating system to sleep for.
        """
        print(f"S3: xSleep({microseconds=})")
        return super().xSleep(microseconds)

    def xCurrentTime(self) -> float:
        """
        :return: the `Julian Day Number` as floating point number where the integer portion is the day and
                 the fractional part is the time. Do not adjust for timezone (ie use `UTC`)
        """
        print("S3: xCurrentTime()")
        return super().xCurrentTime()

    def xGetLastError(self) -> Tuple[int, str]:
        """
        This method is to return an integer error code and (optional) text describing
        the last error that happened in this thread.

        :return: (int, string or None)
        """
        (code, msg) = super().xGetLastError()
        print(f"S3: xGetLastError({code=}, {msg=})")
        return code, msg

    def xDlOpen(self, filename) -> int:
        """
        Load the shared library.
        :param filename:
        :return: a number which will be treated as a void pointer at the C level.
        """
        print(f"S3: xDlOpen({filename=})")
        raise NotImplementedError

    def xDlSym(self, handle, symbol) -> int:
        """
        Returns the address of the named symbol which will be called by SQLite. On error you should return 0 (NULL).
        :param handle:
        :param symbol:
        :return: address
        """
        print(f"S3: xDlSym({handle=}, {symbol=})")
        raise NotImplementedError

    def xDlError(self) -> str:
        """
        :return: error string describing the last error of VFS.xDlOpen or VFS.xDlSym (ie they returned
                 zero/NULL)
        """
        raise NotImplementedError

    def xDlClose(self, handle):
        """
        Close and unload the library corresponding to the handle you returned from `xDlOpen`.
        :param handle:
        """
        print(f"S3: xDlClose({handle=})")
        pass

    def excepthook(self, etype: Type, evalue: BaseException, etraceback: types.TracebackType | None):
        """
        Called when there has been an exception in a :class:`VFS` routine.
        The default implementation calls ``sys.excepthook`` and if that
        fails then ``PyErr_Display``.  The three arguments correspond to
        what ``sys.exc_info()`` would return.

        :param etype: The exception type
        :param evalue: The exception  value
        :param etraceback: The exception traceback.  Note this includes all frames all the way up to the thread
        being started.
        """
        print(f"S3: excepthook({etype=}, {evalue=}, {etraceback=})")
        from traceback import StackSummary, walk_tb
        for line in StackSummary.extract(walk_tb(etraceback), capture_locals=True).format():
            print(line)
        super().excepthook(etype, evalue, etraceback)
