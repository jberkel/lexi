from .vfs import S3VFS
from playhouse.apsw_ext import APSWDatabase
from apsw import Connection, SQLITE_OPEN_READONLY, SQLITE_OPEN_URI


class Database(APSWDatabase):
    """The S3 VFS (virtual file system) of this database connection."""
    s3_vfs: S3VFS | None = None
    open_as_memory_db: bool = False

    def init(self, database: str, open_as_memory_db=False, s3client=None, **kwargs):
        """
        A specialized APSWDatabase database with support for S3VFS and memory dbs.

        :param open_as_memory_db:
            Opens the specified database as an in-memory (:memory:) database,
            by performing a backup to memory.
        :param s3client:
            A boto S3 client to use for

        :param database: path to the database
        """
        self.open_as_memory_db = open_as_memory_db

        if s3client:
            # the parent initializer (VFS.init) registers or replaces the VFS
            # with the name "s3".
            self.s3_vfs = S3VFS(s3client=s3client)
            self.open_as_memory_db = database.startswith("file://")
            self.connect_params['vfs'] = self.s3_vfs.vfsname
        super().init(database, kwargs)

    def close(self):
        if self.s3_vfs:
            self.s3_vfs.unregister()
        super().close()

    def vacuum_into(self, output):
        cursor = self.cursor()
        print(f"VACUUM INTO {output}")
        cursor.execute(f"VACUUM INTO '{output}'")
        cursor.close()

    def _connect(self):
        if self.database != ':memory:' and self.open_as_memory_db:
            connection = Connection(self.database,
                                    flags=SQLITE_OPEN_READONLY | SQLITE_OPEN_URI,
                                    **self.connect_params)

            memory_connection = Connection(':memory:', **self.connect_params)
            with memory_connection.backup('main', connection, 'main') as backup:
                backup.step()
            connection.close()
            return memory_connection
        else:

            return super()._connect()
