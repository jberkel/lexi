#!/usr/bin/env python
from lexi.models.fts import FTSHeadword


def single_search(query: str):
    for (headword, _, score, rank, highlight, highlight_translation)\
            in FTSHeadword.headword_olha_query(query, limit=10).tuples():

        print(headword,
              "\t\t", "%.2f (%.2f)" % (rank, score),
              "\t", highlight,
              "\t", highlight_translation)


if __name__ == '__main__':
    import os
    import sys
    from lexi.models import database_connection

    if len(sys.argv) > 1:
        with database_connection(sys.argv[1]):
            if len(sys.argv) > 2:
                single_search(sys.argv[2])
    else:
        raise Exception("%s [db-path] [query]" % os.path.basename(sys.argv[0]))
