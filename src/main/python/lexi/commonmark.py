#!/usr/bin/env python
import typing

from mistletoe import Document, HTMLRenderer
from mistletoe.block_token import List, Paragraph
from mistletoe.span_token import (EscapeSequence, Link, RawText, Emphasis, SpanToken, Strong, InlineCode,
                                  HtmlSpan)
from urllib.parse import urlparse, unquote
from typing import Optional


class FormatException(Exception):
    def __init__(self, message):
        super().__init__(message)


def link_target(text: str) -> Optional[tuple[str, str]]:
    match _parse(text).children:
        case [Paragraph() as paragraph]:
            match paragraph.children:
                case [RawText(content=content)]:
                    return content, content
                case [Link() as link]:
                    components = urlparse(link.target)
                    target = unquote(components.path.replace('./', ''))
                    link_text = _to_string(link.children)

                    if link_text.strip():
                        return link_text, target
                    else:
                        return None
                case other:
                    return None
        case [List() as _list] if len(_list.children) == 1:  # type: ignore
            return text, text
        case other:
            raise FormatException(f"Unexpected token: {other} ({text})")


def _parse(commonmark) -> Document:
    # need to wrap parsing in HTMLRenderer context manager to activate `HtmlSpan`
    with HTMLRenderer():
        return Document(commonmark)


@typing.no_type_check
def _to_string(nodes: list[SpanToken]) -> str:
    match nodes:
        case [RawText(content=content)]:
            return str(content)
        case [EscapeSequence(children=children) |
              Emphasis(children=children) |
              Strong(children=children) |
              InlineCode(children=children)]:
            return _to_string(children)
        case [HtmlSpan()]:
            return ""
        case [other]:
            raise FormatException(f"Unexpected token: {other}")
        case [a, *rest]:
            return _to_string([a]) + _to_string(rest)
        case []:
            # should return empty string here, but empty nodes perhaps indicate
            # some other kind of bug earlier in the processing
            raise FormatException("Empty nodes")
        case other:
            raise FormatException(f"Unexpected token: {other}")
