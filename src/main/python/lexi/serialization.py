import copy
from typing import Annotated
from typing import Any

import msgspec

from .models.jwktl import PartOfSpeech


class Translation(msgspec.Struct):
    headword: str
    partOfSpeech: str
    gloss: str
    definition: str | None = None


class Pronunciation(msgspec.Struct):
    text: str
    qualifier: list[str] = list()


class Relation(msgspec.Struct):
    target: str
    type: Annotated[int, msgspec.Meta(ge=0, le=17)]
    transliteration: str | None = None


class Usex(msgspec.Struct):
    text: str
    translation: str | None = None
    transliteration: str | None = None
    literally: str | None = None


class Sense(msgspec.Struct):
    definition: str
    number: dict[str, int]
    relations: list[Relation] = list()
    usexes: list[Usex] = list()
    categories: list[str] = list()
    formOf: str | None = None

    def number_tuple(self) -> tuple[int, int, int, int, int]:
        return \
            (self.number['_1'],
             self.number['_2'],
             self.number['_3'],
             self.number['_4'],
             self.number['_5'])


class HeadwordLine(msgspec.Struct):
    headword: str
    gender: list[Annotated[int, msgspec.Meta(ge=0, le=5)]] = list()
    inflections: list[Any] = list()
    alternativeSpellings: list[str] = list()
    transliteration: str | None = None

    def to_data(self) -> dict[str, Any]:
        data: dict[str, Any] = {
            'headword': self.headword
        }
        if transliteration := self.transliteration:
            data['transliterations'] = [transliteration]

        if self.alternativeSpellings:
            data['spellings'] = self.alternativeSpellings

        if self.gender:
            data['genders'] = self.gender
        return data


class Conjugation(msgspec.Struct):
    form: str
    features: list[str]

    def to_data(self) -> dict[str, Any]:
        return {
            "form": self.form,
            "features": self.features
        }


class Inflections(msgspec.Struct):
    conjugations: list[Conjugation]


class Entry(msgspec.Struct):
    pageTitle: str
    headwordLines: list[HeadwordLine]
    partOfSpeech: str | int
    senses: list[Sense]
    # optional fields
    etymology: str | None = None
    usageNotes: str | None = None
    pronunciations: list[Pronunciation] = list()
    relations: list[Relation] = list()
    categories: list[str] = list()
    inflections: Inflections = msgspec.field(default_factory=lambda: Inflections(conjugations=[]))

    def pos(self) -> PartOfSpeech | int | None:
        match self.partOfSpeech:
            case int(value):
                return value
            case str(value):
                return PartOfSpeech.from_string(value)
            case _:
                return None

    def is_translation_page(self) -> bool:
        return self.pageTitle.endswith('/translations')

    def is_translation_hub(self) -> bool:
        return not self.all_categories().isdisjoint({"Translation hubs",
                                                     "English translation hubs"})

    def sense_relations(self) -> list[Relation]:
        return [relation for sense in self.senses for relation in
                sense.relations]

    def senses_with_entry_relations(self) -> list[Sense]:
        """
        If the entry contains a single sense, augments it with the entry's
        relations, otherwise the senses are returned unmodified.
        """
        match (self.senses, self.relations):
            case ([single], [a, *rest]):
                copied = copy.deepcopy(single)
                copied.relations += [a, *rest]
                return [copied]
            case (senses, _):
                return senses
            case _:
                return []

    def all_categories(self) -> set[str]:
        return {
            category
            for sense in self.senses
            for category in sense.categories
        }.union(set(self.categories))

    def is_lemma(self):
        return any(x.endswith(' lemmas') for x in self.categories)

    def is_non_lemma(self):
        return any(x.endswith(' non-lemma forms') for x in self.categories)

    def is_just_forms(self):
        return all(x.endswith('forms') for x in self.categories)

    def forms_sort_key(self):
        return int(self.is_just_forms())

    def data(self) -> dict[str, Any]:
        return self.headword_data() | self.sense_data()

    def headword_data(self) -> dict[str, Any]:
        return self.headwordLines[0].to_data()

    def sense_data(self) -> dict[str, Any]:
        if sense_forms := [sense.formOf for sense in self.senses if sense.formOf]:
            return {
                'formOf': sense_forms
            }
        else:
            return {}


json_decoder = msgspec.json.Decoder()


class Page(msgspec.Struct):
    pageTitle: str
    entries: list[Entry]
    redirects: list[str]
    translations: list[Translation] | None = None

    @staticmethod
    def from_json(line):
        decoded = json_decoder.decode(line)
        return msgspec.convert(decoded, Page)

    def is_translation(self):
        return self.pageTitle.endswith("/translations")
