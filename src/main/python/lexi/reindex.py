#!/usr/bin/env python


if __name__ == '__main__':
    import os
    import sys
    from lexi.models import database_connection
    from lexi.models.fts import FTSHeadword

    if len(sys.argv) > 1:
        with database_connection(sys.argv[1]):
            FTSHeadword.reindex()
    else:
        raise Exception("%s [db-path]" % os.path.basename(sys.argv[0]))
