#!/usr/bin/env python


def print_entries(query):
    from .models.queries import prefetch_headwords
    from .models import Headword

    for headword in prefetch_headwords(Headword.query(query)):
        for pronunciation in headword.pronunciations:
            print("\t\t%s" % pronunciation)
            print()

        for entry in headword.entries:
            print("\n", entry.id, "\t", entry.headword_text, "pos=",
                  str(entry.pos), "data = ",
                  str(entry.data))
            if entry.etymology:
                print("\t\t", entry.etymology.etymology_text, "\n")

            relations = [" %s => %s" % (str(relation.type),
                                        relation.target_headword.headword_text
                                        if relation.target_headword else None)
                         for relation in entry.relations]
            if len(relations) > 0:
                print("\t\trelations: %s\n" % ', '.join(relations))

            for sense in entry.senses:
                print("\t\t", sense.id, "\t", sense.sense_text)
                for example in sense.examples:
                    print("\t\t\t\t", example.example_text)
                if len(sense.categories) > 0:
                    print("\t\t Sense categories:",
                          ', '.join([c.category.category_text for c in sense.categories]))

            if len(entry.categories) > 0:
                print("\t\t Entry categories:",
                      ', '.join([c.category.category_text for c in entry.categories]))

        if len(headword.translations) > 0:
            print("\n\t\t Translations: ",
                  ', '.join(sorted(set([translation.translation_text
                                        for translation
                                        in headword.translations]))))

        if len(headword.categories) > 0:
            print("\t\t Headword categories:",
                  ', '.join([c.category.category_text for c in headword.categories]))

        print("\t\t", "-" * 80)


if __name__ == '__main__':
    import sys
    import os
    from .models import database_connection

    if len(sys.argv) > 2:
        with database_connection(sys.argv[1]):
            print_entries(sys.argv[2])
    else:
        raise Exception("%s [db-path] [headword]" % os.path.basename(sys.argv[0]))
