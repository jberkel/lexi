import dataclasses
import apsw


@dataclasses.dataclass
class PhraseMatch:
    number: int  # the number of the matching phrase, (0-based)
    column: int  # the column which matched, (0-based)
    token_position: int  # relative token position, (0-based)
    num_tokens: int      # the total number of tokens in the column


@dataclasses.dataclass
class RowMatch:
    fixed_score: float  # the static score assigned to this row
    phrase_matches: list[PhraseMatch]  # the matches for this row

    def phrases(self) -> set[int]:
        return {match.number for match in self.phrase_matches}


@dataclasses.dataclass
class RankParameters:
    phrase_weightings: list[float]


def score(row_match: RowMatch, parameters: RankParameters) -> float:
    def phrase_match_weighting(phrase_match: PhraseMatch) -> float:
        if phrase_match.number < len(parameters.phrase_weightings):
            phrase_weighting = parameters.phrase_weightings[phrase_match.number]
        else:
            phrase_weighting = 1.0

        if phrase_match.column == 0:  # headword column
            if phrase_match.number == 0 and phrase_match.num_tokens == 1:
                other_weighting = 3.0  # direct match!
            else:
                other_weighting = 1.0 / (1 + phrase_match.token_position)
        else:
            other_weighting = 1.0

        return phrase_weighting * other_weighting

    return row_match.fixed_score * sum(
        (phrase_match_weighting(phrase_match) for phrase_match in row_match.phrase_matches)
    )


def gather_match_information(fts: apsw.FTS5ExtensionApi, base_score: float) -> RowMatch:
    matches = [
        PhraseMatch(number=phrase_i,
                    column=column_i,
                    token_position=fts.phrase_column_offsets(phrase_i, column_i)[0],
                    num_tokens=fts.column_size(column_i))

        for phrase_i in range(0, fts.phrase_count)
        for column_i in fts.phrase_columns(phrase_i)
    ]
    return RowMatch(fixed_score=base_score, phrase_matches=matches)
