#!/usr/bin/env python
from lexi.serialization import Page, Entry, Relation, Usex, Sense, Pronunciation, \
    Translation
from .models.jwktl import PartOfSpeech, RelationType


class Ranker:
    DEFAULT_POS_SCORE = 0.0
    DEFAULT_RELATION_SCORE = 0.0

    POS_MAPPING: dict[PartOfSpeech, float] = {
        PartOfSpeech.NOUN: 1.0,
        PartOfSpeech.VERB: 0.8,
        PartOfSpeech.ADVERB: 0.7,
        PartOfSpeech.ADJECTIVE: 0.7,
        PartOfSpeech.PHRASE: 0.7,
        PartOfSpeech.PREPOSITION: 0.7,
        PartOfSpeech.PRONOUN: 0.6,
        PartOfSpeech.PROPER_NOUN: 0.0
    }

    RELATION_TYPE_SCORES: dict[int, float] = {
        RelationType.SYNONYM: 0.2
    }

    def score_page(self, page: Page):
        """ Calculates score for a page

            Sum of entry score + number of translations
        """
        return sum([self.score_entry(entry) for entry in page.entries]) + \
            self._score_translations(page.translations)

    def score_entry(self, entry: Entry):
        """Calculates a score for an entry.

         What contributes to the score? (in order of importance)
          - POS (noun, verb, adj, adv, *rest)
          - number of senses (take LemmaType into account)
            - bonus for gloss
            - number of examples
          - number defined synonyms
          - number of pronunciations
          - presence of etymology
        """
        lemma_entry = entry.is_lemma()

        score = 0.0
        score += self._score_pos(entry.pos()) * (1.0 if lemma_entry else 0.5)
        score += self._score_senses(base_score=1.0 if lemma_entry else 0.1,
                                    senses=entry.senses)
        score += self._score_relations(entry.sense_relations())
        score += self._score_etymology(entry.etymology)
        score += self._score_pronunciations(entry.pronunciations)
        return score

    def _score_etymology(self, etymology):
        return 1 if etymology else 0

    def _score_pronunciations(self, pronunciations: list[Pronunciation]):
        return sum([1 for _ in pronunciations])

    def _score_senses(self, base_score, senses: list[Sense]):
        return sum([base_score + self._score_usexes(sense.usexes) for sense in senses])

    def _score_translations(self, translations: list[Translation] | None):
        if translations:
            return sum([1 for _ in translations])
        else:
            return 0

    def _score_usexes(self, examples: list[Usex]):
        return sum([1 for _ in examples])

    def _score_relation(self, relation: Relation):
        score = self.RELATION_TYPE_SCORES.get(relation.type, Ranker.DEFAULT_RELATION_SCORE)
        if relation.target is None:
            score *= 0.1
        return score

    def _score_relations(self, relations: list[Relation]):
        return sum([self._score_relation(relation) for relation in relations])

    def _score_pos(self, pos: PartOfSpeech | int | None) -> float:
        match pos:
            case PartOfSpeech() as p: return self.POS_MAPPING.get(p, Ranker.DEFAULT_POS_SCORE)
            case int(i): return self._score_pos(PartOfSpeech(i))
            case None: return Ranker.DEFAULT_POS_SCORE
