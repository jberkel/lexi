#!/usr/bin/env python
from setuptools import setup, find_packages
import subprocess

src_dir = 'src/main/python'
git_sha = subprocess.check_output(
    ['git', 'rev-parse', '--short',  'HEAD']
).decode('utf-8').strip()

setup(name='lexi',
      version='0.2.dev0+git.%s' % git_sha,
      description='Tools for building dictionaries',
      author='Jan Berkel',
      author_email='jan@berkel.fr',
      url='https://gitlab.com/jberkel/lexi',
      package_dir={'': src_dir},
      packages=find_packages(src_dir),
      install_requires=['apsw==3480000.dev0+olha.06385db5', 'peewee>=3.17.0',
                        'msgspec>=0.19.0', 'mistletoe==1.3.0']
 )
