#!/usr/bin/env python
from urllib.parse import unquote_plus, urlencode, quote
import boto3
import json
from lexi.dictionary import DictionaryDatabase

s3 = boto3.client('s3')


def link(file_name: str, output: str):
    print(f"link {file_name} => {output}")

    with DictionaryDatabase(file_name, s3client=s3) as db:
        db.link_relations()
        db.vacuum_into(output)


def process(bucket: str, key: str):
    print(f"process: {bucket=} {key=}")
    response = s3.head_object(Bucket=bucket, Key=key)
    content_type = response['ContentType']
    metadata = response.get('Metadata', {})

    if content_type == 'application/x-sqlite3':
        params = {
            'Metadata': json.dumps(metadata),
            'ContentType': content_type
        }
        encoded_params = urlencode(params, quote_via=quote)
        link(f"file://{bucket}/{key}", output=f"file://{bucket}-processed/{key}?{encoded_params}")
        return 'OK'
    else:
        print(f"Ignoring content type '{content_type}'")
        return 'IGNORED'


# https://docs.aws.amazon.com/lambda/latest/dg/python-context.html
def lambda_handler(event, context):
    from os import getenv, system
    if getenv('LAMBDA_TASK_ROOT'):
        system('rm -rf /tmp/*')

    print(f"{context.memory_limit_in_mb=}")
    print(f"{context.get_remaining_time_in_millis()=}")

    bucket = event['Records'][0]['s3']['bucket']['name']
    key = unquote_plus(event['Records'][0]['s3']['object']['key'])
    process(bucket, key)
