#!/usr/bin/env python

from urllib.parse import unquote_plus, urlencode, quote
import boto3
import json
from lexi.models import database_connection, Metadata

s3 = boto3.client('s3')


HEADER_PROCESSED = 'processed'


def process(bucket, key):
    print(f"process: {bucket}/{key}")
    response = s3.head_object(Bucket=bucket, Key=key)
    content_type = response['ContentType']
    metadata = response.get('Metadata', {})
    # idempotent processing
    if content_type == 'application/x-sqlite3' and HEADER_PROCESSED not in metadata:
        with database_connection(f"file://{bucket}/{key}", s3client=s3) as db:
            data = Metadata.as_dictionary()
            data[HEADER_PROCESSED] = '1'
            metadata.update(data)
            print(f"metadata: {metadata=}")
            print("execute_sql ANALYZE")
            db.execute_sql('ANALYZE')
            params = {
                'Metadata': json.dumps(metadata),
                'ContentType': content_type
            }
            encoded_params = urlencode(params, quote_via=quote)
            db.vacuum_into(f"file://{bucket}-processed/{key}?{encoded_params}")

        return 'OK'
    else:
        print('Already processed file, ignoring')
        return 'IGNORED'


def lambda_handler(event, context):
    from os import getenv, system
    if getenv('LAMBDA_TASK_ROOT'):
        system('rm -rf /tmp/*')

    print(f"{context.memory_limit_in_mb=}")
    print(f"{context.get_remaining_time_in_millis()=}")

    bucket = event['Records'][0]['s3']['bucket']['name']
    key = unquote_plus(event['Records'][0]['s3']['object']['key'])
    process(bucket, key)


if __name__ == '__main__':
    import sys
    from urllib.parse import urlparse

    if len(sys.argv) > 1:
        file_or_s3_resource = sys.argv[1]
        if file_or_s3_resource.startswith('s3://'):
            s3_url = urlparse(file_or_s3_resource)
            bucket = s3_url.netloc
            key = s3_url.path[1:]
            process(bucket, key)
