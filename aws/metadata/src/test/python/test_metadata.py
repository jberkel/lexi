from unittest import TestCase
from unittest.mock import Mock

from botocore.stub import Stubber

import metadata
from metadata import HEADER_PROCESSED


class MetadataTest(TestCase):
    def setUp(self):
        super().setUp()
        self.stubber = Stubber(metadata.s3)
        self.stubber.activate()
        metadata.Metadata = Mock()  # type: ignore[misc] # noqa: F821
        metadata.Metadata.configure_mock(**{"as_dictionary.return_value": {'db-key': 'db-value'}})
        metadata.database_connection = Mock()
        self.mocked_db = Mock()

        metadata.database_connection().__enter__ = Mock(return_value=self.mocked_db)
        metadata.database_connection().__exit__ = Mock(return_value=False)

    def tearDown(self):
        super().tearDown()
        self.stubber.assert_no_pending_responses()
        self.stubber.deactivate()

    def test_process_previously_unprocessed_file_uploads_file_with_extracted_metadata(self):
        self.stub_head()
        self.assertEqual('OK', metadata.process('bucket', 'key'))
        self.mocked_db.vacuum_into.assert_called_with(
            "file://bucket-processed/key?Metadata=%7B%22db-key%22%3A%20%22db-value"
            "%22%2C%20%22processed%22%3A%20%221%22%7D&ContentType=application%2Fx-sqlite3"
        )

    def test_process_previously_unprocessed_file_uploads_file_with_previously_set_metadata(self):
        self.stubber.add_response('head_object',
                                  service_response={
                                      'ContentType': 'application/x-sqlite3',
                                      'Metadata': {'existing-header': 'existing-value'},
                                      'StorageClass': 'STANDARD'
                                  },
                                  expected_params={'Bucket': 'bucket', 'Key': 'key'})
        self.assertEqual('OK', metadata.process('bucket', 'key'))
        self.mocked_db.vacuum_into.assert_called_with(
            "file://bucket-processed/key?Metadata=%7B%22existing-header%22%3A%20%22existing-value"
            "%22%2C%20%22db-key%22%3A%20%22db-value%22%2C%20%22processed%22%3A%20%221%22%7D"
            "&ContentType=application%2Fx-sqlite3"
        )

    def test_process_previously_processed_file_ignores_file(self):
        self.stubber.add_response('head_object',
                                  service_response={'ContentType': 'application/x-sqlite3',
                                                    'Metadata': {HEADER_PROCESSED: '1'}},
                                  expected_params={'Bucket': 'bucket', 'Key': 'key'})
        self.assertEqual('IGNORED', metadata.process('bucket', 'key'))
        self.mocked_db.assert_not_called()

    def test_process_wrong_content_type_is_ignored(self):
        self.stubber.add_response('head_object',
                                  service_response={'ContentType': 'binary/octet-stream'},
                                  expected_params={'Bucket': 'bucket', 'Key': 'key'})
        self.assertEqual('IGNORED', metadata.process('bucket', 'key'))
        self.mocked_db.assert_not_called()

    def stub_head(self):
        self.stubber.add_response('head_object',
                                  service_response={
                                      'Metadata': {},
                                      'ContentType': 'application/x-sqlite3',
                                      'ContentLength': 123
                                  },
                                  expected_params={'Bucket': 'bucket', 'Key': 'key'})
