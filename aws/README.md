# Amazon Lambda EC2 AMI IDs

  * AMI ID: ami-f0091d91 in the US West (Oregon) region.
  * AMI ID: ami-bc5b48d0 in the EU (Frankfurt) region. [eu-central-1]

[Lambda Execution Environment][]

# Importing processing pipeline

## S3 / AWS λ

```
-> S3::wiktionary-dump-parsed -> aws:dict-builder -> S3::moti-app-link
-> S3::moti-app-link          -> aws:linker       -> S3::moti-app-link-processed      -> aws:duplicator
-> S3::moti-app-index         -> aws:indexer      -> S3::moti-app-index-processed     -> aws:duplicator
-> S3::moti-app-metadata      -> aws:metadata     -> S3::moti-app-metadata-processed  -> aws:duplicator
-> S3::moti-app-compress      -> aws:compressor   -> S3::moti-app-compress-processed  -> aws:duplicator
-> S3::moti-app               -> aws:publisher
```

[Lambda Execution Environment]: http://docs.aws.amazon.com/lambda/latest/dg/current-supported-versions.html
