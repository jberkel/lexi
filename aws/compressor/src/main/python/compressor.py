#!/usr/bin/env python
from urllib.parse import unquote_plus
import boto3
from tempfile import NamedTemporaryFile
import os
import lzma

ACCEPTED_CONTENT_TYPES = ['binary/octet-stream', 'application/x-sqlite3']
HEADER_ORIGINAL_SIZE = 'original-size'
HEADER_COMPRESSION_SETTING = 'compression-setting'
LZMA_CONTENT_TYPE = 'application/x-lzma'
LZMA_EXTENSION = '.lzma'

s3 = boto3.client('s3')


def compress(streaming_body, preset=lzma.PRESET_DEFAULT):
    print(f"compressing with {preset=}")
    compressed_file_obj = NamedTemporaryFile(delete=False)
    with lzma.open(compressed_file_obj, 'w', format=lzma.FORMAT_ALONE, preset=preset) as lzma_out:
        for chunk in iter(lambda: streaming_body.read(32768), b''):
            lzma_out.write(chunk)
    return compressed_file_obj.name


def upload(file_path, bucket, key, head_response, preset):
    assert (os.path.getsize(file_path) > 0)
    print(f"upload => {bucket}/{key}")
    metadata = head_response['Metadata']
    metadata[HEADER_ORIGINAL_SIZE] = str(head_response['ContentLength'])
    metadata[HEADER_COMPRESSION_SETTING] = str(preset)

    s3.upload_file(file_path, bucket, key, ExtraArgs={
        'ContentType': LZMA_CONTENT_TYPE,
        'Metadata': metadata
    })
    return file_path


def process(bucket: str, key: str):
    print(f"process: {bucket}/{key}")
    response = s3.head_object(Bucket=bucket, Key=key)
    content_type = response['ContentType']

    if content_type in ACCEPTED_CONTENT_TYPES:
        content_length = response['ContentLength']
        # 0-9 (PRESET_EXTREME), default = 6
        preset = 4 if content_length > 500_000_000 else lzma.PRESET_DEFAULT
        body = s3.get_object(Bucket=bucket, Key=key)['Body']
        upload(compress(body, preset=preset),
               bucket="%s-processed" % bucket,
               key=key + LZMA_EXTENSION,
               head_response=response,
               preset=preset)
        return 'OK'
    else:
        print(f"Ignoring content type '{content_type}'")
        return 'IGNORED'


def lambda_handler(event, context):
    from os import getenv, system
    if getenv('LAMBDA_TASK_ROOT'):
        system('rm -rf /tmp/*')

    print(f"{context.memory_limit_in_mb=}")
    print(f"{context.get_remaining_time_in_millis()=}")

    bucket = event['Records'][0]['s3']['bucket']['name']
    key = unquote_plus(event['Records'][0]['s3']['object']['key'])
    process(bucket, key)


if __name__ == '__main__':
    import sys
    from urllib.parse import urlparse

    if len(sys.argv) > 1:
        file_or_s3_resource = sys.argv[1]
        if file_or_s3_resource.startswith('s3://'):
            s3_url = urlparse(file_or_s3_resource)
            bucket = s3_url.netloc
            key = s3_url.path[1:]
            process(bucket, key)
