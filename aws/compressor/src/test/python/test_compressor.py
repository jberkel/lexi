from unittest import TestCase
from botocore.stub import Stubber, ANY
from botocore.response import StreamingBody
from io import BytesIO
import compressor
import random


class CompressorTest(TestCase):
    def setUp(self):
        super().setUp()
        self.stubber = Stubber(compressor.s3)
        self.stubber.activate()

    def tearDown(self):
        super().tearDown()
        self.stubber.assert_no_pending_responses()
        self.stubber.deactivate()

    def test_process_ignores_irrelevant_content_types(self):
        self.stubber.add_response('head_object',
                                  service_response={'ContentType': 'application/x-foo'},
                                  expected_params={'Bucket': 'bucket', 'Key': 'key'})

        self.assertEqual('IGNORED', compressor.process('bucket', 'key'))

    def test_process_with_simple_key(self):
        download_bucket = 'bucket'
        upload_bucket = 'bucket-processed'
        key = 'key'
        CompressorTest.stub_responses(self.stubber, download_bucket, upload_bucket, key=key)
        self.assertEqual('OK', compressor.process(download_bucket, key))

    def test_process_with_key_containing_path(self):
        download_bucket = 'bucket'
        upload_bucket = 'bucket-processed'
        key = 'key/with/a/path'
        CompressorTest.stub_responses(self.stubber, download_bucket, upload_bucket, key=key)
        self.assertEqual('OK', compressor.process(download_bucket, key))

    @classmethod
    def stub_responses(cls, stubber, download_bucket, upload_bucket, key='key'):
        stubber.add_response('head_object',
                             service_response={
                                 'ContentType': 'binary/octet-stream',
                                 'ContentLength': 1234,
                                 'Metadata': {'key': 'value'}
                             },
                             expected_params={'Bucket': download_bucket, 'Key': key})

        input_stream = BytesIO(bytes([random.randint(0, 255) for _ in range(20000)]))
        length = len(input_stream.getvalue())
        stubber.add_response('get_object',
                             service_response={'Body': StreamingBody(raw_stream=input_stream,
                                                                     content_length=length)},
                             expected_params={'Bucket': download_bucket, 'Key': key})

        stubber.add_response('put_object',
                             service_response={},
                             expected_params={
                                 'Body': ANY,
                                 'Bucket': upload_bucket,
                                 'ContentType': 'application/x-lzma',
                                 'Metadata': {
                                     'key': 'value',
                                     'original-size': '1234',
                                     'compression-setting': '6'
                                 },
                                 'Key': '%s.lzma' % key
                             })
