#!/usr/bin/env python

from urllib.parse import unquote_plus, urlencode, quote
import boto3
import os
import json
import time

s3 = boto3.client('s3')
DEFAULT_BATCH_SIZE = int(os.getenv('BATCH_SIZE', 1000))


def reindex(file_name, output=None, batch_size=DEFAULT_BATCH_SIZE):
    from lexi import models
    from lexi.models.fts import FTSHeadword
    print(f"reindex {file_name} => {output}, batch_size = {batch_size:d}")

    with models.database_connection(file_name, s3client=s3) as db:
        start = time.perf_counter()
        FTSHeadword.reindex(batch_size=batch_size)
        end = time.perf_counter()
        db.vacuum_into(output)
        print(f"Done in {end - start:0.4f} seconds")


def process(bucket: str, key: str):
    print(f"process: {bucket=} {key=}")
    response = s3.head_object(Bucket=bucket, Key=key)
    content_type = response['ContentType']
    metadata = response.get('Metadata', {})

    if content_type == 'application/x-sqlite3':
        params = {
            'Metadata': json.dumps(metadata),
            'ContentType': content_type
        }
        encoded_params = urlencode(params, quote_via=quote)
        reindex(f"file://{bucket}/{key}", output=f"file://{bucket}-processed/{key}?{encoded_params}")
        return 'OK'
    else:
        print(f"Ignoring content type '{content_type}'")
        return 'IGNORED'


def lambda_handler(event, context):
    from os import getenv, system
    if getenv('LAMBDA_TASK_ROOT'):
        system('rm -rf /tmp/*')

    print(f"{context.memory_limit_in_mb=}")
    print(f"{context.get_remaining_time_in_millis()=}")

    bucket = event['Records'][0]['s3']['bucket']['name']
    key = unquote_plus(event['Records'][0]['s3']['object']['key'])
    process(bucket, key)


if __name__ == '__main__':
    import sys
    from urllib.parse import urlparse

    if len(sys.argv) > 1:
        file_or_s3_resource = sys.argv[1]
        if file_or_s3_resource.startswith('s3://'):
            s3_url = urlparse(file_or_s3_resource)
            bucket = s3_url.netloc
            key = s3_url.path[1:]
            process(bucket, key)
        else:
            reindex(file_or_s3_resource)
