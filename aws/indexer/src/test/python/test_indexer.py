from unittest import TestCase
from unittest.mock import patch

from botocore.stub import Stubber

import indexer


class IndexerTest(TestCase):
    def setUp(self):
        super().setUp()
        self.stubber = Stubber(indexer.s3)
        self.stubber.activate()

    def tearDown(self):
        super().tearDown()
        self.stubber.assert_no_pending_responses()
        self.stubber.deactivate()

    @patch('lexi.models.fts.FTSHeadword')
    @patch('lexi.models.database_connection')
    def test_process(self, db_mock, index_mock):
        self.stubber.add_response('head_object',
                                  service_response={
                                      'Metadata': {
                                          'key': 'value'
                                      },
                                      'ContentType': 'application/x-sqlite3',
                                      'ContentLength': 1
                                  },
                                  expected_params={'Bucket': 'bucket', 'Key': 'key'})

        self.assertEqual('OK', indexer.process('bucket', 'key'))
        index_mock.reindex.assert_called_with(batch_size=1000)

    def test_process_ignores_irrelevant_content_types(self):
        self.stubber.add_response('head_object',
                                  service_response={'ContentType': 'application/x-foo'},
                                  expected_params={'Bucket': 'bucket', 'Key': 'key'})

        self.assertEqual('IGNORED', indexer.process('bucket', 'key'))
