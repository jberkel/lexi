from unittest import TestCase
from botocore.stub import Stubber
import publisher

ALL_USERS = 'http://acs.amazonaws.com/groups/global/AllUsers'


class PublisherTest(TestCase):
    def setUp(self):
        super().setUp()
        self.stubber = Stubber(publisher.s3)
        self.stubber.activate()

    def tearDown(self):
        super().tearDown()
        self.stubber.deactivate()

    def test_process_object_already_public(self):
        self.stubber.add_response('get_object_acl',
                                  service_response={'Owner': {'ID': 'ownerId'},
                                                    'Grants':
                                                        [
                                                            {
                                                                'Grantee': {
                                                                    'Type': 'CanonicalUser',
                                                                    'ID': 'userId'
                                                                },
                                                                'Permission': 'FULL_CONTROL'
                                                            },
                                                            {
                                                                'Grantee': {
                                                                    'Type': 'CanonicalUser',
                                                                    'ID': 'userId'
                                                                },
                                                                'Permission': 'WRITE_ACP'
                                                            },
                                                            {
                                                                'Grantee': {
                                                                    'Type': 'Group',
                                                                    'URI': ALL_USERS
                                                                }, 'Permission': 'READ'
                                                            }
                                                        ]
                                                    },
                                  expected_params={'Bucket': 'bucket', 'Key': 'key'})
        self.assertEqual('OK', publisher.process('bucket', 'key'))

    def test_process_object_private(self):
        self.stubber.add_response('get_object_acl',
                                  service_response={'Owner': {'ID': 'ownerId'},
                                                    'Grants': [
                                                        {
                                                            'Grantee': {
                                                                'Type': 'CanonicalUser',
                                                                'ID': 'userId'
                                                            },
                                                            'Permission': 'FULL_CONTROL'
                                                        },
                                                        {
                                                            'Grantee': {
                                                                'Type': 'CanonicalUser',
                                                                'ID': 'userId'
                                                            },
                                                            'Permission': 'WRITE_ACP'
                                                        }
                                                    ]},
                                  expected_params={'Bucket': 'bucket', 'Key': 'key'})
        self.stubber.add_response('put_object_acl',
                                  service_response={'RequestCharged': 'requester'},
                                  expected_params={'Bucket': 'bucket',
                                                   'Key': 'key', 'ACL': 'public-read'})
        self.assertEqual('OK', publisher.process('bucket', 'key'))
