from urllib.parse import unquote_plus
import boto3

s3 = boto3.client('s3')


def is_public(bucket, key):
    response = s3.get_object_acl(Bucket=bucket, Key=key)
    public_read = ['groups/global/AllUsers' in g['Grantee'].get('URI', '')
                   for g in response['Grants'] if g['Permission'] == 'READ']
    return any(public_read)


def make_public(bucket, key):
    print(f'make public: {bucket}/{key}')
    s3.put_object_acl(Bucket=bucket, Key=key, ACL='public-read')


def process(bucket, key):
    print(f'process: {bucket}/{key}')
    if not is_public(bucket, key):
        make_public(bucket, key)
    return 'OK'


def lambda_handler(event, context):
    from os import getenv, system
    if getenv('LAMBDA_TASK_ROOT'):
        system('rm -rf /tmp/*')

    print("CloudWatch log stream name:", context.log_stream_name)

    bucket = event['Records'][0]['s3']['bucket']['name']
    key = unquote_plus(event['Records'][0]['s3']['object']['key'])
    process(bucket, key)
