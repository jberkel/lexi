#!/usr/bin/env python
from urllib.parse import unquote_plus
from itertools import chain

import boto3
from botocore.exceptions import ClientError

s3 = boto3.client('s3')

TARGET_BUCKET = 'TargetBucket'


# ideas stolen from
# https://github.com/eleven41/aws-lambda-copy-s3-objects/
def duplicate(bucket, key, destinations):
    head_response = s3.head_object(Bucket=bucket, Key=key)
    for destination_bucket in destinations:
        print(f'copy to bucket {destination_bucket}')
        s3.copy_object(CopySource={'Bucket': bucket, 'Key': key},
                       Bucket=destination_bucket,
                       Key=key,
                       ContentType=head_response['ContentType'],
                       Metadata=head_response['Metadata'],
                       StorageClass=head_response.get('StorageClass', 'STANDARD'))


# https://github.com/boto/boto3/issues/341
def tag_set_for_bucket(bucket):
    try:
        response = s3.get_bucket_tagging(Bucket=bucket)
        if 'TagSet' in response:
            return response['TagSet']
        else:
            return []
    except ClientError as e:
        if 'Error' in e.response and e.response['Error'].get('Code', None) == 'NoSuchTagSet':
            return []
        else:
            raise e


def process(bucket, key):
    print(f"process: {bucket}/{key}")
    tag_set = tag_set_for_bucket(bucket)
    target_buckets = [tags['Value'] for tags in tag_set
                      if 'Key' in tags and tags['Key'] == TARGET_BUCKET]
    destinations = list(chain.from_iterable([target_bucket.split(' ')
                                             for target_bucket in target_buckets]))
    print(f'{destinations=}')

    if len(destinations) > 0:
        duplicate(bucket, key, destinations)
        return 'OK'
    else:
        print('no valid destinations found, ignoring')
        return 'IGNORED'


def lambda_handler(event, context):
    from os import getenv, system
    if getenv('LAMBDA_TASK_ROOT'):
        system('rm -rf /tmp/*')

    print("CloudWatch log stream name:", context.log_stream_name)

    bucket = event['Records'][0]['s3']['bucket']['name']
    key = unquote_plus(event['Records'][0]['s3']['object']['key'])
    process(bucket, key)
