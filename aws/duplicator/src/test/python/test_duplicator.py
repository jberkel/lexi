from unittest import TestCase
from botocore.stub import Stubber

import duplicator


class DuplicatorTest(TestCase):
    def setUp(self):
        super().setUp()
        self.stubber = Stubber(duplicator.s3)
        self.stubber.activate()

    def tearDown(self):
        super().tearDown()
        self.stubber.assert_no_pending_responses()
        self.stubber.deactivate()

    def test_process_without_target_bucket_is_ignored(self):
        self.stubber.add_response('get_bucket_tagging',
                                  service_response={'TagSet': []},
                                  expected_params={'Bucket': 'bucket'})
        self.assertEqual('IGNORED', duplicator.process('bucket', 'key'))

    def test_process_no_such_tag_set_is_raised(self):
        self.stubber.add_client_error('get_bucket_tagging', service_error_code='NoSuchTagSet')
        self.assertEqual('IGNORED', duplicator.process('bucket', 'key'))

    def test_process_simple_target_bucket_is_copied(self):
        self.stubber.add_response('get_bucket_tagging',
                                  service_response={'TagSet': [{'Key': 'TargetBucket',
                                                                'Value': 'destination'}]},
                                  expected_params={'Bucket': 'bucket'})

        self.stubber.add_response('head_object',
                                  service_response={
                                      'ContentType': 'binary/octet-stream',
                                      'ContentLength': 1,
                                      'Metadata': {'key': 'value'},
                                      'StorageClass': 'STANDARD'
                                  },
                                  expected_params={'Bucket': 'bucket', 'Key': 'key'})

        self.stubber.add_response('copy_object',
                                  service_response={},
                                  expected_params={
                                      'CopySource': {'Bucket': 'bucket', 'Key': 'key'},
                                      'Bucket': 'destination',
                                      'Key': 'key',
                                      'ContentType': 'binary/octet-stream',
                                      'StorageClass': 'STANDARD',
                                      'Metadata': {'key': 'value'}
                                  })

        self.assertEqual('OK', duplicator.process('bucket', 'key'))

    def test_process_multiple_target_bucket_is_copied(self):
        self.stubber.add_response('get_bucket_tagging',
                                  service_response={
                                      'TagSet': [{'Key': 'TargetBucket',
                                                  'Value': 'destination1 destination2'}]
                                  },
                                  expected_params={'Bucket': 'bucket'})

        self.stubber.add_response('head_object',
                                  service_response={
                                      'ContentType': 'binary/octet-stream',
                                      'ContentLength': 1,
                                      'Metadata': {'key': 'value'},
                                      'StorageClass': 'STANDARD'
                                  },
                                  expected_params={'Bucket': 'bucket', 'Key': 'key'})

        for dest_bucket in ['destination1', 'destination2']:
            self.stubber.add_response('copy_object',
                                      service_response={},
                                      expected_params={
                                          'CopySource': {'Bucket': 'bucket', 'Key': 'key'},
                                          'Bucket': dest_bucket,
                                          'Key': 'key',
                                          'ContentType': 'binary/octet-stream',
                                          'StorageClass': 'STANDARD',
                                          'Metadata': {'key': 'value'}
                                      })

        self.assertEqual('OK', duplicator.process('bucket', 'key'))
