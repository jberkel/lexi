#!/usr/bin/env python
from typing import Iterable, Tuple
from urllib.parse import unquote_plus, urlencode, quote

import boto3
from botocore.response import StreamingBody
from lexi import LANGUAGES
from lexi.serialization import Page
from lexi.dictionary import DictionaryDatabase
import re

from lexi.rank import Ranker

s3 = boto3.client('s3')

FILENAME = re.compile(r"\A(\d{8})-(\w{2,3})\.ndjson\Z")


def process(bucket: str, key: str):
    print(f"process bucket {bucket}, key={key}")
    file_name = key.split('/')[-1]

    dump_version, language_code, language_name = _dump_infos(file_name)

    response = s3.get_object(Bucket=bucket, Key=key)
    body: StreamingBody = response['Body']
    page_gen = (Page.from_json(line) for line in body.iter_lines())

    _build(dump_version, language_name, language_code, page_gen)


def _dump_infos(file_name: str) -> Tuple[str, str, str]:
    if match := FILENAME.search(file_name):
        version = match.group(1)
        code = match.group(2)
        name = LANGUAGES.get(code, None)
        if not name:
            raise Exception(f"unknown language code: {code}")
        return version, code, name
    else:
        raise Exception("could not detect language code")


def _build(dump_version: str,
           language_name: str,
           language_code: str,
           page_gen: Iterable[Page]):

    print(f"dump version {dump_version}, {language_name} ({language_code})")
    with DictionaryDatabase(':memory:', s3client=s3) as db:
        db.make_dictionary(
            dump_version=dump_version,
            language_code=language_code,
            language_name=language_name,
            pages=page_gen,
            ranker=Ranker()
        )

        encoded_params = urlencode({
            'ContentType': 'application/x-sqlite3'
        }, quote_via=quote)
        output = (f"file://moti-app-link/db/{dump_version}/"
                  f"wiktionary-{dump_version}-{language_code}.sqlite?{encoded_params}")
        db.vacuum_into(output)


# https://docs.aws.amazon.com/lambda/latest/dg/python-context.html
def lambda_handler(event, context):
    from os import getenv, system
    if getenv('LAMBDA_TASK_ROOT'):
        system('rm -rf /tmp/*')

    print(f"{context.memory_limit_in_mb=}")
    print(f"{context.get_remaining_time_in_millis()=}")

    bucket = event['Records'][0]['s3']['bucket']['name']
    key = unquote_plus(event['Records'][0]['s3']['object']['key'])
    process(bucket, key)


def _pages(file_name: str) -> Iterable[Page]:
    with open(file_name, 'r') as file_input:
        for line in file_input:
            yield Page.from_json(line)


if __name__ == '__main__':
    import sys
    from urllib.parse import urlparse

    if len(sys.argv) > 1:
        if sys.argv[1].startswith("s3:"):
            s3_resource = sys.argv[1]
            s3_url = urlparse(s3_resource)
            bucket = s3_url.netloc
            key = s3_url.path[1:]
            process(bucket, key)
        else:
            file_name = sys.argv[1]
            version, language_code, language_name = _dump_infos(file_name)
            _build(version, language_name, language_code, _pages(file_name))
