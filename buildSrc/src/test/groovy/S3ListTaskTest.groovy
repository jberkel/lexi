import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.ObjectListing
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.S3ObjectSummary
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test

import static org.mockito.ArgumentMatchers.any
import static org.mockito.ArgumentMatchers.eq
import static org.mockito.Mockito.mock
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.when

class S3ListTaskTest {
    @Test(expected = IllegalArgumentException.class)
    void testRequiresBucketAndPrefix() throws Exception {
        def task = createTask()
        task.list()
    }

    @Test(expected = IllegalArgumentException.class)
    void testRequiresBucket() throws Exception {
        def task = createTask()
        task.list()
        task.prefix = 'prefix'
    }

    @Test(expected = IllegalArgumentException.class)
    void testRequiresPrefix() throws Exception {
        def task = createTask()
        task.list()
        task.bucket = 'bucket'
    }

    @Test void testList() throws Exception {
        def task = createTask()
        def client = mock(AmazonS3.class)
        task.client = client
        task.bucket = 'bucket'
        task.prefix = 'prefix'

        def objectListing = new ObjectListing()
        def s3ObjectSummary = new S3ObjectSummary()
        s3ObjectSummary.key = 'key'
        objectListing.objectSummaries = [ s3ObjectSummary ]

        def metadata = new ObjectMetadata()

        when(client.listObjects(any(String.class), any(String.class))).thenReturn(objectListing)
        when(client.getObjectMetadata(any(String.class), any(String.class))).thenReturn(metadata)

        task.list()

        verify(client).listObjects(eq('bucket'), eq('prefix'))
        verify(client).getObjectMetadata(eq('bucket'), eq('key'))
    }

    static S3ListTask createTask() {
        def project = ProjectBuilder.builder().build()
        return project.task('test', type: S3ListTask) as S3ListTask
    }
}
