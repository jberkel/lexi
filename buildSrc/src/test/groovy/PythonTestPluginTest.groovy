import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test

import static org.assertj.core.api.Assertions.assertThat

class PythonTestPluginTest {
    @Test void pluginCreatesTestTaskForPython() throws Exception {
        def project = projectWithAppliedPlugin()
        def testTask = project.tasks.findByName('testPython') as PythonTest
        assertThat(testTask).isNotNull()
        assertThat(testTask.pythonInterpreter).isEqualTo('python3')
    }

    @Test void pluginCreatesIntegrationTestTaskForPython() throws Exception {
        def project = projectWithAppliedPlugin()
        def testTask = project.tasks.findByName('integrationTestPython') as PythonTest
        assertThat(testTask).isNotNull()
        assertThat(testTask.pythonInterpreter).isEqualTo('python3')
    }

    @Test void pluginCreatesIntegrationTestPythonTask() throws Exception {
        def project = projectWithAppliedPlugin()
        def integrationTestTask = project.tasks.findByName('integrationTestPython')
        assertThat(integrationTestTask).isNotNull()
    }

    static Project projectWithAppliedPlugin(Class plugin = PythonTestPlugin) {
        Project project = ProjectBuilder.builder().build()
        for (String dir : [PythonPlugin.PYTHON_TEST_SRC_DIR,
                           PythonPlugin.PYTHON_INTEGRATION_TEST_SRC_DIR]) {
            new File(project.rootDir, dir).mkdirs()
        }
        project.pluginManager.apply(plugin)
        return project
    }
}
