import org.junit.Test

import static org.assertj.core.api.Assertions.assertThat

class RequirementsParserTest {
    @Test void testParse() {
        def requirements = RequirementsParser.parse(
                new File(getClass().getResource("/fixtures/test-requirements.txt").file))
        assertThat(requirements).containsEntry('baz', '2.8.3')
        assertThat(requirements).containsEntry('test', '3.14.1.dev0+olha.0e8720f')
    }
}
