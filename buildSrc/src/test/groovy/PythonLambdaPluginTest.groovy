import org.gradle.api.Project
import org.gradle.api.tasks.bundling.Zip
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test

import static com.amazonaws.services.lambda.model.Runtime.Python39
import static org.assertj.core.api.Assertions.assertThat

class PythonLambdaPluginTest {
    @Test void pluginCreatesDeployTaskWithDefaults() throws Exception {
        def project = projectWithAppliedPlugin()
        def deployTask = project.tasks.findByName('deploy') as AWSLambdaTask
        assertThat(deployTask).isNotNull()
        assertThat(deployTask.memorySize).isEqualTo(128)
        assertThat(deployTask.runtime).isEqualTo(Python39)
        assertThat(deployTask.zipFile.getName()).isEqualTo('test-lambda.zip')
    }

    @Test void pluginCreatesZipTaskWithDefaults() throws Exception {
        def project = projectWithAppliedPlugin()
        def zipTask = project.tasks.findByName('zip') as Zip
        assertThat(zipTask).isNotNull()
        assertThat(zipTask.archiveName).isEqualTo('test-lambda.zip')
        assertThat(zipTask.excludes).containsAll(['**/*.pyc', '**/__pycache__'])
        assertThat(zipTask.destinationDir).isEqualTo(project.buildDir)
    }

    @Test void pluginCreatesTestTaskForPython3() throws Exception {
        def project = projectWithAppliedPlugin()
        def testTask = project.tasks.findByName('testPython') as PythonTest
        assertThat(testTask).isNotNull()
        assertThat(testTask.pythonInterpreter).isEqualTo('python3')
    }

    static Project projectWithAppliedPlugin(Class plugin = PythonLambdaPlugin) {
        def project = ProjectBuilder.builder().build()
        for (String dir : [PythonPlugin.PYTHON_TEST_SRC_DIR,
                           PythonPlugin.PYTHON_INTEGRATION_TEST_SRC_DIR]) {
            new File(project.rootDir, dir).mkdirs()
        }
        project.pluginManager.apply(plugin)
        return project
    }
}


