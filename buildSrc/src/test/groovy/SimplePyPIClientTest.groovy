import org.junit.Before
import org.junit.Rule
import org.junit.Test
import software.betamax.ConfigurationBuilder
import software.betamax.TapeMode
import software.betamax.junit.Betamax
import software.betamax.junit.RecorderRule

import static org.assertj.core.api.Assertions.assertThat

class SimplePyPIClientTest {
    private SimplePyPIClient subject

    @Rule
    public RecorderRule recorder = new RecorderRule(new ConfigurationBuilder().sslEnabled(true).build())

    @Before
    void setUp() throws Exception {
        subject = new SimplePyPIClient([new URL('http://zegoggl.es:8080/simple')])
    }

    @Betamax(tape = "apsw", mode=TapeMode.READ_ONLY)
    @Test
    void testResolveApsw() {
        URL resolved = subject.resolve("apsw", "3.14.1.dev0+olha.0e8720f", "cp27", "linux_x86_64")
        assertThat(resolved.toString()).isEqualTo("http://zegoggl.es:8080/packages/apsw-3.14.1.dev0+olha.0e8720f-cp27-cp27mu-linux_x86_64.whl")
    }

    @Betamax(tape = "not-found", mode=TapeMode.READ_ONLY)
    @Test(expected = PyPIClientException.class)
    void testThrowsClientExceptionIfNotFound() throws Exception {
        subject.resolve("xxx", "1.0.0")
    }
}
