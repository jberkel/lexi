import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test

import static org.assertj.core.api.Assertions.assertThat

class PythonPluginTest {
    @Test
    void pluginCreatesDefaults() throws Exception {
        def project = projectWithAppliedPlugin()

        assertThat(project.relativePath(project.python_src_dir)).isEqualTo('src/main/python')
        assertThat(project.relativePath(project.python_test_src_dir)).isEqualTo('src/test/python')
        assertThat(project.relativePath(project.python_integration_test_src_dir)).isEqualTo('src/it/python')
        assertThat(project.default_python_interpreter).isEqualTo('python3')
    }

    static Project projectWithAppliedPlugin(Class plugin = PythonPlugin) {
        def project = ProjectBuilder.builder().build()
        project.pluginManager.apply(plugin)
        return project
    }
}
