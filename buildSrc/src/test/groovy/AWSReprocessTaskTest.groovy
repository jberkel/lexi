import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.CopyObjectRequest
import com.amazonaws.services.s3.model.ObjectListing
import com.amazonaws.services.s3.model.S3ObjectSummary
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test
import org.mockito.ArgumentCaptor

import static org.assertj.core.api.Assertions.assertThat
import static org.mockito.ArgumentMatchers.any
import static org.mockito.ArgumentMatchers.eq
import static org.mockito.Mockito.mock
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.when

class AWSReprocessTaskTest {
    @Test(expected = IllegalArgumentException.class)
    void testRequiresBucketAndPrefix() throws Exception {
        def task = createTask()
        task.reprocess()
    }

    @Test(expected = IllegalArgumentException.class)
    void testRequiresBucket() throws Exception {
        def task = createTask()
        task.reprocess()
        task.prefix = 'prefix'
    }

    @Test(expected = IllegalArgumentException.class)
    void testRequiresPrefix() throws Exception {
        def task = createTask()
        task.reprocess()
        task.bucket = 'bucket'
    }

    @Test void testReprocess() {
        def task = createTask()
        def client = mock(AmazonS3.class)
        task.client = client
        task.bucket = 'bucket'
        task.prefix = 'prefix'

        def objectListing = new ObjectListing()
        def s3ObjectSummary = new S3ObjectSummary()
        s3ObjectSummary.key = 'key'
        s3ObjectSummary.storageClass = 'REDUCED_REDUNDANCY'
        objectListing.objectSummaries = [ s3ObjectSummary ]
        when(client.listObjects(any(String.class), any(String.class))).thenReturn(objectListing)

        task.reprocess()

        verify(client).listObjects(eq('bucket'), eq('prefix'))

        def captor = ArgumentCaptor.forClass(CopyObjectRequest.class)
        verify(client).copyObject(captor.capture())

        assertThat(captor.value.destinationKey).isEqualTo('key')
        assertThat(captor.value.destinationBucketName).isEqualTo('bucket')
        assertThat(captor.value.storageClass).isEqualTo('REDUCED_REDUNDANCY')
    }

    static AWSReprocessTask createTask() {
        def project = ProjectBuilder.builder().build()
        return project.task('test', type: AWSReprocessTask) as AWSReprocessTask
    }
}
