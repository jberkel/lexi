import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test

import static org.assertj.core.api.Assertions.assertThat

class PythonLintPluginTest {
    @Test void pluginCreatesPythonLintTaskForPython2() throws Exception {
        def project = projectWithAppliedPlugin()
        def testTask = project.tasks.findByName('pylint') as PythonLint
        assertThat(testTask.pythonInterpreter).isEqualTo('python3')
    }

    static Project projectWithAppliedPlugin(Class plugin = PythonLintPlugin) {
        def project = ProjectBuilder.builder().build()
        project.pluginManager.apply(plugin)
        return project
    }
}
