import com.amazonaws.services.logs.AWSLogs
import com.amazonaws.services.logs.model.DescribeLogStreamsRequest
import com.amazonaws.services.logs.model.DescribeLogStreamsResult
import com.amazonaws.services.logs.model.GetLogEventsRequest
import com.amazonaws.services.logs.model.GetLogEventsResult
import com.amazonaws.services.logs.model.LogStream
import com.amazonaws.services.logs.model.OutputLogEvent
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test
import org.mockito.Mockito

import static org.assertj.core.api.Assertions.assertThat
import static org.mockito.ArgumentMatchers.any
import static org.mockito.Mockito.when

class AWSLogTaskTest {

    @Test void testTaskCreation() {
        def task = createTask()
        assertThat(task.logGroupName).isNull()
        assertThat(task.region).isNull()
    }

    @Test(expected = IllegalArgumentException.class)
    void testDisplayWithoutLogGroupName() {
        def task = createTask()
        task.display()
    }

    @Test void testDisplayWithoutLogStreamsResults() {
        def client = Mockito.mock(AWSLogs.class)
        def task = createTask()
        task.client = client
        def logStreamsResult = new DescribeLogStreamsResult()
        when(client.describeLogStreams(any(DescribeLogStreamsRequest.class))).thenReturn(logStreamsResult)
        task.logGroupName = '/aws/lambda/Test'
        task.display()
    }

    @Test void testDisplayWithResults() {
        def client = Mockito.mock(AWSLogs.class)
        def task = createTask()
        task.client = client

        def logStreamsResult = new DescribeLogStreamsResult()
        def logStream = new LogStream()
        logStreamsResult.logStreams = [ logStream ]
        def logEventsResult = new GetLogEventsResult()
        def outputLogEvent = new OutputLogEvent()
        outputLogEvent.timestamp = System.currentTimeMillis()
        outputLogEvent.message = "Event"
        logEventsResult.events = [ outputLogEvent ]

        when(client.describeLogStreams(any(DescribeLogStreamsRequest.class))).thenReturn(logStreamsResult)
        when(client.getLogEvents(any(GetLogEventsRequest.class))).thenReturn(logEventsResult)

        task.logGroupName = '/aws/lambda/Test'
        task.display()
    }


    static AWSLogTask createTask() {
        def project = ProjectBuilder.builder().build()
        return project.task('test', type: AWSLogTask) as AWSLogTask
    }
}
