import org.gradle.testkit.runner.GradleRunner
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

import static org.assertj.core.api.Assertions.assertThat

class FunctionalBuildTest {
    @Rule
    public final TemporaryFolder testProjectDir = new TemporaryFolder()
    private File buildFile

    @Before
    void setup() throws IOException {
        buildFile = testProjectDir.newFile('build.gradle')
    }

    @Test
    void testHelloWorldTask() {
        String buildFileContent = """
            task helloWorld {
                 doLast {
                      println 'Hello world!'
                 }
             }
         """
        buildFile.write(buildFileContent)

        def result = GradleRunner.create()
            .withProjectDir(testProjectDir.getRoot())
            .withArguments("helloWorld")
            .build()

        assertThat(result.getOutput()).contains("Hello world!")
    }
}
