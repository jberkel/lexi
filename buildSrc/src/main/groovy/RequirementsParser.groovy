class RequirementsParser {
    static Map<String, String> parse(File file) {
        def requirements = [:]
        file.readLines().findAll { l -> !l.startsWith("#") }.collect { l ->
            def parts = l.split('==', 2)
            if (parts.length == 2) {
                requirements[parts[0]] = parts[1]
            }
        }
        return requirements
    }
}
