import org.gradle.api.tasks.TaskAction

class S3ListTask extends S3BaseTask {
    @TaskAction
    void list() {
        if (bucket == null || bucket.isEmpty()) throw new IllegalArgumentException("no bucket specified")
        if (prefix == null || prefix.isEmpty()) throw new IllegalArgumentException("no prefix specified")

        def listing = client.listObjects(bucket, prefix)
        listing.getObjectSummaries().each { summary ->
            if (!summary.key.endsWith('/')) {
                def metadata = client.getObjectMetadata(bucket, summary.key)
                logger.warn("key=${summary.key}\tsize=${summary.size}\tmeta=${metadata.userMetadata}")
            }
        }
    }
}
