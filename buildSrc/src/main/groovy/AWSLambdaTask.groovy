import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.lambda.AWSLambda
import com.amazonaws.services.lambda.AWSLambdaClientBuilder
import com.amazonaws.services.lambda.model.CreateFunctionRequest
import com.amazonaws.services.lambda.model.CreateFunctionResult
import com.amazonaws.services.lambda.model.DeadLetterConfig
import com.amazonaws.services.lambda.model.Environment
import com.amazonaws.services.lambda.model.FunctionCode
import com.amazonaws.services.lambda.model.FunctionConfiguration
import com.amazonaws.services.lambda.model.GetFunctionConfigurationRequest
import com.amazonaws.services.lambda.model.GetFunctionConfigurationResult
import com.amazonaws.services.lambda.model.GetFunctionRequest
import com.amazonaws.services.lambda.model.GetFunctionResult
import com.amazonaws.services.lambda.model.PutFunctionEventInvokeConfigRequest
import com.amazonaws.services.lambda.model.ResourceNotFoundException
import com.amazonaws.services.lambda.model.Runtime
import com.amazonaws.services.lambda.model.UpdateFunctionCodeRequest
import com.amazonaws.services.lambda.model.UpdateFunctionCodeResult
import com.amazonaws.services.lambda.model.UpdateFunctionConfigurationRequest
import com.amazonaws.services.lambda.model.UpdateFunctionConfigurationResult
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction

import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel

class AWSLambdaTask extends DefaultTask {
    @Input
    String functionName

    @Input
    String functionDescription

    @Input
    String handler

    @Input
    Integer lambdaTimeout

    @Input
    Integer memorySize

    @Input
    Integer maximumRetryAttempts  // 0–2

    @Input
    String role

    @Input @Optional
    String deadLetterQueueArn

    @Input
    Runtime runtime

    @InputFile
    File zipFile

    @Input
    String region

    @Input @Optional
    Map<String, String> environment

    @Input @Optional
    Boolean publish

    @TaskAction
    upload() {
        AWSLambda lambda = AWSLambdaClientBuilder.standard()
            .withRegion(region)
            .withCredentials(new EnvironmentVariableCredentialsProvider())
            .build()

        try {
            GetFunctionResult result = lambda.getFunction(new GetFunctionRequest().withFunctionName(functionName))
            logger.info("result: "+result)

            FunctionConfiguration config = result.getConfiguration() ?: new FunctionConfiguration().withRuntime(runtime)
            updateFunctionConfiguration(lambda, config)
            updateFunctionCode(lambda)
        } catch (ResourceNotFoundException e) {
            logger.warn(e.getMessage())
            createFunction(lambda)
        }
    }

    private void updateFunctionConfiguration(AWSLambda lambda, FunctionConfiguration config) {
        logger.info("updateFunctionConfiguration: " +config)
        String updateFunctionName = functionName ?: config.getFunctionName()
        String updateRole = role ?: config.getRole()
        Runtime updateRuntime = runtime
        String updateHandler = handler ?: config.getHandler()
        String updateDescription = functionDescription ?: config.getDescription()
        Integer updateTimeout = lambdaTimeout > 0 ? lambdaTimeout : config.getTimeout()
        Integer updateMemorySize = memorySize > 0 ? memorySize : config.getMemorySize()

        UpdateFunctionConfigurationRequest request = new UpdateFunctionConfigurationRequest()
            .withFunctionName(updateFunctionName)
            .withRole(updateRole)
            .withRuntime(updateRuntime)
            .withHandler(updateHandler)
            .withDescription(updateDescription)
            .withTimeout(updateTimeout)
            .withEnvironment(new Environment().withVariables(environment))
            .withMemorySize(updateMemorySize)

        if (deadLetterQueueArn != null) {
            request.withDeadLetterConfig(new DeadLetterConfig().withTargetArn(deadLetterQueueArn))
        }

        UpdateFunctionConfigurationResult updateFunctionConfiguration = lambda.updateFunctionConfiguration(request)
        logger.info("Update Lambda function configuration requested: {}", updateFunctionConfiguration.getFunctionArn())

        // set invokeConfig
        PutFunctionEventInvokeConfigRequest putFunctionEventInvokeConfigRequest = new PutFunctionEventInvokeConfigRequest()
                .withMaximumRetryAttempts(maximumRetryAttempts)
                .withFunctionName(updateFunctionName)
        lambda.putFunctionEventInvokeConfig(putFunctionEventInvokeConfigRequest)

        GetFunctionConfigurationRequest configurationRequest = new GetFunctionConfigurationRequest()
                .withFunctionName(updateFunctionName)
        GetFunctionConfigurationResult updatedConfig = lambda.getFunctionConfiguration(configurationRequest)
        while (updatedConfig.lastUpdateStatus == "InProgress") {
            logger.info("waiting for config to update")
            Thread.sleep(1000)
            updatedConfig = lambda.getFunctionConfiguration(configurationRequest)
        }
        logger.info("updateConfig: {}", updatedConfig.lastUpdateStatus)
    }

    private void updateFunctionCode(AWSLambda lambda) throws IOException {
        logger.info("updateFunctionCode")

        UpdateFunctionCodeRequest request = new UpdateFunctionCodeRequest()
                .withFunctionName(getFunctionName())

        try (RandomAccessFile raf = new RandomAccessFile(getZipFile(), "r"); FileChannel channel = raf.getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size())
            buffer.load()
            request = request.withZipFile(buffer)
        }

        if (publish != null) {
            request.withPublish(publish)
        }

        UpdateFunctionCodeResult updateFunctionCode = lambda.updateFunctionCode(request)
        getLogger().info("Update Lambda function requested: {}", updateFunctionCode.getFunctionArn())
    }

    private void createFunction(AWSLambda lambda) throws IOException {
        logger.info("createFunction")

        CreateFunctionRequest request = new CreateFunctionRequest()
            .withFunctionName(functionName)
            .withRuntime(runtime)
            .withRole(role)
            .withHandler(handler)
            .withDescription(functionDescription)
            .withTimeout(lambdaTimeout)
            .withMemorySize(memorySize)
            .withPublish(publish)
            .withEnvironment(new Environment().withVariables(environment))

        if (deadLetterQueueArn != null) {
            request.withDeadLetterConfig(new DeadLetterConfig().withTargetArn(deadLetterQueueArn))
        }

        try (RandomAccessFile raf = new RandomAccessFile(zipFile, "r");
             FileChannel channel = raf.getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size())
            buffer.load()

            request.withCode(new FunctionCode().withZipFile(buffer))
        }
        CreateFunctionResult result = lambda.createFunction(request)
        logger.info("result="+result)
    }
}
