import com.amazonaws.services.s3.model.AbortIncompleteMultipartUpload
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration
import com.amazonaws.services.s3.model.lifecycle.LifecycleFilter
import com.amazonaws.services.s3.model.lifecycle.LifecyclePrefixPredicate
import org.gradle.api.tasks.TaskAction

class AWSBucketLifecycleTask extends S3BaseTask {
    @TaskAction
    list() {
        if (bucket == null) {
            throw new IllegalArgumentException("no bucket configured")
        }
        def config = client.getBucketLifecycleConfiguration(bucket)
        if (config == null) {
            config = new BucketLifecycleConfiguration()
        }
        logger.info("setting content expiration lifecycle rule for bucket '$bucket'")
        client.setBucketLifecycleConfiguration(bucket, config.withRules(expireContentRule()))
    }


    // After an Object Expiration rule is added, the rule is applied to objects that already exist in the bucket as
    // well as new objects added to the bucket. Once objects are past their expiration date, they are identified and
    // queued for removal.
    // https://aws.amazon.com/s3/faqs/
    static BucketLifecycleConfiguration.Rule expireContentRule() {
        return new BucketLifecycleConfiguration.Rule()
                .withExpirationInDays(1)
                .withStatus(BucketLifecycleConfiguration.ENABLED)
                .withId("delete_old_files")
                .withFilter(new LifecycleFilter(new LifecyclePrefixPredicate("")))
                .withAbortIncompleteMultipartUpload(new AbortIncompleteMultipartUpload().withDaysAfterInitiation(1))
    }
}
