import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal

class PythonLint extends PythonVirtualEnvExec {
    @Internal File pythonSrcDir = project.file(PythonPlugin.PYTHON_SRC_DIR)
    @Internal File pythonTestDir = project.file(PythonPlugin.PYTHON_TEST_SRC_DIR)

    @Input def includes = ['**/*.py']
    @Input def excludes = []

    @Override
    protected void exec() {
        pythonPath += project.files(pythonSrcDir)
        def srcFiles = project.fileTree(dir: pythonSrcDir, includes: includes, excludes: excludes)
        def testFiles = project.fileTree(dir: pythonTestDir, includes: includes, excludes: excludes)
        args = ['-m', 'flake8']
        args += srcFiles
        args += testFiles
        super.exec()
    }
}
