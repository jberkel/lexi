import org.jsoup.Jsoup
import org.jsoup.UnsupportedMimeTypeException
import org.jsoup.nodes.Element

import java.util.regex.Pattern

class SimplePyPIClient {
    private static URL DEFAULT_INDEX_URL = new URL('https://pypi.org/simple')
    private static Pattern PEP440_VERSION_RE = Pattern.compile("v?(\\d+!)?(\\d+(\\.\\d+)*)((a|b|c|rc)(\\d+))?(\\.(post)(\\d+))?(\\.(dev)(\\d+))?(\\+([a-zA-Z\\d]+(\\.[a-zA-Z\\d]+)?))?")
    private static Pattern WHEEL_RE = Pattern.compile("^(?<namever>(?<name>.+?)-(?<ver>\\d.*?))((-(?<build>\\d.*?))?-(?<pyver>.+?)-(?<abi>.+?)-(?<plat>.+?)\\.whl|\\.dist-info)\$")
    private static final String TAR_GZ = '.tar.gz'

    private Map<VersionDescriptor, URL> cache = [:]
    private List<URL> repositories

    SimplePyPIClient(String url) {
        this([
           new URL(url),
           DEFAULT_INDEX_URL
        ])
    }

    SimplePyPIClient(List<URL> repositories) {
        this.repositories = repositories
    }

    URL resolve(String name, String version, String pythonVersion=null, String platform=null)
        throws PyPIClientException
    {
        return resolve(new VersionDescriptor(name, version, pythonVersion, platform))
    }

    URL resolve(VersionDescriptor descriptor) throws PyPIClientException {
        if (cache.containsKey(descriptor)) {
            return cache.get(descriptor)
        }
        for (URL repository : repositories) {
            def repoPath = repository.path
            if (repoPath.endsWith('/')) {
                repoPath = repoPath.substring(0, repoPath.length()-1)
            }
            def index = new URL(repository, "${repoPath}/${descriptor.name}/")
            try {
                def links = Jsoup.connect(index.toString())
                        .followRedirects(false)
                        .ignoreHttpErrors(true)
                        .get()
                        .select('a')
                def matching = findPythonVersionAndPlatform(findVersion(links, descriptor.version),
                        descriptor.pythonVersion,
                        descriptor.platform)
                if (matching.size() > 0) {
                    def href = matching[0].attr('href')
                    if (href.lastIndexOf('#') != -1) {
                        href = href.substring(0, href.lastIndexOf('#'))
                    }
                    def url = new URL(index, href)
                    cache.put(descriptor, url)
                    return url
                }
            } catch (UnsupportedMimeTypeException ignored) {
            }
        }
        throw new PyPIClientException("Could not resolve "+descriptor)
    }

    static List<Element> findPythonVersionAndPlatform(List<Element> links,
                                                      String pythonVersion,
                                                      String platform) {
        links.findAll { v ->
            def nameString = v.text()
            if (nameString.endsWith(TAR_GZ))  {
                return true
            }
            def matcher = WHEEL_RE.matcher(nameString)
            if (matcher.matches()) {
                return (platform == null || (platform == matcher.group("plat"))) &&
                       (pythonVersion == null || (pythonVersion == matcher.group("pyver")))
            } else {
                return false
            }
        }
    }

    static List<Element> findVersion(List<Element> links, String version) {
        links.findAll { l ->
            def matcher = PEP440_VERSION_RE.matcher(l.text())
            if (matcher.find()) {
                return matcher.group(0) == version
            } else {
                return false
            }
        }
    }
}

class PyPIClientException extends RuntimeException {
    PyPIClientException(String message) {
        super(message)
    }
}

class VersionDescriptor {
    final String name, version, pythonVersion, platform
    VersionDescriptor(String name, String version, String pythonVersion, String platform) {
        this.name = name
        this.version = version
        this.pythonVersion = pythonVersion
        this.platform = platform
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        VersionDescriptor that = (VersionDescriptor) o

        if (name != that.name) return false
        if (platform != that.platform) return false
        if (pythonVersion != that.pythonVersion) return false
        if (version != that.version) return false

        return true
    }

    int hashCode() {
        int result
        result = (name != null ? name.hashCode() : 0)
        result = 31 * result + (version != null ? version.hashCode() : 0)
        result = 31 * result + (pythonVersion != null ? pythonVersion.hashCode() : 0)
        result = 31 * result + (platform != null ? platform.hashCode() : 0)
        return result
    }

    @Override
    String toString() {
        return "VersionDescriptor{" +
                "name='" + name + '\'' +
                ", version='" + version + '\'' +
                ", pythonVersion='" + pythonVersion + '\'' +
                ", platform='" + platform + '\'' +
                '}'
    }
}
