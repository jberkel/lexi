import com.amazonaws.services.s3.model.CopyObjectRequest
import org.gradle.api.tasks.TaskAction

class AWSReprocessTask extends S3BaseTask {
    @TaskAction
    reprocess() {
        if (bucket == null || bucket.isEmpty()) throw new IllegalArgumentException("no bucket specified")
        if (prefix == null || prefix.isEmpty()) throw new IllegalArgumentException("no prefix specified")

        def listing = client.listObjects(bucket, prefix)

        println("reprocessing objects in ${bucket}/${prefix}")

        listing.getObjectSummaries().each { summary ->
            def copyRequest = new CopyObjectRequest(bucket, summary.key, bucket, summary.key)
            copyRequest.setStorageClass(summary.storageClass)

            logger.warn("=> touch ${bucket}://${summary.key}")
            client.copyObject(copyRequest)
        }
    }
}
