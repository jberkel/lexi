import org.gradle.api.file.FileCollection
import org.gradle.api.logging.LogLevel
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.AbstractExecTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal

class PythonVirtualEnvExec extends AbstractExecTask<PythonVirtualEnvExec> {
    @Internal def requirements = [ 'requirements.txt', 'requirements-model.txt', 'requirements-dev.txt' ]

    @Internal String pythonInterpreter = project.property('default_python_interpreter')
    @Internal String extraIndexUrl = project.findProperty('python_extra_index_url')
    @Internal FileCollection pythonPath = project.files()

    @Internal File virtualEnv = new File(project.buildDir, "virtual-env-$pythonInterpreter")
    @Internal File python     = new File(virtualEnv, 'bin/python')
    @Internal File pip        = new File(virtualEnv, 'bin/pip')

    String requiredPythonVersion = project.findProperty('required_python_version')
    @Internal File pythonVersionFile = new File(project.rootDir, '.python-version')

    PythonVirtualEnvExec() {
        super(PythonVirtualEnvExec.class)
    }

    @Override
    protected void exec() {
        checkPythonVersion()
        createVirtualEnv()
        setEnvironment(['PYTHONPATH': pythonPath.asPath, 'PYTHONIOENCODING': 'UTF-8'])
        setExecutable(python)
        super.exec()
    }

    private void checkPythonVersion() {
        def requiredVersion = getRequiredPythonVersion()
        if (requiredVersion == null) {
            return
        }
        def currentVersion = getCurrentPythonVersion()
        if (currentVersion != "Python $requiredVersion") {
            throw new RuntimeException("Wrong Python version: $currentVersion, expected $requiredVersion")
        }
    }

    private String getRequiredPythonVersion() {
        if (requiredPythonVersion != null) {
            return requiredPythonVersion
        } else if (pythonVersionFile.exists()) {
            return pythonVersionFile.text.trim()
        } else {
            return null
        }
    }

    private String getCurrentPythonVersion() {
        def exec = execActionFactory.newExecAction()
        exec.commandLine = [pythonInterpreter, '--version']
        exec.standardOutput = new ByteArrayOutputStream()
        exec.execute()

        return exec.standardOutput.toString().trim()
    }

    private createVirtualEnv() {
        def exec = execActionFactory.newExecAction()
        exec.commandLine = commandLine(pythonInterpreter)
        exec.execute()
        requirements.collect { r -> project.file(r) }
            .findAll { f -> f.exists() }
            .each { r -> installRequirement(project.file(r)) }
    }

    String[] commandLine(interpreter) {
        if (interpreter == PythonPlugin.PYTHON3) {
            return [interpreter, '-m', 'venv', virtualEnv.absolutePath]
        } else {
            return [
                'virtualenv',
                logger.isEnabled(LogLevel.INFO) ? null : '--quiet',
                '-p', interpreter,
                virtualEnv.absolutePath
            ].findAll { e -> e != null }
        }
    }

    def installRequirement(file) {
        def args = [
            'install',
            '--isolated',
            '--requirement', file.absolutePath,
            '--quiet',
            '--upgrade', 'pip', 'setuptools'
        ]
        if (extraIndexUrl != null) {
            args += [
                '--extra-index-url', extraIndexUrl,
            ]
        }
        def exec = execActionFactory.newExecAction()
        exec.executable(pip)
        exec.args = args
        exec.execute()
    }
}
