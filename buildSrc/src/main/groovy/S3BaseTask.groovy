import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction

class S3BaseTask extends DefaultTask {
    @Input
    String bucket

    @Input
    String region = ""

    @Input
    String prefix = ""

    @Internal AmazonS3 client

    @TaskAction
    initClient() {
        client = AmazonS3ClientBuilder
                .standard()
                .withRegion(region)
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .build()
    }
}
