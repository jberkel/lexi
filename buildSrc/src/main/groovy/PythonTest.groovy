import org.gradle.api.tasks.Internal

class PythonTest extends PythonVirtualEnvExec {
    @Internal File startDirectory = project.python_test_src_dir

    @Override
    protected void exec() {
        pythonPath += project.files(project.python_src_dir)
        pythonPath += project.files(project.rootProject.python_src_dir)
        args = [
            '-m', 'unittest', 'discover',
            '--start-directory', startDirectory.absolutePath
        ]
        super.exec()
    }
}
