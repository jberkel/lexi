import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.logs.AWSLogs
import com.amazonaws.services.logs.AWSLogsClient
import com.amazonaws.services.logs.model.DescribeLogStreamsRequest
import com.amazonaws.services.logs.model.GetLogEventsRequest
import com.amazonaws.services.logs.model.OrderBy
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction

class AWSLogTask extends DefaultTask {
    @Internal AWSLogs client
    @Input String logGroupName
    @Input String region

    AWSLogTask() {
        client = new AWSLogsClient(new DefaultAWSCredentialsProviderChain())
    }

    @TaskAction
    display() {
        if (logGroupName == null || logGroupName.isEmpty()) {
            throw new IllegalArgumentException("logGroupName is not set")
        }
        setRegion()

        def result = client.describeLogStreams(
            new DescribeLogStreamsRequest(logGroupName)
                .withOrderBy(OrderBy.LastEventTime)
                .withDescending(true))

        def streams = result.logStreams.sort { a,b -> b.lastEventTimestamp <=> a.lastEventTimestamp }

        if (!streams.isEmpty()) {
            def lastActiveStream = streams.first()

            def request = new GetLogEventsRequest(logGroupName, lastActiveStream.logStreamName)
            def logEventsResult = client.getLogEvents(request)

            logEventsResult.events.each { event ->
                logger.warn(new Date(event.timestamp).format("[HH:mm:ss.SSS]") + ' ' + event.message.trim())
            }
        }
    }

    protected setRegion() {
        if (region != null && !region.isEmpty()) {
            client.setRegion(Region.getRegion(Regions.fromName(region)))
        }
    }
}
