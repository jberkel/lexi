import com.amazonaws.services.lambda.model.Runtime
import de.undercouch.gradle.tasks.download.Download
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.bundling.Zip

import java.lang.reflect.Field

class PythonLambdaPlugin implements Plugin<Project> {
    private static int DEFAULT_λ_TIMEOUT = 300
    private static int DEFAULT_λ_MEMORY  = 128
    private static int DEFAULT_λ_RETRIES = 0

    @Override
    void apply(Project project) {
        project.pluginManager.apply(PythonTestPlugin)
        project.pluginManager.apply(PythonLintPlugin)

        def aws_default_region = project.findProperty('aws_default_region')
        def aws_account_id = project.findProperty('aws_account_id')

        project.task('zip', type: Zip) {
            from project.file(PythonPlugin.PYTHON_SRC_DIR)
            setArchiveFileName("${project.name}-lambda.zip")
            setDestinationDirectory(project.buildDir)
            exclude('**/*.pyc')
            exclude('**/__pycache__')
        }

        project.task('deploy', type: AWSLambdaTask, dependsOn: [project.testPython, project.zip]) {
            functionName         = project.findProperty('function_name')
            functionDescription  = project.findProperty('function_description')
            handler              = project.findProperty('handler_name')
            lambdaTimeout        = (project.findProperty('lambda_timeout') ?: DEFAULT_λ_TIMEOUT).toInteger()
            memorySize           = (project.findProperty('lambda_memory') ?: DEFAULT_λ_MEMORY).toInteger()
            maximumRetryAttempts = (project.findProperty('max_retries') ?: DEFAULT_λ_RETRIES).toInteger()
            deadLetterQueueArn   = project.findProperty('dead_letter_queue_arn')
            region               = aws_default_region
            role     = "arn:aws:iam::${aws_account_id}:role/lambda_basic_execution"

            // HACK: AWS SDK 1.x won't get this constant    , maintenance mode only
            Field valueField = Runtime.getDeclaredField("value")
            valueField.setAccessible(true)
            runtime  = com.amazonaws.services.lambda.model.Runtime.Python312
            valueField.set(runtime, "python3.13")

            zipFile  = project.zip.archivePath
        }

        project.task('log', type: AWSLogTask) {
            region = aws_default_region
            logGroupName = '/aws/lambda/'+project.findProperty('function_name')
        }

        project.task('clean', type: Delete) {
            delete project.buildDir
        }

        def function_bucket = project.findProperty('function_bucket')

        if (function_bucket != null) {
            /*
            project.task('reprocess', type: AWSReprocessTask) {
                region = aws_default_region
                bucket = function_bucket
                prefix = 'db/'+project.property('dump_version')
            }
            */

            project.task('set-lifecycle-main', type: AWSBucketLifecycleTask) {
                region = aws_default_region
                bucket = function_bucket
            }

            project.task('set-lifecycle-processed', type: AWSBucketLifecycleTask) {
                region = aws_default_region
                bucket =  "$function_bucket-processed"
            }
            project.task('set-lifecycle').dependsOn('set-lifecycle-main', 'set-lifecycle-processed')
        }
    }
}

class PythonLambdaWithParentPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.pluginManager.apply(PythonLambdaPlugin)
        def requirements = RequirementsParser.parse(project.file("requirements-model.txt"))
        def pypiClient = new SimplePyPIClient(project.python_extra_index_url.toString())

        project.task('downloadApsw', type: Download) {
            src  { pypiClient.resolve('apsw', requirements['apsw'], 'cp313', 'linux_x86_64') }
            dest new File(project.buildDir, 'apsw.whl')
            onlyIfNewer true
        }

        project.task('downloadPeewee', type: Download) {
            src  { pypiClient.resolve('peewee', requirements['peewee']) }
            dest new File(project.buildDir, 'peewee.tar.gz')
            onlyIfNewer true
        }

        project.task('downloadMsgspec', type: Download) {
            src  { pypiClient.resolve('msgspec', requirements['msgspec'], 'cp313', 'manylinux_2_17_x86_64.manylinux2014_x86_64') }
            dest new File(project.buildDir, 'msgspec.whl')
            onlyIfNewer true
        }

        project.task('downloadMistletoe', type: Download) {
            src  { pypiClient.resolve('mistletoe', requirements['mistletoe']) }
            dest new File(project.buildDir, 'mistletoe.whl')
            onlyIfNewer true
        }
        if (requirements.containsKey("msgspec")) {
            project.zip.dependsOn(project.downloadMsgspec)
        }
        if (requirements.containsKey("mistletoe")) {
            project.zip.dependsOn(project.downloadMistletoe)
        }

        project.zip.dependsOn(project.downloadApsw, project.downloadPeewee)
        project.zip {
            duplicatesStrategy = 'warn'

            from(project.rootProject.file(PythonPlugin.PYTHON_SRC_DIR))
            from(project.zipTree(project.downloadApsw.dest)) { include '**/*.so' }
            from(project.tarTree(project.downloadPeewee.dest)) {
                include '*/peewee.py',
                        '**/*/playhouse/__init__.py',
                        '**/*/playhouse/apsw_ext.py',
                        '**/*/playhouse/migrate.py',
                        '**/*/playhouse/shortcuts.py',
                        '**/*/playhouse/sqlite_ext.py'
                eachFile { f -> f.path = f.path.substring(f.path.indexOf('/')) /* strip basedir*/ }
                includeEmptyDirs = false
            }
            if (requirements.containsKey("msgspec")) {
                from(project.zipTree(project.downloadMsgspec.dest)) { include 'msgspec/**' }
            }
            if (requirements.containsKey("mistletoe")) {
                from(project.zipTree(project.downloadMistletoe.dest)) { include '**/*.py' }
            }
        }

        if (project.tasks.findByPath('testPython')) {
            project.testPython {
                pythonPath = project.parent.files(PythonPlugin.PYTHON_SRC_DIR)
            }
        }
    }
}
