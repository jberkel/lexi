import org.gradle.api.Plugin
import org.gradle.api.Project

class PythonPlugin implements Plugin<Project> {
    public static final PYTHON_SRC_DIR = 'src/main/python'
    public static final PYTHON_TEST_SRC_DIR = 'src/test/python'
    public static final PYTHON_INTEGRATION_TEST_SRC_DIR = 'src/it/python'
    public static final PYTHON3 = 'python3'

    @Override
    void apply(Project project) {
        project.ext.python_src_dir = project.file(PYTHON_SRC_DIR)
        project.ext.python_test_src_dir = project.file(PYTHON_TEST_SRC_DIR)
        project.ext.python_integration_test_src_dir = project.file(PYTHON_INTEGRATION_TEST_SRC_DIR)
        if (!project.hasProperty('default_python_interpreter')) {
            project.ext.default_python_interpreter = PYTHON3
        }
    }
}

class PythonTestPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.pluginManager.apply(PythonPlugin)
        setupTests(project, 'test', project.python_test_src_dir)
        setupTests(project, 'integrationTest', project.python_integration_test_src_dir)
    }

    private static setupTests(Project project, String taskName, File dir) {
        if (!dir.exists()) { return }

        project.task("${taskName}Python", type: PythonTest) {
            startDirectory = dir
            pythonInterpreter = PythonPlugin.PYTHON3
        }
    }
}

class PythonLintPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.pluginManager.apply(PythonPlugin)
        project.task('pylint', type: PythonLint)
        def test = project.tasks.findByName('testPython')
        if (test != null) {
            test.dependsOn('pylint')
        }
    }
}
